# -*- coding: utf-8 -*-

import unittest
from ....box.utility import gen_uuid
from ....models.user import User
from ..helper import truncate_db
client = app.test_client()

class AuthenticationControllerTest(unittest.TestCase):

    def confirmable_user(self, session):
        user = User(email='user1@ing.com', password_digest='123password4')
        user.password_confirmation='123password4'
        user.confirmable(session)
        return user

    def setUp(self):
        self.user = self.confirmable_user(self.db_session)
    
    def tearDown(self):
        truncate_db()

    def test_signup_get(self):
        response = client.get('/signup')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<form class='signup-form' action='/signup'" in body)

    def test_signup(self):
        data = {'email': 'user2@ing.com', 'password': '123password4',\
        'password_confirmation': '123password4'}
        response = client.post('/signup', data=data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<form class='signin-form' action='/signin' method='post'>" in body)

    def test_signup_with_invalid_data(self):
        data = {'email': 'user2@ing.com', 'password': '123password4',
            'password_confirmation': '123pw4'}
        response = client.post('/signup', data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<ul class='validation-errors'>" in body)
        self.assertTrue(b"<form class='signup-form' action='/signup'" in body)

    def test_confirm(self):
        data = {'confirmation_token': self.user.confirmation_token}
        response = client.put('/confirm', data=data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<form class='signin-form' action='/signin'" in body)

#it returns 404 
#if the unguessable token is not valid 
    def test_confirm_invalid_token(self):
        for token in ['', '  ', gen_uuid()]:
            data = {'confirmation_token': token}
            response = client.put('/confirm', data=data)
            self.assertEqual(response.status_code, 404)
            self.assertEqual(response.headers['Content-Type'], 'text/html')
            body = response.get_data()
            self.assertTrue(b'404' in body)

#it returns 404 if user is already confirmed
    def test_confirm_confirm_again(self):
        data = {'confirmation_token': self.user.confirmation_token}
        client.put('/confirm', data=data)
        response = client.put('/confirm', data=data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b'404' in body)
     
    def test_signin_get(self):
        response = client.get('/signin')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<form class='signin-form' action='/signin'" in body)
      
    def test_signin(self):
        data = {'confirmation_token': self.user.confirmation_token}
        client.put('/confirm', data=data)
        data = {'email': 'user1@ing.com', 'password': '123password4'}
        response = client.post('/signin', data=data)
        self.assertEqual(response.status_code, 302)

    def test_signout(self):
        response = client.delete('/signout', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<form class='signin-form' action='/signin'" in body)

#it returns 404 if user inputs incorrect email or password
    def test_signin_incorrect_data(self):
        data = {'confirmation_token': self.user.confirmation_token}
        client.put('/confirm', data=data)
        data = {'email': 'user1@ing.com', 'password': '123pw4'}
        response = client.post('/signin',data=data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b'404' in body)

#it returns 403 if user not confirm his email 
    def test_signin_not_confirmed(self):
        data = {'email': 'user1@ing.com', 'password': '123password4'}
        response = client.post('/signin', data=data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b'403' in body)
    
    def test_forget_password_get(self):
        response = client.get('/forget_password')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<form class='forget-password-form' action='/forget_password?_method=put'" in body)


    def test_forget_password(self):
        data = {'email': 'user1@ing.com'}
        response = client.put('/forget_password', data=data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<form class='signin-form' action='/signin'" in body)

#it returns 404, if can not find the user 
    def test_forget_password_invalid_email(self):
        data = {'email': 'foo@ing.com'}
        response = client.put('/forget_password', data=data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b'404' in body)
    
    def test_reset_password_get(self):
        data = {'email': 'user1@ing.com'}
        response = client.put('/forget_password', data=data)
        response = client.get('/reset_password?reset_password_token={token}'.\
        format(token=self.user.reset_password_token))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<form class='reset-password-form' action='/reset_password?_method=put' method='post'>" in body)
        
       
    def test_reset_password(self):
        data = {'email': 'user1@ing.com'}
        response = client.put('/forget_password', data=data)
        data = {'reset_password_token': self.user.reset_password_token,\
        'password': '123pw4', 'password_confirmation': '123pw4'}
        response = client.put('/reset_password', data=data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<form class='signin-form' action='/signin' method='post'>" in body)

##it returns 400, if user inputs invalid data
    def test_reset_password_invalid_data(self):
        data = {'email': 'user1@ing.com'}
        response = client.put('/forget_password', data=data)
        data = {'reset_password_token': self.user.reset_password_token,\
        'password': '123pw4', 'password_confirmation': '123pw5'}
        response = client.put('/reset_password', data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.headers['Content-Type'], 'text/html')
        body = response.get_data()
        self.assertTrue(b"<ul class='validation-errors'" in body)
        self.assertTrue(b"<form class='reset-password-form' action='/reset_password?_method=put' method='post'>" in body)

#it returns 404, if reset password token is not correct
    def test_reset_password_incorrect_token(self):
        data = {'email': 'user1@ing.com'}
        client.put('/forget_password', data=data)
        for token in ['', '  ', gen_uuid()]:
            data = {'reset_password_token': token, 'password': '123pw4',\
            'password_confirmation': '123pw4'}
            response = client.put('/reset_password', data=data)
            self.assertEqual(response.status_code, 404)
            self.assertEqual(response.headers['Content-Type'], 'text/html')
            body = response.get_data()
            self.assertTrue(b'404' in body)

if __name__ == '__main__':
    unittest.main()
