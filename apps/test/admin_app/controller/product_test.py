# -*- coding: utf-8 -*-

import unittest
from ....models.product import Product 
from ..helper import truncate_db


class ProductControllerTest(unittest.TestCase): 
    
    def setUp(self):
        self.user = signin(self.db_session, client)
   
    def tearDown(self):
        truncate_db()

    #def test_index_200(self):
    #    self.user.has_role(self.db_session, 'admin')
    #    response = client.get('/products?page=1')
    #    self.assertEqual(response.status_code, 200)
    #    self.assertEqual(response.headers['Content-Type'], 'text/html')
    #    body = response.get_data()
    #    self.assertTrue(b"<a href='/products/new'" in body)

    #def test_index_403(self):
    #    response = client.get('/products?page=1')
    #    self.assertEqual(response.status_code, 403)

    #def test_new_200(self):
    #    self.user.has_role(self.db_session, 'admin')
    #    response = client.get('/products/new') 
    #    self.assertEqual(response.status_code, 200)
    #    self.assertEqual(response.headers['Content-Type'], 'text/html')
    #    body = response.get_data()
    #    form = b"<form action='/products' method='post' enctype='multipart/form-data'>"
    #    self.assertTrue(form in body) 

    #def test_new_403(self):
    #    response = client.get('/products/new')
    #    self.assertEqual(response.status_code, 403)
    #
    #def test_create_200(self):
    #    data = {'name': 'spring', 'time_to_market': '2014/06/01',\
    #    'description': 'the new spring style', 'price': '100'
    #    }
    #    self.user.has_role(self.db_session, 'admin')
    #    response = client.post('/products', data=data, follow_redirects=True)
    #    self.assertEqual(response.status_code, 200)
    #    self.assertEqual(response.headers['Content-Type'], 'text/html')
    #    body = response.get_data()
    #    self.assertTrue(b"<div class='products-show'>" in body)

    #def test_create_403(self):
    #    data = {'name': 'spring', 'time_to_market': '2014/06/01',\
    #    'description': 'the new spring style', 'price': '100'
    #    }
    #    response = client.post('/products', data=data)
    #    self.assertEqual(response.status_code, 403)
    #    self.assertEqual(response.headers['Content-Type'], 'text/html')
    #    body = response.get_data()
    #    self.assertTrue(b"403" in body)

    #def test_create_400(self):
    #    data = {'name': '', 'time_to_market': '2014/06/01',\
    #    'description': 'the new spring style', 'price': '100'
    #    }
    #    self.user.has_role(self.db_session, 'admin')
    #    response = client.post('/products', data=data)
    #    self.assertEqual(response.status_code, 400)
    #    self.assertEqual(response.headers['Content-Type'], 'text/html')
    #    body = response.get_data()
    #    form = b"<form action='/products' method='post' enctype='multipart/form-data'>"
    #    self.assertTrue(form in body)

    #def test_show_200(self):
    #    self.user.has_role(self.db_session, 'admin')
    #    data = {'name': 'spring', 'time_to_market': '2014/06/01',\
    #    'description': 'the new spring style', 'price': '100'
    #    }
    #    client.post('/products', data=data)
    #    product = self.db_session.query(Product).first()
    #    response = client.get('/products/{id}'.format(id=product.id))
    #    self.assertEqual(response.status_code, 200)
    #    self.assertEqual(response.headers['Content-Type'], 'text/html')
    #    body = response.get_data()
    #    self.assertTrue(b"<div class='products-show'>" in body)

    #def test_show_403(self):
    #    self.user.has_role(self.db_session, 'admin')
    #    data = {'name': 'spring', 'time_to_market': '2014/06/01',\
    #    'description': 'the new spring style', 'price': '100'
    #    }
    #    client.post('/products', data=data)

    #    self.user.has_no_role(self.db_session, 'admin')
    #    product = self.db_session.query(Product).first()
    #    response = client.get('/products/{id}'.format(id=product.id))
    #    self.assertEqual(response.status_code, 403)
    #    self.assertEqual(response.headers['Content-Type'], 'text/html')
    #    body = response.get_data()
    #    self.assertTrue(b"403" in body)

    #def test_show_404(self):
    #    self.user.has_role(self.db_session, 'admin')
    #    response = client.get('/products/{id}'.format(id=1))
    #    self.assertEqual(response.status_code, 404)
    #    self.assertEqual(response.headers['Content-Type'], 'text/html')
    #    body = response.get_data()
    #    self.assertTrue(b"404" in body)

if __name__ == '__main__':
    unittest.main()
