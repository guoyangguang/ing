# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime, timedelta
from ...model.user import User
from ...model.profile import Profile
from ...model.item import Item 
from ...model.order import Order
from ...box import utils
from ..helper import truncate_db


class OrderTest(unittest.TestCase):

    def setUp(self):
        # user publish an item, user1 pay it
        user = Storage(
            email='user@book.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)
        item = Storage(
            kind='1',
            name='<Norwegian Wood>',
            link='http://book.douban.com/subject/1046265/',
            buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
            percent_new='95',
            buy_price='1800',
            price='500',
            express_price='1000',
            if_bargain='0',
            state='1',
            city='1',
            img='cover1.jpg',
            img_meta=dict(
                dimension=[500, 666],
                ratio=1.3,
                size=1024,
                filetype='image/jpeg',
                vdimension={'detail': [375, 400], 'thumb': [235, 300]}
            ),
            description='The cover'
        )
        self.item = Item.publish(Item.create(self.profile, item))

        user1 = Storage(
            email='user1@book.com',
            phone='18612345679',
            password_digest='123password4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)
        self.order = Storage(amount='1000')

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Order.table, 'orders')

    def test_per_page(self):
        self.assertEqual(Order.per_page, 15)

    def test_account_validation(self):
        self.assertTrue(Order.validator.validate(self.order, results={}))
        for val in ['', '-1', '8.8', '0']:
            self.order.amount = val
            errors = dict()
            self.assertFalse(
                Order.validator.validate(self.order, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['positive_int'] in errors['amount']
            )

    def test_create(self): 
        created = Order.create(self.profile1, self.item, self.order)
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.profile_id, self.profile1.id)
        self.assertEqual(created.item_id, self.item.id)
        self.assertEqual(created.amount, int(self.order.amount))
        self.assertEqual(created.status, 1)
        self.assertIsInstance(created.created_at, datetime)

    def test_get(self): 
        created = Order.create(self.profile1, self.item, self.order)
        order = Order.get(created.id)
        self.assertEqual(order, created)


if __name__ == '__main__':
    unittest.main()
