# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...model.user import User
from ...model.profile import Profile
from ...model.account import Account
from ...model.account_order import AccountOrder
from ...box import utils
from ..helper import truncate_db


class AccountOrderTest(unittest.TestCase):

    def setUp(self):
        user = Storage(
            email='user@book.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)
        self.account = Account.create(
            self.profile,
            Storage(profile_id=self.profile.id, balance='0')
        )

        self.account_order = Storage(amount='1000')

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(AccountOrder.table, 'account_orders')

    def test_per_page(self):
        self.assertEqual(AccountOrder.per_page, 15)

    def test_account_validation(self):
        self.assertTrue(AccountOrder.validator.validate(
            self.account_order, results={})
        )
        for val in ['', '-1', '8.8', '0']:
            self.account_order.amount = val
            errors = dict()
            self.assertFalse(
                AccountOrder.validator.validate(self.account_order, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['positive_int'] in errors['amount']
            )

    def test_create(self): 
        created = AccountOrder.create(self.account, self.account_order)
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.account_id, self.account.id)
        self.assertEqual(created.amount, int(self.account_order.amount))
        self.assertEqual(created.status, 1)
        self.assertIsInstance(created.created_at, datetime)

    def test_get(self): 
        created = AccountOrder.create(self.account, self.account_order)
        account_order = AccountOrder.get(created.id)
        self.assertEqual(account_order, created)


if __name__ == '__main__':
    unittest.main()
