# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime, timedelta
from ... model.user import User
from ... model.profile import Profile
from ... model.item import Item
from ...model.item_img import ItemImg
from ...box import utils
from ...box.uploader import Uploader
from ..helper import truncate_db


class ItemImgTest(unittest.TestCase):

    def setUp(self): 
        user = Storage(
            email='user1@ing.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)
        self.item = Item.create(
            self.profile,
            Storage(
                kind='1',
                name='<Norwegian Wood>',
                link='http://book.douban.com/subject/1046265/',
                buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
                percent_new='95',
                buy_price='1800',
                price='500',
                express_price='1000',
                if_bargain='0',
                state='1',
                city='1',
                img='cover.jpg',
                img_meta=dict(
                    size=1024,
                    filetype='image/jpeg',
                    vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
                ),
                description='The cover'
            )
        )
        self.item_img = Storage(
            img='note.jpg',
            img_meta=dict(
                size=1024 * 2,
                filetype='image/jpeg'
            ),
            description='The img is note.',
            is_note='1'
        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(ItemImg.table, 'item_imgs')

    def test_versions(self):
        self.assertEqual(ItemImg.versions, {'detail': 425, 'thumb': 235}) 

    def test_description_validation(self):
        self.assertTrue(
            ItemImg.validator.validate(self.item_img, results={})
        )
        for val in ['']:
            errors = dict()
            self.item_img.description = val
            self.assertFalse(
                ItemImg.validator.validate(self.item_img, results=errors)
            )
            self.assertTrue(utils.invalid_msgs['required'] in errors['description'])
        for val in ['the img is note'*4]:
            errors = dict()
            self.item_img.description = val
            self.assertFalse(
                ItemImg.validator.validate(self.item_img, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['length'].format(1, 50) in errors['description']
            )

    def test_is_note_validation(self):
        self.assertTrue(
            ItemImg.validator.validate(self.item_img, results={})
        )
        for val in ['', '-1']:
            errors = dict()
            self.item_img.is_note = val
            self.assertFalse(
                ItemImg.validator.validate(self.item_img, results=errors)
            )
            self.assertTrue(utils.invalid_msgs['one_of'] in errors['is_note'])

    def test_create(self):
        created = ItemImg.create(self.item, self.item_img)
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.item_id, self.item.id)
        self.assertTrue(created.img)
        self.assertTrue(created.img_meta, self.item_img.img_meta)
        self.assertEqual(created.is_note, False)
        self.assertIsNone(created.description)
        self.assertIsInstance(created.created_at, datetime)

    def test_update_general(self):
        created = ItemImg.create(self.item, self.item_img)
        new_itemimg = Storage(description='the note img', is_note='1')
        updated = ItemImg.update_general(created, new_itemimg)
        self.assertTrue(updated.description)
        self.assertIsInstance(updated.is_note, bool)
        self.assertIsInstance(updated.updated_at, datetime)

    def test_get(self):
        created = ItemImg.create(self.item, self.item_img)
        itemimg = ItemImg.get(created.id)
        self.assertEqual(itemimg, created)

    def test_filter_by_item(self):
        created = ItemImg.create(self.item, self.item_img)
        itemimgs = ItemImg.filter_by_item(self.item)
        self.assertIsInstance(itemimgs, list)
        self.assertEqual(len(itemimgs), 1)
        self.assertEqual(itemimgs[0], created)


if __name__ == '__main__':
    unittest.main()
