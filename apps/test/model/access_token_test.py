# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...model.user import User
from ...model.profile import Profile
from ...model.access_token import AccessToken
from ...box import utils
from ..helper import truncate_db


class AccessTokenTest(unittest.TestCase):

    def setUp(self):
        user = Storage(
            email='user@book.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)

        self.access_token = Storage(
            site=u'1',
            access_token=u'109a80b8bbf66ba531cf822a33e342ef',
            expires_in=604800,
            user_id=u'4567923',
        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(AccessToken.table, 'access_tokens')

    def test_site_validation(self):
        self.assertTrue(
            AccessToken.validator.validate(self.access_token, results={})
        )
        for val in ['', '  ', None, '8', 'douban', True, datetime.now()]:
            self.access_token.site = val
            errors = dict()
            self.assertFalse(
                AccessToken.validator.validate(self.access_token, results=errors)
            )
            self.assertTrue(utils.invalid_msgs['one_of'] in errors['site'])

    def test_create(self):
        created = AccessToken.create(self.profile, self.access_token)
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.profile_id, self.profile.id)
        self.assertEqual(unicode(created.site), self.access_token.site)
        self.assertIsInstance(created.created_at, datetime)

    def test_get(self):
        created = AccessToken.create(self.profile, self.access_token)
        access_token = AccessToken.get(created.id)
        self.assertEqual(access_token, created)

    def test_filter_by_profile_and_site(self):
        created = AccessToken.create(self.profile, self.access_token)
        access_token = AccessToken.filter_by_profile_and_site(
            self.profile, self.access_token.site
        ) 
        self.assertEqual(access_token, created)

    def test_filter_by_profile(self):
        created = AccessToken.create(self.profile, self.access_token)
        access_tokens = AccessToken.filter_by_profile(self.profile)
        self.assertEqual(len(access_tokens), 1)
        self.assertEqual(access_tokens[0], created)

    def test_update_access_token(self):
        created = AccessToken.create(self.profile, self.access_token)
        access_token = AccessToken.filter_by_profile_and_site(
            self.profile, self.access_token.site
        ) 
        access_token.access_token = access_token.access_token + 'new' 

        updated = AccessToken.update_access_token(access_token)
        self.assertIsInstance(updated.updated_at, datetime)
        self.assertEqual(updated.access_token, access_token.access_token)

if __name__ == '__main__':
    unittest.main()
