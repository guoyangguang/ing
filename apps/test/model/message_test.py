# -*- coding: utf-8 -*-

import unittest
from web import storage
from ...model.user import User
from ...model.profile import Profile
from ...model.message import Message
from ..helper import truncate_db
from ...box.utils import invalid_msgs
from datetime import datetime

class MessageTest(unittest.TestCase):

    def setUp(self):
        self.message = storage(body='i am msg1')

        user1 = storage(
            email='user1@ing.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)

        user2 = storage(
            email='user2@ing.com',
            phone='18612345679',
            password_digest='123password4'
        )
        self.user2 = User.signup(user2)
        self.profile2 = Profile.create_default_profile(self.user2)

        user3 = storage(
            email='user3@ing.com',
            phone='18612345680',
            password_digest='123password4'
        )
        self.user3 = User.signup(user3)
        self.profile3 = Profile.create_default_profile(self.user3)

        user4 = storage(
            email='user4@ing.com',
            phone='18612345681',
            password_digest='123password4'
        )
        self.user4 = User.signup(user4)
        self.profile4 = Profile.create_default_profile(self.user4)

        msg1 = storage(body='profile1 msg to profile2.')
        msg2 = storage(body='profile1 msg to profile3.')
        msg3 = storage(body='profile2 msg to profile1.')
        msg4 = storage(body='profile3 msg to profile2.')

        self.msg1 = Message.send_message(self.profile1, msg1, self.profile2)
        self.msg2 = Message.send_message(self.profile1, msg2, self.profile3)
        self.msg3 = Message.send_message(self.profile2, msg3, self.profile1)
        self.msg4 = Message.send_message(self.profile3, msg4, self.profile2)

    def tearDown(self):
        truncate_db()
    
    def test_body_validation(self):
        self.assertTrue(Message.validator.validate(self.message, results=dict()))
        for val in [None, '']:
            self.message.body = val 
            errors = dict()
            self.assertFalse(Message.validator.validate(self.message, results=errors))
            msg = invalid_msgs.get('required')
            self.assertTrue(msg in errors['body'])

    def test_get(self):
        msg = Message.get(self.msg1.id)
        self.assertEqual(msg.id, self.msg1.id)

    def test_contacts(self):
        contacts = Message.contacts(self.profile1)
        self.assertEqual(len(contacts), 2)
        self.assertTrue(self.profile2 in contacts)
        self.assertTrue(self.profile3 in contacts)

        contacts = Message.contacts(self.profile2)
        self.assertEqual(len(contacts), 2)
        self.assertTrue(self.profile1 in contacts)
        self.assertTrue(self.profile3 in contacts)

        contacts = Message.contacts(self.profile3)
        self.assertEqual(len(contacts), 2)
        self.assertTrue(self.profile1 in contacts)
        self.assertTrue(self.profile2 in contacts)
  
    def test_unread(self):
        msgs = Message.unread(self.profile1, 1)
        self.assertEqual(len(msgs), 1)
        self.assertTrue(msgs[0].id, self.msg3.id)

        msgs = Message.unread(self.profile2, 1)
        self.assertEqual(len(msgs), 2)
        msg_ids = [msg.id for msg in msgs]
        for id in [self.msg1.id, self.msg4.id]:
            self.assertTrue(id in msg_ids)

        msgs = Message.unread(self.profile3, 1)
        self.assertEqual(len(msgs), 1)
        self.assertTrue(msgs[0].id, self.msg2.id)

        msgs = Message.unread(self.profile4, 1)
        self.assertEqual(len(msgs), 0)

    def test_unread_count(self):
        count = Message.unread_count(self.profile1)      
        self.assertEqual(count, 1) 

        count = Message.unread_count(self.profile2)
        self.assertEqual(count, 2)
 
        count = Message.unread_count(self.profile3)      
        self.assertEqual(count, 1) 
 
        count = Message.unread_count(self.profile4)      
        self.assertEqual(count, 0) 

    def test_between(self):
        msgs = Message.between(self.profile1, self.profile2, 1)
        self.assertEqual(len(msgs), 2)
        for msg in [self.msg1, self.msg3]:
            self.assertTrue(msg in msgs)

        msgs = Message.between(self.profile1, self.profile3, 1)
        self.assertEqual(len(msgs), 1)
        self.assertTrue(self.msg2 in msgs)

        msgs = Message.between(self.profile2, self.profile3, 1)
        self.assertEqual(len(msgs), 1)
        self.assertTrue(self.msg4 in msgs)

        msgs = Message.between(self.profile1, self.profile4, 1)
        self.assertEqual(len(msgs), 0)

    def test_read_message(self):
        self.assertFalse(self.msg1.read)
        self.assertIsNone(self.msg1.read_at)
        self.assertIsNone(self.msg1.updated_at)
	msg = Message.read_message(self.msg1)
        self.assertTrue(msg.read)
        self.assertIsInstance(msg.read_at, datetime)
        self.assertIsInstance(msg.updated_at, datetime)

    def test_send_message(self):
        msg = Message.send_message(
            self.profile1, storage(body='hi, user2.'), self.profile2
        )
        self.assertIsInstance(msg.id, int)
        self.assertEqual(msg.profile_id, self.profile1.id)
        self.assertEqual(msg.body, msg.body)
        self.assertEqual(msg.recipient_id, self.profile2.id)
        self.assertFalse(msg.read)
        self.assertIsNone(msg.read_at)
        self.assertIsInstance(msg.created_at, datetime)
        self.assertIsNone(msg.updated_at)
        self.assertIsNone(msg.deleted_at)
        

if __name__ == '__main__':
    unittest.main()
