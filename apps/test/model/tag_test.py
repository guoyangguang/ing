# -*- coding: utf-8 -*-

import unittest
from ..helper import truncate_db
from ...models.tag import Tag 
from ...box.validation import Validation
from datetime import datetime

class TagTest(unittest.TestCase):

    def setUp(self):
        self.tag = Tag(name='art')
  
    def tearDown(self):
        truncate_db()
    
    def test_table(self):
        self.assertEqual(Tag.__tablename__, 'tags')

    def test_name_validation(self):
        self.assertTrue(Validation.validate(Tag.validator, self.tag))
        self.tag.name = ''
        self.assertFalse(Validation.validate(Tag.validator, self.tag))
        msg = 'Required field cannot be left blank.'
        self.assertTrue(msg in self.tag.errors['name'])
    
    def test_build_tags(self):
        tag = Tag(name='coffee', created_at=datetime.now())
        self.db_session.add(tag)
        self.db_session.commit()
        tags = Tag.build_tags(self.db_session, 'coffee drink bar')
        self.assertEqual(tags['created'], [tag])
        self.assertEqual(len(tags['news']), 2)
        self.assertEqual(tags['news'][0].name, 'drink')
        self.assertEqual(tags['news'][1].name, 'bar')

    def test_build_tags_whitespace_data(self):
        tag = Tag(name='coffee', created_at=datetime.now())
        self.db_session.add(tag)
        self.db_session.commit()
        for data in ['', '         ']:
            tags = Tag.build_tags(self.db_session, data)
            self.assertEqual(len(tags['created']), 0)
            self.assertEqual(len(tags['news']), 0)


if __name__ == '__main__':
    unittest.main()
