# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ..helper import truncate_db
from ...model.user import User
from ...box.utils import invalid_msgs


class UserTest(unittest.TestCase):
    
    def setUp(self):
        self.user = Storage(
            email='user1@ing.com',
            phone='18612345678',
            password_digest='123password4',
            password_confirmation='123password4'
        )

    def tearDown(self):
        truncate_db()

    def test_email_validation(self):
        self.assertTrue(User.validator.validate(self.user, results=dict()))
        for email in ['', ' ', 'user1', 'user1@ing', 'user1ating.com']:
            self.user.email = email
            errors = dict()
            self.assertFalse(User.validator.validate(self.user, results=errors))
            self.assertTrue(invalid_msgs.get('email') in errors.get('email'))
        
        user2 = Storage(
            email='user2@ing.com',
            phone='18656781234',
            password_digest='123password4'
        )
        user2 = User.signup(user2)
        self.user.email = 'user2@ing.com'
        errors = dict()
        self.assertFalse(User.validator.validate(self.user, results=errors))
        self.assertTrue(
            invalid_msgs.get('unique').format('email') in errors.get('email')
        )
    
    def test_phone_validation(self):
        self.assertTrue(User.validator.validate(self.user, results=dict()))
        for phone in ['', ' ', 'phone']:
            self.user.phone = phone 
            errors = dict()
            self.assertFalse(User.validator.validate(self.user, results=errors))
            self.assertTrue(invalid_msgs.get('phone') in errors.get('phone'))
        
        user2 = Storage(
            email='user2@ing.com',
            phone='18656781234',
            password_digest='123password4'
        )
        user2 = User.signup(user2)
        self.user.phone = '18656781234'
        errors = dict()
        self.assertFalse(User.validator.validate(self.user, results=errors))
        self.assertTrue(
            invalid_msgs.get('unique').format('phone') in errors.get('phone')
        )

    def test_password_digest_validation(self):
        self.assertTrue(User.validator.validate(self.user, results=dict()))
        for pwd in [' ', '123pw']:
            self.user.password_digest = pwd 
            errors = dict()
            self.assertFalse(User.validator.validate(self.user, results=errors))
            msg = invalid_msgs.get('length').format(6, 30)
            self.assertTrue(msg in errors.get('password_digest'))
        self.user.password_digest = '123pw4'
        self.user.password_confirmation = '123password4'
        errors = dict()
        self.assertFalse(User.validator.validate(self.user, results=errors))
        msg = invalid_msgs.get('compare').format("password_confirmation")
        self.assertTrue(msg in errors.get('password_digest'))

    def test_encrypt_password_digest(self):
        password_digest = self.user.password_digest
        User.encrypt_password_digest(self.user)
        self.assertNotEqual(self.user.password_digest, password_digest)

    def test_check_password(self):
        User.encrypt_password_digest(self.user)
        password = '123pw4'
        self.assertFalse(User.check_password(self.user, password))
        password = '123password4'
        self.assertTrue(User.check_password(self.user, password))
    
    def test_signup(self):
        user = User.signup(self.user)
        self.assertIsInstance(user.id, int) 
        self.assertEqual(user.phone, self.user.phone)
        self.assertNotEqual(user.password_digest, self.user.password_confirmation)
        self.assertIsInstance(user.created_at, datetime)
        self.assertIsNone(user.deleted_at)

    def test_get(self):
        created = User.signup(self.user)
        user = User.get(created.id) 
        self.assertEqual(user, created)

    def test_filter_by_phone(self):
        created = User.signup(self.user)
        user = User.filter_by_phone(created.phone) 
        self.assertEqual(user, created)

    def test_filter_by_email(self):
        created = User.signup(self.user)
        user = User.filter_by_email(created.email)
        self.assertEqual(user, created) 

    def test_count(self):
        User.signup(self.user)
        self.assertEqual(User.count(), 1) 
    
    def test_signin(self):
        created = User.signup(self.user) 
        self.assertIsNone(created.access_token)
        user = User.filter_by_phone(created.phone)
        self.assertTrue(User.check_password(created, '123password4'))

        user = User.signin(user)
        self.assertTrue(user.access_token)
        self.assertEqual(user.sign_in_count, 1)
        self.assertIsInstance(user.current_sign_in_at, datetime)
        self.assertIsNone(user.last_sign_in_at)
        self.assertEqual(user.current_sign_in_ip, '127.0.0.1')
        self.assertIsNone(user.last_sign_in_ip)

    def test_filter_by_access_token(self):
        created = User.signup(self.user) 
        user = User.filter_by_phone(created.phone)
        User.check_password(user, '123password4')
        user = User.signin(user)

        filter_by_access_token = User.filter_by_access_token(user.access_token)
        self.assertEqual(filter_by_access_token, user) 

    def test_signout(self):
        created = User.signup(self.user) 
        user = User.filter_by_phone(created.phone)
        User.check_password(user, '123password4')
        user = User.signin(user)
        self.assertTrue(user.access_token)

        user = User.signout(user)
        self.assertIsNone(user.access_token)

    def test_reset_password(self):
        user = User.signup(self.user)

        user = User.filter_by_phone(user.phone)
        user.password_digest = '123Pass'
        user = User.reset_password(user)
        self.assertTrue(User.check_password(user, '123Pass'))


if __name__ == '__main__':
    unittest.main()
