# -*- coding: utf-8 -*-

from web import Storage
from datetime import datetime, timedelta
import unittest
from ...model.user import User
from ...model.profile import Profile
from ...model.item import Item
from ..helper import truncate_db
from ...box.utils import invalid_msgs
from ...box.uploader import Uploader


class ItemTest(unittest.TestCase):
    def setUp(self):
        user = Storage(
            email='user@ing.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)

        user1 = Storage(
            email='user1@ing.com',
            phone='18612345679',
            password_digest='123password4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)

        self.item = Storage(
            kind='1',
            name='<Norwegian Wood>',
            link='http://book.douban.com/subject/1046265/',
            buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
            percent_new='95',
            buy_price='18',
            price='5',
            express_price='10',
            if_bargain='0',
            state='1',
            city='1',
            img='cover.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            ),
            description='The cover'
        )
        self.item1 = Storage(
            kind='11',
            name='muji shirt',
            link='http://book.douban.com/subject/1046266/',
            buy_at=datetime.strftime(
                datetime.now()-timedelta(days=60), '%Y/%m/%d'
            ),
            percent_new='90',
            buy_price='300',
            price='150',
            express_price='10',
            if_bargain='1',
            state='3',
            city='43',
            img='cover.jpg',
            img_meta=dict(
                size=1024 * 2,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 275], 'thumb': [235, 185]}
            ),
            description='please send message to me'
        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Item.table, 'items')

    def test_per_page(self):
        self.assertEqual(Item.per_page, 15)

    def test_versions(self):
        self.assertEqual(Item.versions, {'detail': 425, 'thumb': 235}) 

    def test_kind_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '  ', '1.5', '-18', '0', '18¥', '2015/11/02', '19', '20']:
            errors = dict()
            self.item.kind = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('one_of') in errors.get('kind')
            )

    def test_name_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['']:
            errors = dict()
            self.item.name = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('required') in errors.get('name')
            )
        for val in ['book'*8]:
            errors = dict()
            self.item.name = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('length').format(1, 30) in errors.get('name')
            )

    def test_link_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', 'https://example.com/books/1']:
            errors = dict()
            self.item.link = val
            self.assertTrue(Item.validator.validate(self.item, results=errors))
        for val in ['  ', 'example.com']:
            errors = dict()
            self.item.link = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('http_link') in errors.get('link')
            )

    def test_buy_at_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '  ', '1', '二零壹伍']:
            errors = dict()
            self.item.buy_at = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('date') in errors.get('buy_at')
            )

    def test_percent_new_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '  ', '1.5', '-18', '0', '18¥', '2015/11/02', '101']:
            errors = dict()
            self.item.percent_new = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('one_of') in errors.get('percent_new')
            )

    def test_buy_price_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '  ', '1.5', '-18', '0', '18¥', '2015/11/02']:
            errors = dict()
            self.item.buy_price = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('positive_int') in errors.get('buy_price')
            )

    def test_price_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '  ', '1.5', '-18', '0', '18¥', '2015/11/02']:
            errors = dict()
            self.item.price = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('positive_int') in errors.get('price')
            )

    def test_express_price_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        self.item.express_price = '0'
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '  ', '1.5', '-18', '18¥', '2015/11/02']:
            errors = dict()
            self.item.express_price = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('positive_int_or_zero') in errors.get('express_price')
            )

    def test_if_bargain_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '  ', '3', 'True', '2015/11/02']:
            errors = dict()
            self.item.if_bargain = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('one_of') in errors.get('if_bargain')
            )

    def test_state_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '10000']:
            errors = dict()
            self.item.state = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs['one_of'] in errors['state']
            )

    def test_city_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['', '10000']:
            self.item.city = val
            errors = dict()
            self.assertFalse(
                Item.validator.validate(self.item, results=errors)
            )
            self.assertTrue(
                invalid_msgs['one_of'] in errors['city']
            )
        for val in ['43']:
            self.item.city = val
            errors = dict()
            self.assertFalse(
                Item.validator.validate(self.item, results=errors)
            )
            self.assertTrue(
                invalid_msgs['city'] in errors['city']
            )

    def test_description_validation(self):
        self.assertTrue(Item.validator.validate(self.item, results={}))
        for val in ['']:
            errors = dict()
            self.item.description = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('required') in errors.get('description')
            )
        for val in ['the cover'*45]:
            errors = dict()
            self.item.description = val
            self.assertFalse(Item.validator.validate(self.item, results=errors))
            self.assertTrue(
                invalid_msgs.get('length').format(1, 200) in errors.get('description')
            )
 
    def test_create(self):
        item = Item.create(self.profile, self.item)
        self.assertIsInstance(item.id, int)
        self.assertEqual(item.profile_id, self.profile.id)
        self.assertEqual(item.kind, int(self.item.kind))
        self.assertEqual(
            item.buy_at,
            datetime.strptime(self.item.buy_at, '%Y/%m/%d')
        )
        self.assertEqual(item.percent_new, int(self.item.percent_new))
        self.assertEqual(item.buy_price, int(self.item.buy_price) * 100)
        self.assertEqual(item.price, int(self.item.price) * 100)
        self.assertEqual(item.express_price, int(self.item.express_price) * 100)
        self.assertEqual(item.if_bargain, bool(int(self.item.if_bargain))),
        self.assertEqual(item.status, 1),
        self.assertEqual(item.img_meta, self.item.img_meta),
        self.assertIsInstance(item.created_at, datetime)

    def test_get(self):
        created = Item.create(self.profile, self.item)
        item = Item.get(created.id)
        self.assertIsInstance(item, Storage)
        self.assertEqual(item, created)

    def test_publish(self):
        created = Item.create(self.profile, self.item)
        self.assertEqual(created.status, 1)
        self.assertIsNone(created.updated_at)
        published = Item.publish(created)
        self.assertEqual(published.status, 2)
        self.assertIsInstance(published.updated_at, datetime)

    def test_filter_pubitems_by_kind_and_city(self):
        created = Item.create(self.profile, self.item)
        published = Item.publish(created)
        created1 = Item.create(self.profile1, self.item1)
        published1 = Item.publish(created1)

        items = Item.filter_pubitems_by_kind_and_city(1, 1, 1)
        self.assertEqual(len(items), 1)
        self.assertEqual(items[0], published)

        items = Item.filter_pubitems_by_kind_and_city(11, 43, 1)
        self.assertEqual(len(items), 1)
        self.assertEqual(items[0], published1)

    def test_filter_by_published(self):
        created = Item.create(self.profile, self.item)
        created1 = Item.create(self.profile1, self.item1)
        published1 = Item.publish(created1)

        items = Item.filter_by_published(1)
        self.assertEqual(len(items), 1)
        self.assertEqual(items[0], published1)

    def test_filter_by_profile(self):
        created = Item.create(self.profile, self.item)
        items = Item.filter_by_profile(self.profile, 1)
        self.assertIsInstance(items, list)
        self.assertEqual(items[0].profile_id, self.profile.id)

    def test_filter_pubitems_by_profile(self):
        created = Item.create(self.profile, self.item)
        items = Item.filter_pubitems_by_profile(self.profile, 1)
        self.assertIsInstance(items, list)
        self.assertEqual(len(items), 0)

        published = Item.publish(created)
        items = Item.filter_pubitems_by_profile(self.profile, 1)
        self.assertIsInstance(items, list)
        self.assertEqual(items[0], published)

    def test_filter_by_ids(self):
        created = Item.create(self.profile, self.item)
        # published = Item.publish(created)
        items = Item.filter_by_ids([created.id])
        self.assertEqual(len(items), 1)
        self.assertEqual(items[0], created)

    def test_filter_pubitems_by_ids(self):
        created = Item.create(self.profile, self.item)
        published = Item.publish(created)
        created1 = Item.create(self.profile1, self.item1)

        self.assertFalse(Item.filter_pubitems_by_ids([]))
        items = Item.filter_pubitems_by_ids([published.id, created1.id])
        self.assertEqual(
            len(items),
            1
        )
        self.assertEqual(items[0], published)


if __name__ == '__main__':
    unittest.main()
