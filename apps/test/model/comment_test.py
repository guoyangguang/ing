# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime, timedelta
from ..helper import truncate_db
from ...model.user import User
from ...model.profile import Profile
from ...model.item import Item
from ...model.comment import Comment
from ...box import utils


class CommentTest(unittest.TestCase):

    def setUp(self):
        user = Storage(
            email='user@book.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)

        user1 = Storage(
            email='user1@book.com',
            phone='18656781234',
            password_digest='123password4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)

        item = Storage(
            kind='1',
            name='<Norwegian Wood>',
            link='http://book.douban.com/subject/1046265/',
            buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
            percent_new='95',
            buy_price='1800',
            price='500',
            express_price='1000',
            if_bargain='0',
            state='1',
            city='1',
            img='cover1.jpg',
            img_meta=dict(
                dimension=[500, 666],
                ratio=1.3,
                size=1024,
                filetype='image/jpeg',
                vdimension={'detail': [375, 400], 'thumb': [235, 300]}
            ),
            description='The cover'
        )
        self.item = Item.publish(Item.create(self.profile, item))
        self.comment = Storage(body='I like the item')


    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Comment.table, 'comments')

    def test_per_page(self):
        self.assertEqual(Comment.per_page, 15)

    def test_body_validation(self): 
        self.assertTrue(Comment.comment_validator.validate(self.comment, results={}))
        for val in ['']:
            self.comment.body = val
            errors = dict()
            self.assertFalse(
                Comment.comment_validator.validate(self.comment, results=errors)
            )
            self.assertTrue(utils.invalid_msgs['required'] in errors['body'])
        for val in ['I like the item'*10]:
            self.comment.body = val
            errors = dict()
            self.assertFalse(
                Comment.comment_validator.validate(self.comment, results=errors)
            )
            self.assertTrue(
                  utils.invalid_msgs['length'].format(1, 100) in errors['body']
            )

    def test_comment_id_validation(self):
        created = Comment.create(
            profile=self.profile1, item=self.item, new=self.comment
        )
        reply = Storage(body='thanks', comment_id=str(created.id))
        self.assertTrue(
            Comment.reply_validator.validate(reply, results={})
        )
        for val in ['-1', '0', '1,2', '']:
            reply.comment_id = val
            errors = dict()
            self.assertFalse(
                Comment.reply_validator.validate(reply, results=errors)
            )
            self.assertTrue(
                  utils.invalid_msgs['positive_int'] in errors['comment_id']
            )

    def test_create(self):
        created = Comment.create(
            profile=self.profile1, item=self.item, new=self.comment
        )
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.profile_id, self.profile1.id)
        self.assertEqual(created.item_id, self.item.id)
        self.assertEqual(created.body, self.comment.body)
        self.assertIsNone(created.comment_id)
        self.assertIsInstance(created.created_at, datetime)

        reply = Storage(body='would you like to buy it?')
        created1 = Comment.create(
            profile=self.profile, item=self.item, new=reply, comment=created
        )
        self.assertIsInstance(created1.id, int)
        self.assertEqual(created1.profile_id, self.profile.id)
        self.assertEqual(created1.item_id, self.item.id)
        self.assertEqual(created1.body, reply.body)
        self.assertEqual(created1.comment_id, created.id)
        self.assertIsInstance(created1.created_at, datetime)

    def test_get(self):
        created = Comment.create(
            profile=self.profile, item=self.item, new=self.comment
        )
        comment = Comment.get(created.id)
        self.assertEqual(comment, created)

    def test_filter_by_item(self):
        created = Comment.create(
            profile=self.profile1, item=self.item, new=self.comment
        )
        reply = Storage(body='would you like to buy it?')
        created1 = Comment.create(
            profile=self.profile, item=self.item, new=reply, comment=created
        )
        comments = Comment.filter_by_item(self.item)
        self.assertIsInstance(comments, list)
        for comment in comments:
            if comment.profiles_id == self.profile1.id:
                profile1_comment = comment
            if comment.profiles_id == self.profile.id:
                profile_comment = comment
        self.assertEqual(profile1_comment.profiles_id, self.profile1.id)
        self.assertEqual(profile1_comment.id, created.id)
        self.assertEqual(profile1_comment.profile_id, self.profile1.id)
        self.assertEqual(profile1_comment.item_id, self.item.id)
        self.assertIsNone(profile1_comment.comment_id)

        self.assertEqual(profile_comment.profiles_id, self.profile.id)
        self.assertEqual(profile_comment.id, created1.id)
        self.assertEqual(profile_comment.profile_id, self.profile.id)
        self.assertEqual(profile_comment.item_id, self.item.id)
        self.assertEqual(profile_comment.comment_id, created.id)


if __name__ == '__main__':
    unittest.main()
