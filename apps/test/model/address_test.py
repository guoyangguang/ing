# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...model.user import User
from ...model.profile import Profile
from ...model.address import Address
from ...box import utils
from ..helper import truncate_db


class AddressTest(unittest.TestCase):

    def setUp(self):
        user = Storage(
            email='user@book.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)

        self.address = Storage(
            recipient='user',
            phone='18611440935',
            state='1',
            city='1',
            detail='MiLiangKuHuTong No.4, DiAnMenNei Street',
            postcode='100000'
        )

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Address.table, 'addresses')

    def test_per_page(self):
        self.assertEqual(Address.per_page, 15)

    def test_recipient_validation(self):
        errors = dict()
        self.assertTrue(Address.validator.validate(self.address, results=errors))
        for val in ['']:
            self.address.recipient = val
            errors = dict()
            self.assertFalse(
                Address.validator.validate(self.address, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['required'] in errors['recipient']
            )
        for val in ['emily'*6]:
            self.address.recipient = val
            errors = dict()
            self.assertFalse(
                Address.validator.validate(self.address, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['max_length'].format(25) in errors['recipient']
            )

    def test_phone_validation(self):
        self.assertTrue(Address.validator.validate(
            self.address, results={})
        )
        for val in ['', 'phone']:
            self.address.phone = val
            errors = dict()
            self.assertFalse(
                Address.validator.validate(self.address, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['phone'] in errors['phone']
            )

    def test_state_validation(self):
        self.assertTrue(Address.validator.validate(
            self.address, results={})
        )
        for val in ['', '10000']:
            self.address.state = val
            errors = dict()
            self.assertFalse(
                Address.validator.validate(self.address, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['one_of'] in errors['state']
            )

    def test_city_validation(self):
        self.assertTrue(Address.validator.validate(
            self.address, results={})
        )
        for val in ['', '10000']:
            self.address.city = val
            errors = dict()
            self.assertFalse(
                Address.validator.validate(self.address, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['one_of'] in errors['city']
            )
        for val in ['43']:
            self.address.city = val
            errors = dict()
            self.assertFalse(
                Address.validator.validate(self.address, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['city'] in errors['city']
            )

    def test_detail_validation(self):
        self.assertTrue(Address.validator.validate(
            self.address, results={})
        )
        for val in ['']:
            self.address.detail = val
            errors = dict()
            self.assertFalse(
                Address.validator.validate(self.address, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['required'] in errors['detail']
            )
        for val in ['NO'*51]:
            self.address.detail = val
            errors = dict()
            self.assertFalse(
                Address.validator.validate(self.address, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['max_length'].format(100) in errors['detail']
            )

    def test_postcode_validation(self):
        self.assertTrue(Address.validator.validate(
            self.address, results={})
        )
        for val in ['', 'postcode']:
            self.address.postcode = val
            errors = dict()
            self.assertFalse(
                Address.validator.validate(self.address, results=errors)
            )
            self.assertTrue(
                utils.invalid_msgs['postcode'] in errors['postcode']
            )

    def test_create(self): 
        created = Address.create(self.profile, self.address)
        self.assertIsInstance(created.id, int)
        self.assertEqual(created.profile_id, self.profile.id)
        self.assertIsInstance(created.created_at, datetime)

    def test_get(self): 
        created = Address.create(self.profile, self.address)
        address = Address.get(created.id)
        self.assertEqual(address, created)

    def test_filter_by_profile(self):
        created = Address.create(self.profile, self.address)
        addresses = Address.filter_by_profile(self.profile)
        self.assertIsInstance(addresses, list)
        self.assertEqual(addresses[0].profile_id, self.profile.id)


if __name__ == '__main__':
    unittest.main()
