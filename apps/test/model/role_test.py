# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ...model.role import Role
from ..helper import truncate_db
from ...box.utils import invalid_msgs


class RoleTest(unittest.TestCase):

    def setUp(self):
        self.admin = Storage(name='admin')
        self.customer_service = Role.create(Storage(name='customer service'))
        self.seller = Role.create(Storage(name='seller'))

    def tearDown(self):
        truncate_db()
    
    def test_validation_name(self):
        self.assertTrue(Role.validator.validate(self.admin, results=dict()))
        for val in ['@', '!', '$']:
            self.admin.name = val
            errors=dict()
            self.assertFalse(Role.validator.validate(self.admin, results=errors))
            self.assertTrue(invalid_msgs.get('slug') in errors.get('name'))
        Role.create(Storage(name='admin'))
        self.admin.name = 'admin'
        errors=dict()
        self.assertFalse(Role.validator.validate(self.admin, results=errors))
        msg = invalid_msgs.get('unique').format('role')
        self.assertTrue(msg in errors.get('name'))
   
    def test_all(self):
        roles = Role.all()
        self.assertEqual(len(roles), 2)
        self.assertTrue(self.customer_service in roles)
        self.assertTrue(self.seller in roles)
        
    def test_filter_by_ids(self):
        ids = [self.seller.id, self.customer_service.id]
        roles = Role.filter_by_ids(ids)
        self.assertEqual(len(roles), 2)
        self.assertTrue(self.customer_service in roles)
        self.assertTrue(self.seller in roles)

    def test_filter_by_name(self):
        role = Role.filter_by_name('admin')      
        self.assertIsNone(role)
        role = Role.filter_by_name('customer service')      
        self.assertEqual(role.id, self.customer_service.id)

    def test_create(self): 
        admin = Role.create(self.admin)
        self.assertIsInstance(admin.id, int)
        self.assertIsInstance(admin.created_at, datetime)

    def test_delete(self):
        self.assertIsInstance(
            Role.filter_by_name('customer service'),
            Storage
        )
        customer_service = Role.delete(self.customer_service) 
        self.assertIsInstance(customer_service.deleted_at, datetime)
        self.assertIsNone(
            Role.filter_by_name('customer service')
        )

if __name__ == '__main__':
    unittest.main()
