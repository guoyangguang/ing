# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime
from ..helper import truncate_db
from ...model.followship import Followship
from ...model.user import User
from ...model.profile import Profile 


class FollowshipTest(unittest.TestCase):
   
    def setUp(self):
        user1 = Storage(
            email='user1@book.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)

        user2 = Storage(
            email='user2@book.com',
            phone='18612345679',
            password_digest='123password4'
        )
        self.user2 = User.signup(user2)
        self.profile2 = Profile.create_default_profile(self.user2)

        user3 = Storage(
            email='user3@book.com',
            phone='18612345680',
            password_digest='123password4'
        )
        self.user3 = User.signup(user3)
        self.profile3 = Profile.create_default_profile(self.user3)

    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Followship.table, 'followship')
    
    def test_get(self):
        followship = Followship.get(self.profile1.id, self.profile2.id)
        self.assertIsNone(followship)
        Followship.follow(self.profile1, self.profile2)
        followship = Followship.get(self.profile1.id, self.profile2.id)
        self.assertEqual(followship.following_id, self.profile1.id)
        self.assertEqual(followship.followed_id, self.profile2.id)
        self.assertIsInstance(followship.created_at, datetime)
        self.assertIsNone(followship.deleted_at)

    def test_followeds(self):
        self.assertEqual(len(Followship.followeds(self.profile1)), 0)
        Followship.follow(self.profile1, self.profile2) 
        Followship.follow(self.profile1, self.profile3) 
        profiles = Followship.followeds(self.profile1)
        self.assertEqual(len(profiles), 2)
        self.assertTrue(self.profile2 in profiles)
        self.assertTrue(self.profile3 in profiles)

    def test_followings(self):
        self.assertEqual(len(Followship.followings(self.profile2)), 0)
        self.assertEqual(len(Followship.followings(self.profile3)), 0)
        Followship.follow(self.profile1, self.profile2) 
        Followship.follow(self.profile1, self.profile3) 

        profiles = Followship.followings(self.profile2)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile1)

        profiles = Followship.followings(self.profile3)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile1)

    def test_follow(self):
        self.assertEqual(len(Followship.followeds(self.profile1)), 0)
        self.assertEqual(len(Followship.followings(self.profile2)), 0)
        self.assertEqual(len(Followship.followings(self.profile3)), 0)
        followship = Followship.follow(self.profile1, self.profile2) 
        self.assertIsNone(followship.deleted_at)
        Followship.follow(self.profile1, self.profile3) 

        profiles = Followship.followeds(self.profile1)
        self.assertEqual(len(profiles), 2)
        self.assertTrue(self.profile2 in  profiles)
        self.assertTrue(self.profile3 in profiles)

        profiles = Followship.followings(self.profile2)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile1)

        profiles = Followship.followings(self.profile3)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile1)
   
    def test_unfollow(self): 
        followship = Followship.follow(self.profile1, self.profile2) 
        Followship.follow(self.profile1, self.profile3) 

        profiles = Followship.followeds(self.profile1)
        self.assertEqual(len(profiles), 2)
        self.assertTrue(self.profile2 in profiles)
        self.assertTrue(self.profile3 in profiles)

        profiles = Followship.followings(self.profile2)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile1)
        
        followship = Followship.unfollow(followship)
        self.assertIsInstance(followship.deleted_at, datetime)
        profiles = Followship.followeds(self.profile1)
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0], self.profile3)
        profiles = Followship.followings(self.profile2)
        self.assertEqual(len(profiles), 0)

if __name__ == '__main__':
    unittest.main()
