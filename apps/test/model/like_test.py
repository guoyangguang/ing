# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime, timedelta
from ..helper import truncate_db
from ...model.user import User
from ...model.profile import Profile 
from ...model.item import Item
from ...model.like import Like 


class LikeTest(unittest.TestCase):

    def setUp(self):
        # user create two items, and is liked by other two users
        user = Storage(
            email='user@book.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)

        user1 = Storage(
            email='user1@book.com',
            phone='18612345679',
            password_digest='123password4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)

        user2 = Storage(
            email='user2@book.com',
            phone='18612345680',
            password_digest='123password4'
        )
        self.user2 = User.signup(user2)
        self.profile2 = Profile.create_default_profile(self.user2)

        item1 = Storage(
            kind='1',
            name='<Norwegian Wood>',
            link='http://book.douban.com/subject/1046265/',
            buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
            percent_new='95',
            buy_price='1800',
            price='500',
            express_price='1000',
            if_bargain='0',
            state='1',
            city='1',
            img='cover1.jpg',
            img_meta=dict(
                dimension=[500, 666],
                ratio=1.3,
                size=1024,
                filetype='image/jpeg',
                vdimension={'detail': [375, 400], 'thumb': [235, 300]}
            ),
            description='The cover'
        )
        item2 = Storage(
            kind='1',
            name='<Angel Cake>',
            link='http://book.douban.com/subject/1046266/',
            buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
            percent_new='95',
            buy_price='1500',
            price='500',
            express_price='1000',
            if_bargain='0',
            state='1',
            city='1',
            img='cover2.jpg',
            img_meta=dict(
                dimension=[800, 600],
                ratio=0.9,
                size=1024 * 2,
                filetype='image/jpeg',
                vdimension={'detail': [375, 275], 'thumb': [235, 185]}
            ),
            description='The cover'
        )
        self.item1 = Item.publish(Item.create(self.profile, item1))
        self.item2 = Item.publish(Item.create(self.profile, item2))


    def tearDown(self):
        truncate_db()

    def test_table(self):
        self.assertEqual(Like.table, 'likes')

    def test_per_page(self):
        self.assertEqual(Like.per_page, 15)

    def test_like(self):
        like = Like.like(self.profile1, self.item1)
        self.assertEqual(like.profile_id, self.profile1.id)
        self.assertEqual(like.item_id, self.item1.id)
        self.assertIsInstance(like.created_at, datetime)

    def test_get(self):
        created = Like.like(self.profile1, self.item1)
        like = Like.get(self.profile1, self.item1)
        self.assertEqual(like, created)

    def test_cancel_like(self):
        created = Like.like(self.profile1, self.item1)
        canceled = Like.cancel_like(created)
        self.assertIsInstance(canceled.deleted_at, datetime)
        self.assertIsNone(Like.get(self.profile1, self.item1))

    def test_item_liked_count(self):
        Like.like(self.profile1, self.item1)
        Like.like(self.profile2, self.item1)
        self.assertEqual(Like.item_liked_count(self.item1), 2)
        Like.like(self.profile1, self.item2)
        self.assertEqual(Like.item_liked_count(self.item2), 1)

    def test_pubitems_liked_by(self):
        Like.like(self.profile1, self.item1)
        Like.like(self.profile2, self.item1)
        Like.like(self.profile1, self.item2)
        items = Like.pubitems_liked_by(self.profile1, 1)
        # FIXME why the order is not created_at desc?
        self.assertEqual(items, [self.item1, self.item2])
        self.assertEqual(Like.pubitems_liked_by(self.profile2, 1), [self.item1])


if __name__ == '__main__':
    unittest.main()
