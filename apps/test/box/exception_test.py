#-*- coding: utf-8 -*-

import unittest
import web
import json
from ...box.exception import (
    format_errors, Unauthorized, Forbidden,
    NotFound, BadRequest, MissedData
)

class ExceptionTest(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass
       
    def test_Unauthorized(self):
        urls = (
            '/raise_unauthorized', 'unauthorized'
        )
        class unauthorized(object):
            def GET(self):
                raise Unauthorized('please signin.')
        app = web.application(urls, locals())
        r = app.request('/raise_unauthorized', method='GET')
        self.assertEqual(r.status, '401 Unauthorized')
        self.assertEqual(json.loads(r.data), dict(msg='please signin.'))

    def test_Forbidden(self):
        urls = (
            '/raise_forbidden', 'forbidden'
        )
        class forbidden(object):
            def GET(self):
                raise Forbidden('the request is access denied.')
        app = web.application(urls, locals())
        r = app.request('/raise_forbidden', method='GET')
        self.assertEqual(r.status, '403 Forbidden')
        self.assertEqual(json.loads(r.data), dict(msg='the request is access denied.'))

    
    def test_NotFound(self):
        urls = (
            '/raise_notfound', 'notfound'
        )
        class notfound(object):
            def GET(self):
                raise NotFound('the resource is not found.')
        app = web.application(urls, locals())
        r = app.request('/raise_notfound', method='GET')
        self.assertEqual(r.status, '404 Not Found')
        self.assertEqual(json.loads(r.data), dict(msg='the resource is not found.'))

    def test_BadRequest(self):
        urls = (
            '/raise_badrequest', 'badrequest'
        )
        class badrequest(object):
            def GET(self):
                errors = {'email': ['Required field cannot be left blank.']}
                raise BadRequest(errors)
        app = web.application(urls, locals())
        r = app.request('/raise_badrequest')
        self.assertEqual(r.status, '400 Bad Request')
        self.assertEqual(
            json.loads(r.data),
            {'msg': ['Email, required field cannot be left blank.']}
        )

    def test_MissedData(self):
        urls = (
            '/raise_misseddata', 'misseddata'
        )
        class misseddata(object):
            def GET(self):
                raise MissedData('the name is missed.')
        app = web.application(urls, locals())
        r = app.request('/raise_misseddata')
        self.assertEqual(r.status, '400 Bad Request')
        self.assertEqual(json.loads(r.data), dict(msg='the name is missed.'))

if __name__ == '__main__':
    unittest.main()
