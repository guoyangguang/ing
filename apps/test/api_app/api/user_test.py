#-*- coding: utf-8 -*-

import unittest
import json
from web import Storage
import datetime
import uuid
from ....box import utils 
from ....model.user import User
from ....model.profile import Profile
from ....model.account import Account
from ....model.verification import Verification
from ...helper import truncate_db
from ....api_app.app import app

class UserAPITest(unittest.TestCase):

    def setUp(self):
        user = Storage(
            email='user1@ing.com',
            phone='18612345678',
            password_digest='123password4',
            password_confirmation='123password4'
        )
        self.user = User.signup(user)
    
    def tearDown(self):
        truncate_db()

    def test_AuthSignupAPI_POST_missed_verification_code(self):
        Verification.create(
            Storage(phone='18612345679', verification_code='100000')
        )

        request_data = dict(
            csrf_token=uuid.uuid4().hex,
            captcha_data='A1B2',
            email='user2@ing.com',
            phone='18612345679',
            password='123password4',
            password_confirmation='123password4',
            accept='1'
        )
        json_data = json.dumps(request_data)
        response = app.request(
            '/api/signup',
            method='POST',
            data=json_data,
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'verification_code is required.'
        )

    def test_AuthSignupAPI_POST_404_verification(self):
        request_data = dict(
            csrf_token=uuid.uuid4().hex,
            captcha_data='A1B2',
            email='user2@ing.com',
            phone='18612345679',
            password='123password4',
            password_confirmation='123password4',
            verification_code='100000',
            accept='1'
        )
        json_data = json.dumps(request_data)
        response = app.request(
            '/api/signup',
            method='POST',
            data=json_data,
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'), u'请点击获取验证码.'
        )

    def test_AuthSignupAPI_POST_400_confirm_phone(self):
        Verification.create(
            Storage(phone='18612345679', verification_code='100000')
        )

        request_data = dict(
            csrf_token=uuid.uuid4().hex,
            captcha_data='A1B2',
            email='user2@ing.com',
            phone='18612345679',
            password='123password4',
            password_confirmation='123password4',
            verification_code='100001',
            accept='1'
        )
        json_data = json.dumps(request_data)
        response = app.request(
            '/api/signup',
            method='POST',
            data=json_data,
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'),
            u'无法确认手机号码, 请点击获取验证码.'
        )

    def test_AuthSignupAPI_POST_400_invalid_data(self):
        Verification.create(
            Storage(phone='18612345679', verification_code='100000')
        )

        request_data = dict(
            csrf_token=uuid.uuid4().hex,
            captcha_data='A1B2',
            email='user1@ing.com',
            phone='18612345679',
            password='123password4',
            password_confirmation='123password4',
            verification_code='100000',
            accept='1'
        )
        json_data = json.dumps(request_data)
        response = app.request(
            '/api/signup',
            method='POST',
            data=json_data,
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(
            u'Email, email 已存在.', response_data.get('msg')[0]
        )

    def test_AuthSignupAPI_POST(self):
        Verification.create(
            Storage(phone='18612345679', verification_code='100000')
        )

        request_data= dict(
            csrf_token=uuid.uuid4().hex,
            captcha_data='A1B2',
            email='user2@ing.com',
            phone='18612345679',
            password='123password4',
            password_confirmation='123password4',
            verification_code='100000',
            accept='1'
        )
        json_data = json.dumps(request_data)
        response = app.request(
            '/api/signup',
            method='POST',
            data=json_data,
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        user = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(user.get('id'), int)
        self.assertEqual(user.get('email'), request_data.get('email'))
        self.assertEqual(user.get('phone'), request_data.get('phone'))
        self.assertIsNone(user.get('access_token'))
        self.assertTrue(user.get('created_at'))
        self.assertIsNone(user.get('updated_at'))
        self.assertIsNone(user.get('deleted_at'))
        profile = Profile.filter_by_user(Storage(user))
        self.assertTrue(profile)
        self.assertTrue(Account.filter_by_profile(profile))

    def test_AuthSigninAPI_POST_missed_csrf_token(self):
        request_data = {
            'captcha_data': 'A1B2',
            'phone': '18612345678',
            'password': '1234password4'
        }
        json_data = json.dumps(request_data)
        response = app.request(
            '/api/signin',
            method='POST',
            data=json_data,
            headers={'Content_Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'csrf_token is required.')

    def test_AuthSigninAPI_POST_404_password(self):
        request_data = {
            'csrf_token': uuid.uuid4().hex,
            'captcha_data': 'A1B2',
            'phone': '18612345678',
            'password': '123password'
        }
        json_data = json.dumps(request_data)
        response = app.request(
            '/api/signin',
            method='POST',
            data=json_data,
            headers={'Content_Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'用户不存在.')

    def test_AuthSigninAPI_POST(self):
        request_data = {
            'csrf_token': uuid.uuid4().hex,
            'captcha_data': 'A1B2',
            'phone': '18612345678',
            'password': '123password4'
        }
        json_data = json.dumps(request_data)
        response = app.request(
            '/api/signin',
            method='POST',
            data=json_data,
            content_type='application/json'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        user = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(user.get('id'), int)
        self.assertEqual(user.get('email'), self.user.email)
        self.assertEqual(user.get('phone'), self.user.phone)
        self.assertTrue(user.get('access_token'))
        self.assertTrue(user.get('created_at'))
        self.assertTrue(user.get('updated_at'))
        self.assertIsNone(user.get('deleted_at'))

    def test_AuthSignoutAPI_POST_400_missed_csrf_token(self):
        user = User.signin(self.user) 
        self.assertTrue(user.access_token)

        request_data = {
            'access_token': user.access_token
        }
        response = app.request(
            '/api/signout',
            method='POST',
            data=json.dumps(request_data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"csrf_token is required."
        )

    def test_AuthSignoutAPI_POST_401_invalid_access_token(self):
        user = User.signin(self.user) 
        self.assertTrue(user.access_token)

	json_data = json.dumps(
            dict(csrf_token=uuid.uuid4().hex, access_token=utils.gen_uuid())
         )
        response = app.request(
            '/api/signout',
            method='POST',
            data=json_data,
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_AuthSignoutAPI_POST(self):
        user = User.signin(self.user) 
        self.assertTrue(user.access_token)
  
	json_data = json.dumps(
            dict(csrf_token=uuid.uuid4().hex, access_token=user.access_token)
        )
        response = app.request(
            '/api/signout',
            method='POST',
            data=json_data,
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        user = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(user.get('id'), int)
        self.assertIsNone(user.get('access_token'))
        self.assertTrue(user.get('created_at'))
        self.assertTrue(user.get('updated_at'))
        self.assertIsNone(user.get('deleted_at'))

    def test_AuthResetPasswordAPI_POST_400_missed_verification_code(self):
        Verification.create(
            Storage(phone='18612345678', verification_code='100000')
        )

        request_data = {
            'csrf_token': uuid.uuid4().hex,
            'captcha_data': 'A1B2',
            'phone': '18612345678',
            'password': '123pw4',
            'password_confirmation': '123pw5'
        } 
        response = app.request(
            '/api/reset_password',
            method='POST',
            data=json.dumps(request_data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'verification_code is required.')

    def test_AuthResetPasswordAPI_POST_404_user(self):
        Verification.create(
            Storage(phone='18612345678', verification_code='100000')
        )

        request_data = {
            'csrf_token': uuid.uuid4().hex,
            'captcha_data': 'A1B2',
            'verification_code': '100000',
            'phone': '18612345679',
            'password': '123pw4',
            'password_confirmation': '123pw4'
        }
        response = app.request(
            '/api/reset_password',
            method='POST',
            data=json.dumps(request_data),
            headers={'Content-Type': 'application/json'}
        ) 
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'用户不存在.')

    def test_AuthResetPasswordAPI_POST_404_verification(self):
        request_data = {
            'csrf_token': uuid.uuid4().hex,
            'captcha_data': 'A1B2',
            'verification_code': '100000',
            'phone': '18612345678',
            'password': '123pw4',
            'password_confirmation': '123pw4'
        }
        response = app.request(
            '/api/reset_password',
            method='POST',
            data=json.dumps(request_data),
            headers={'Content-Type': 'application/json'}
        ) 
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请点击获取验证码.')

    def test_AuthResetPasswordAPI_POST_400_not_confirmed(self):
        Verification.create(
            Storage(phone='18612345678', verification_code='100000')
        )

        request_data = {
            'csrf_token': uuid.uuid4().hex,
            'captcha_data': 'A1B2',
            'verification_code': '100001',
            'phone': '18612345678',
            'password': '123pw4',
            'password_confirmation': '123pw4'
        }
        response = app.request(
            '/api/reset_password',
            method='POST',
            data=json.dumps(request_data),
            headers={'Content-Type': 'application/json'}
        ) 
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'无法确认手机号码, 请点击获取验证码.'
        )

    def test_AuthResetPasswordAPI_POST_400_invalid_password(self):
        Verification.create(
            Storage(phone='18612345678', verification_code='100000')
        )

        request_data = {
            'csrf_token': uuid.uuid4().hex,
            'captcha_data': 'A1B2',
            'verification_code': '100000',
            'phone': '18612345678',
            'password': '123pw4',
            'password_confirmation': '123pw5'
        }
        response = app.request(
            '/api/reset_password',
            method='POST',
            data=json.dumps(request_data),
            headers={'Content-Type': 'application/json'}
        ) 
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            utils.invalid_msgs['compare'].format('password_confirmation') in response_data.get('msg')[0] 
        )

    def test_AuthResetPasswordAPI_POST(self):
        Verification.create(
            Storage(phone='18612345678', verification_code='100000')
        )

        self.assertTrue(
            User.check_password(User.get(self.user.id), '123password4')
        )
        request_data = {
            'csrf_token': uuid.uuid4().hex,
            'captcha_data': 'A1B2',
            'verification_code': '100000',
            'phone': '18612345678',
            'password': '123pw4',
            'password_confirmation': '123pw4'
        }
        response = app.request(
            '/api/reset_password',
            method='POST',
            data=json.dumps(request_data),
            headers={'Content-Type': 'application/json'}
        ) 
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')

        self.assertTrue(
            User.check_password(User.get(self.user.id), '123pw4')
        )
        user = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(user.get('id'), int)
        self.assertIsNone(user.get('access_token'))
        self.assertTrue(user.get('created_at'))
        self.assertTrue(user.get('updated_at'))
        self.assertIsNone(user.get('deleted_at'))

if __name__ == '__main__':
    unittest.main()
