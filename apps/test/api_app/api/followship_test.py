# -*- coding: utf-8 -*-

import unittest
from web import Storage
import json
import uuid
from ...helper import truncate_db, signup_and_signin
from ....api_app.app import app
from ....model.user import User
from ....model.profile import Profile
from ....box.utils import gen_uuid


class FollowshipAPITest(unittest.TestCase):

    def setUp(self):
        self.user1, self.profile1 = signup_and_signin()
        user2 = Storage(
            email='user2@book.com',
            phone='18612345679',
            password_digest='123pw4'
        )
        self.user2 = User.signup(user2)
        self.profile2 = Profile.create_default_profile(self.user2)

    def tearDown(self):
        truncate_db()

    def test_FollowedMineAPI_GET(self):
        response = app.request(
            '/api/followeds/mine?access_token={0}'.format(self.user1.access_token),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        profiles = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(profiles), 0)
       
        request_data = json.dumps(
            dict(
                access_token=self.user1.access_token,
                csrf_token=uuid.uuid4().hex,
                id=self.profile2.id
            )
        ) 
        response = app.request(
            '/api/followeds/mine',
            method='POST',
            data=request_data, 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        followship = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            followship.get('followed_id'), self.profile2.id
        )
        response = app.request(
            '/api/followeds/mine?access_token={0}'.format(self.user1.access_token),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        profiles = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            profiles[0].get('id'), self.profile2.id
        )
 
    def test_FollowedMineAPI_GET_missed_data(self):
        response = app.request('/api/followeds/mine', method='GET')
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"access_token is required."
        )

    def test_FollowedMineAPI_GET_unauthorized(self):
        response = app.request(
            '/api/followeds/mine?access_token={0}'.format(gen_uuid()),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u"请登录.")

    def test_FollowedMineAPI_POST(self):
        request_data = json.dumps(
            dict(
                access_token=self.user1.access_token,
                csrf_token=uuid.uuid4().hex,
                id=self.profile2.id
            )
        ) 
        response = app.request('/api/followeds/mine', method='POST', data=request_data, 
            headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        followship = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            followship.get('followed_id'),
            self.profile2.id
        )
        self.assertTrue(followship.get('created_at'))

    def test_FollowedMineAPI_POST_missed_data(self):
        request_data = json.dumps(
            dict(
                csrf_token=uuid.uuid4().hex,
                access_token=self.user1.access_token
            )
        )
        response = app.request('/api/followeds/mine', method='POST', data=request_data, 
            headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'id is required.')

    def test_FollowedMineAPI_POST_unauthorized(self):
        request_data = json.dumps(
            dict(
                access_token=gen_uuid(),
                csrf_token=uuid.uuid4().hex,
                id=self.profile2.id
            )
        ) 
        response = app.request('/api/followeds/mine', method='POST', data=request_data, 
            headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_FollowedMineAPI_POST_follow_user_himself(self):
        request_data = json.dumps(
            dict(
                access_token=self.user1.access_token,
                csrf_token=uuid.uuid4().hex,
                id=self.profile1.id
            )
        ) 
        response = app.request('/api/followeds/mine', method='POST', data=request_data, 
            headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'Could not follow yourself.')

    def test_FollowedMineAPI_POST_followed_user_not_found(self):
        request_data = json.dumps(
            dict(
                access_token=self.user1.access_token,
                csrf_token=uuid.uuid4().hex,
                id=self.profile2.id + 1
            )
        )
                                   
        response = app.request('/api/followeds/mine', method='POST', data=request_data, 
            headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'user is not found.')

    def test_FollowedMineAPI_POST_already_followed(self):
        request_data = json.dumps(
            dict(
                access_token=self.user1.access_token,
                csrf_token=uuid.uuid4().hex,
                id=self.profile2.id
            )
        ) 
        response = app.request('/api/followeds/mine', method='POST', data=request_data, 
            headers={'Content-Type': 'application/json'})

        request_data = json.dumps(
            dict(
                access_token=self.user1.access_token,
                csrf_token=uuid.uuid4().hex,
                id=self.profile2.id
            )
        ) 
        response = app.request('/api/followeds/mine', method='POST', data=request_data, 
            headers={'Content-Type': 'application/json'})
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'user is already followed.')

    def test_FollowedMineAPI_PUT(self):
        request_data = json.dumps(
            dict(
                access_token=self.user1.access_token,
                csrf_token=uuid.uuid4().hex,
                id=self.profile2.id
            )
        ) 
        app.request('/api/followeds/mine', method='POST', data=request_data, 
            headers={'Content-Type': 'application/json'})
        response = app.request(
            '/api/followeds/mine?access_token={0}'.format(self.user1.access_token),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        profiles = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0].get('id'), self.profile2.id)
        
        response = app.request(
            '/api/followeds/mine/{0}'.format(self.profile2.id),
            method='PUT',
            data=json.dumps(
                dict(csrf_token=uuid.uuid4().hex, access_token=self.user1.access_token)
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertTrue(response_data['deleted_at'])

        response = app.request(
            '/api/followeds/mine?access_token={0}'.format(self.user1.access_token),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        profiles = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(profiles), 0)

    def test_FollowedMineAPI_PUT_missed_data(self):
        response = app.request(
            '/api/followeds/mine/{0}'.format(self.profile2.id),
            method='PUT',
            data=json.dumps(
                dict(csrf_token=uuid.uuid4().hex)
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        self.assertEqual(
            json.loads(response.data, encoding='utf-8').get('msg'),
            u"access_token is required."
        )

    def test_FollowedMineAPI_PUT_unauthorized(self):
        response = app.request(
            '/api/followeds/mine/{0}'.format(self.profile2.id),
            method='PUT',
            data=json.dumps(
                dict(csrf_token=uuid.uuid4().hex, access_token=gen_uuid())
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        self.assertEqual(
            json.loads(response.data, encoding='utf-8').get('msg'),
            u'请登录.'
        )

    def test_FollowedMineAPI_PUT_user_not_found(self):
        response = app.request(
            '/api/followeds/mine/{0}'.format(self.profile2.id + 1),
            method='PUT',
            data=json.dumps(
                dict(csrf_token=uuid.uuid4().hex, access_token=self.user1.access_token)
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        self.assertEqual(
            json.loads(response.data, encoding='utf-8').get('msg'),
            u'user is not found.'
        )

    def test_FollowedMineAPI_PUT_user_not_followed(self):
        response = app.request(
            '/api/followeds/mine/{0}'.format(self.profile2.id),
            method='PUT',
            data=json.dumps(
                dict(csrf_token=uuid.uuid4().hex, access_token=self.user1.access_token)
            ),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        self.assertEqual(
            json.loads(response.data, encoding='utf-8').get('msg'),
            u'user is not followed.'
        )
   
    def test_FollowingMineAPI_GET(self):
        response = app.request(
            '/api/followings/mine?access_token={0}'.format(self.user1.access_token),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        profiles = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(profiles), 0)
       
        User.signin(self.user2) 
        request_data = json.dumps(
            dict(
                access_token=self.user2.access_token,
                csrf_token=uuid.uuid4().hex,
                id=self.profile1.id
            )
        ) 
        app.request('/api/followeds/mine', method='POST', data=request_data, 
            headers={'Content-Type': 'application/json'})
        User.signout(self.user2)

        response = app.request(
            '/api/followings/mine?access_token={0}'.format(self.user1.access_token),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        profiles = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(profiles), 1)
        self.assertEqual(profiles[0].get('id'), self.profile2.id)

    def test_FollowingMineAPI_GET_missed_data(self):
        response = app.request('/api/followings/mine', method='GET')
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"access_token is required."
        )

    def test_FollowingMineAPI_GET_unauthorized(self):
        response = app.request(
            '/api/followings/mine?access_token={0}'.format(gen_uuid()),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u"请登录.")

if __name__ == '__main__':
    unittest.main()
