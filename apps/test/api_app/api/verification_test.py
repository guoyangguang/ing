# -*- coding: utf-8 -*-

import unittest
from datetime import datetime
import json
from web import Storage

from ....api_app.app import app
from ....model.verification import Verification 
from ....box import utils 
from ...helper import truncate_db


class VerificationAPITest(unittest.TestCase):

    def setUp(self):
        self.verification = Storage(
            phone='18612345678',
            verification_code=100000
        )

    def tearDown(self):
        truncate_db()

    def test_VerificationAPI_POST_missed_data_phone(self):
        data = {'csrf_token': utils.gen_uuid()}

        response = app.request(
            '/api/verifications',
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'phone is required.')

    def test_VerificationAPI_POST_bad_request(self):
        data = {
            'csrf_token': utils.gen_uuid(),
            'phone': 'phone' 
        }

        response = app.request(
            '/api/verifications',
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue('Phone' in response_data.get('msg')[0])

    def test_VerificationAPI_POST_post(self):
        data = {
            'csrf_token': utils.gen_uuid(),
            'phone': self.verification.phone
        }

        response = app.request(
            '/api/verifications',
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(response_data['id'], int)
        self.assertIsNone(response_data.get('verification_code'))
        self.assertIsNone(response_data['updated_at'])

    def test_VerificationAPI_POST_put(self):
        created = Verification.create(self.verification)
        data = {
            'csrf_token': utils.gen_uuid(),
            'phone': self.verification.phone
        }

        response = app.request(
            '/api/verifications',
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(response_data['id'], int)
        self.assertIsNone(response_data.get('verification_code'))
        self.assertTrue(response_data['updated_at'])


if __name__ == '__main__':
    unittest.main()
