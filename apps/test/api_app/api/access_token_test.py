# -*- coding: utf-8 -*-

import unittest
from datetime import datetime
import json
from web import Storage
import uuid

from ....api_app.app import app
from ....model.access_token import AccessToken
from ....box import utils 
from ....config import conf
from ...helper import truncate_db, signup_and_signin


class AccessTokenAPITest(unittest.TestCase):

    def setUp(self):
        self.user, self.profile = signup_and_signin()

    def tearDown(self):
        truncate_db()

    def test_AccessTokenMineAPI_POST_missed_site(self):
        data = Storage(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user.access_token,
            code='9eee3820r322j4qppij'
        )
        response = app.request(
            '/api/access_tokens/mine',
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'site is required.')

    def test_AccessTokenMineAPI_POST_401(self):
        data = Storage(
            csrf_token=uuid.uuid4().hex,
            access_token=utils.gen_uuid(),
            code='9eee3820r322j4qppij',
            site='1'
        )
        response = app.request(
            '/api/access_tokens/mine',
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_AddressMineAPI_POST_404_profile(self):
        data = Storage(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user.access_token,
            code='9eee3820r322j4qppij',
            site='1'
        )
        conf.db.update(
            'profiles', deleted_at=datetime.now(), where='id=$_id',
            vars=dict(_id=self.profile.id)
        )
        response = app.request(
            '/api/access_tokens/mine',
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'用户不存在.')

    def test_AddressMineAPI_POST_201(self):
        data = Storage(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user.access_token,
            code='9eee3820r322j4qppij',
            site='1'
        )
        response = app.request(
            '/api/access_tokens/mine',
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(response_data['id'], int)

    def test_AccessTokenMinePubStatusAPI_GET_missed_share(self):
        access_token = Storage(
            site='1',
            access_token='ei33rr3jiji23ed0',
            expires_in=64000,
            user_id='129309'
        ) 
        AccessToken.create(self.profile, access_token)

        response = app.request(
            '/api/access_tokens/mine/pubstatus?access_token={0}&sites=1'.format(
                self.user.access_token
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue('share' in response_data.get('msg'))

    def test_AccessTokenMinePubStatusAPI_GET_400_bad_share_data(self):
        access_token = Storage(
            site='1',
            access_token='ei33rr3jiji23ed0',
            expires_in=64000,
            user_id='129309'
        ) 
        AccessToken.create(self.profile, access_token)

        response = app.request(
            '/api/access_tokens/mine/pubstatus?access_token={0}&share={1}&sites=1'.format(
                self.user.access_token, 'test share' 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'分享的文字不正确.')

    def test_AccessTokenMinePubStatusAPI_GET_401(self):
        access_token = Storage(
            site='1',
            access_token='ei33rr3jiji23ed0',
            expires_in=64000,
            user_id='129309'
        ) 
        AccessToken.create(self.profile, access_token)

        response = app.request(
            '/api/access_tokens/mine/pubstatus?access_token={0}&share={1}&sites=1'.format(
                utils.gen_uuid(), 'test share, http://localhost/pubitems/1' 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_AccessTokenMinePubStatusAPI_GET_404_profile(self):
        access_token = Storage(
            site='1',
            access_token='ei33rr3jiji23ed0',
            expires_in=64000,
            user_id='129309'
        ) 
        AccessToken.create(self.profile, access_token)
        conf.db.update(
            'profiles', deleted_at=datetime.now(), where='id=$_id',
            vars=dict(_id=self.profile.id)
        )

        response = app.request(
            '/api/access_tokens/mine/pubstatus?access_token={0}&share={1}&sites=1'.format(
                self.user.access_token, 'test share, http://localhost/pubitems/1' 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'用户不存在.')

    def test_AccessTokenMinePubStatusAPI_GET_400_access_token(self):
        response = app.request(
            '/api/access_tokens/mine/pubstatus?access_token={0}&share={1}&sites=1'.format(
                self.user.access_token, 'test share, http://localhost/pubitems/1' 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'豆瓣未授权')

    def test_AccessTokenMinePubStatusAPI_GET_200(self):
        access_token = Storage(
            site='1',
            access_token='ei33rr3jiji23ed0',
            expires_in=64000,
            user_id='129309'
        ) 
        AccessToken.create(self.profile, access_token)

        response = app.request(
            '/api/access_tokens/mine/pubstatus?access_token={0}&share={1}&sites=1'.format(
                self.user.access_token, 'test share, http://localhost/pubitems/1' 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(response_data['id'], int)

if __name__ == '__main__':
    unittest.main()
