# -*- coding: utf-8 -*-

import unittest
from datetime import datetime, timedelta
import json
from web import Storage
import uuid
from ...helper import truncate_db, signup_and_signin
from ....api_app.app import app
from ....box import utils
from ....config.conf import db
from ....model.user import User
from ....model.profile import Profile 
from ....model.item import Item


class ItemAPITest(unittest.TestCase):

    def setUp(self):
        self.user, self.profile = signup_and_signin()

        user1 = Storage(
            email='user1@ing.com',
            phone='18612345679',
            password_digest='123password4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)

        self.item = dict(
            kind='1',
            name='<Norwegian Wood>',
            link='http://book.douban.com/subject/1046265/',
            buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
            percent_new='95',
            buy_price='1800',
            price='500',
            express_price='1000',
            if_bargain='0',
            state='1',
            city='1',
            img='cover.jpg',  # img pretend to be uploaded image.
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            ),
            description='The cover'
        )

        item1 = Storage(
            kind='11',
            name='muji shirt',
            link='http://book.douban.com/subject/1046266/',
            buy_at=datetime.strftime(
                datetime.now()-timedelta(days=60), '%Y/%m/%d'
            ),
            percent_new='90',
            buy_price='30000',
            price='15000',
            express_price='1000',
            if_bargain='1',
            state='3',
            city='43',
            img='cover.jpg',
            img_meta=dict(
                size=1024 * 2,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 275], 'thumb': [235, 185]}
            ),
            description='please send message to me'
        )
        self.item1 = Item.publish(
            Item.create(self.profile1, item1)
        )

    def tearDown(self):
        truncate_db()

    def test_ItemMineAPI_POST_missed_data_price(self):
        self.item['csrf_token'] = uuid.uuid4().hex
        self.item['access_token'] = self.user.access_token
        self.item.pop('price')
        response = app.request(
            '/api/items/mine',
            method='POST',
            data=self.item
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u'price' in response_data.get('msg'))

    def test_ItemMineAPI_POST_401(self):
        self.item['csrf_token'] = uuid.uuid4().hex
        self.item['access_token'] = utils.gen_uuid()
        response = app.request(
            '/api/items/mine',
            method='POST',
            data=self.item
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_ItemMineAPI_POST_bad_request_invalid_img(self):
        self.item['csrf_token'] = uuid.uuid4().hex
        self.item['access_token'] = self.user.access_token
        response = app.request(
            '/api/items/mine',
            method='POST',
            data=self.item
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            u'请上传图片.' in response_data.get('msg')[0]
        )

    @unittest.skip('cgi.FieldStorage is used in the api.')
    def test_ItemMineAPI_POST_not_found_profile(self):
        self.item['csrf_token'] = uuid.uuid4().hex
        self.item['access_token'] = self.user.access_token
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=self.profile.id)
        )

        response = app.request(
            '/api/items/mine',
            method='POST',
            data=self.item,
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'The profile is not found.')

    @unittest.skip('cgi.FieldStorage is used in the api.')
    def test_ItemMineAPI_POST(self):
        self.item['csrf_token'] = uuid.uuid4().hex
        self.data['access_token'] = self.user.access_token
        response = app.request(
            '/api/items/mine',
            method='POST',
            data=self.data
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['status'], utils.item_status[1])

    def test_ItemMineAPI_GET_missed_data_page(self):
        item = Item.create(self.profile, Storage(self.item))

        response = app.request(
            '/api/items/mine?access_token={0}'.format(
                self.user.access_token
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'access_token, page is required.')

    def test_ItemMineAPI_GET_401(self):
        item = Item.create(self.profile, Storage(self.item))

        response = app.request(
            '/api/items/mine?access_token={0}&page={1}'.format(
                utils.gen_uuid(), '1'
            ),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'请登录.')

    def test_ItemMineAPI_GET_404_profile(self):
        item = Item.create(self.profile, Storage(self.item))
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=self.profile.id)
        )

        response = app.request(
            '/api/items/mine?access_token={0}&page={1}'.format(
                self.user.access_token, '1'
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'用户不存在.')

    def test_ItemMineAPI_GET(self):
        item = Item.create(self.profile, Storage(self.item))

        response = app.request(
            '/api/items/mine?access_token={0}&page={1}'.format(
                self.user.access_token, '1'
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(response_data, list)
        self.assertEqual(len(response_data), 1)
        self.assertEqual(response_data[0]['id'], item.id)
        self.assertEqual(response_data[0]['profile_id'], self.profile.id)
        self.assertEqual(response_data[0]['status'], 1)

    def test_ItemMineShowAPI_GET_missed_data_access_token(self):
        item = Item.publish(
            Item.create(self.profile, Storage(self.item))
        )

        response = app.request(
            "/api/items/mine/{0}".format(item.id),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue('access_token' in response_data.get('msg'))

    def test_ItemMineShowAPI_GET_401(self):
        item = Item.publish(
            Item.create(self.profile, Storage(self.item))
        )

        response = app.request(
            "/api/items/mine/{0}?access_token={1}".format(
                item.id,
                utils.gen_uuid()
            ),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_ItemMineShowAPI_GET_404_item(self):
        item = Item.publish(
            Item.create(self.profile, Storage(self.item))
        )

        response = app.request(
            "/api/items/mine/{0}?access_token={1}".format(
                item.id + 1,
                self.user.access_token
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'物品不存在.')

    def test_ItemMineShowAPI_GET_404_profile(self):
        item = Item.publish(
            Item.create(self.profile, Storage(self.item))
        )
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=self.profile.id)
        )

        response = app.request(
            "/api/items/mine/{0}?access_token={1}".format(
                item.id,
                self.user.access_token
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'用户不存在.')

    def test_ItemMineShowAPI_GET_400_profile_id(self):
        item = Item.publish(
            Item.create(self.profile, Storage(self.item))
        )

        response = app.request(
            "/api/items/mine/{0}?access_token={1}".format(
                self.item1.id,
                self.user.access_token
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'不是您的物品.')

    def test_ItemMineShowAPI_GET_200(self):
        item = Item.publish(
            Item.create(self.profile, Storage(self.item))
        )

        response = app.request(
            "/api/items/mine/{0}?access_token={1}".format(
                item.id,
                self.user.access_token
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['id'], item.id)
        self.assertFalse(response_data['is_liked'])
        self.assertFalse(response_data['like_count'])
        self.assertEqual(len(response_data['comments']), 0)
        self.assertEqual(len(response_data['itemimgs']), 0)

    def test_PubItemsAPI_GET_missed_data_page(self):
        response = app.request(
            '/api/pubitems?access_token={0}'.format(self.user.access_token),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"page is required." in response_data['msg'])

    def test_PubItemsAPI_GET_401(self):
        response = app.request(
            '/api/pubitems?access_token={0}&page={1}'.format(
                utils.gen_uuid(), '1'
            ),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data['msg'], u"请登录.")

    def test_PubItemsAPI_GET_404_profile(self):
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=self.profile.id)
        )

        response = app.request(
            '/api/pubitems?access_token={0}&page={1}'.format(
                self.user.access_token, '1'
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data['msg'], u"用户不存在.")

    def test_PubItemsAPI_GET(self):
        response = app.request(
            '/api/pubitems?access_token={0}&page={1}'.format(
                self.user.access_token, '1'
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 1)

    def test_PubItemKindInCityAPI_GET_missed_data_kind(self):
        query_str = "access_token={0}&city={1}&page={2}".format(
            self.user.access_token, '43', '1'
        )
        response = app.request(
            "/api/pubitems/kind_in_city?" + query_str,
            method='GET'
        )
        json.loads(response.data, encoding='utf-8')
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"kind", response_data['msg'])

    def test_PubItemKindInCityAPI_GET_401(self):
        query_str = "access_token={0}&city={1}&page={2}&kind={3}".format(
             utils.gen_uuid(), '43', '1', '11'
        )
        response = app.request(
            "/api/pubitems/kind_in_city?" + query_str,
            method='GET'
        )
        json.loads(response.data, encoding='utf-8')
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u"请登录.")

    def test_PubItemKindInCityAPI_GET_404_profile(self):
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=self.profile.id)
        )

        query_str = "access_token={0}&city={1}&page={2}&kind={3}".format(
             self.user.access_token, '43', '1', '11'
        )
        response = app.request(
            "/api/pubitems/kind_in_city?" + query_str,
            method='GET'
        )
        json.loads(response.data, encoding='utf-8')
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u"用户不存在.")

    def test_PubItemKindInCityAPI_GET_404_kind(self):
        query_str = "access_token={0}&city={1}&page={2}&kind={3}".format(
             self.user.access_token, '43', '1', '19'
        )
        response = app.request(
            "/api/pubitems/kind_in_city?" + query_str,
            method='GET'
        )
        json.loads(response.data, encoding='utf-8')
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u"分类不存在.")

    def test_PubItemKindInCityAPI_GET_404_city(self):
        query_str = "access_token={0}&city={1}&page={2}&kind={3}".format(
             self.user.access_token, '10000', '1', '11'
        )
        response = app.request(
            "/api/pubitems/kind_in_city?" + query_str,
            method='GET'
        )
        json.loads(response.data, encoding='utf-8')
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u"城市不存在.")

    def test_PubItemKindInCityAPI_GET(self):
        query_str = "access_token={0}&city={1}&page={2}&kind={3}".format(
             self.user.access_token, '43', '1', '11'
        )
        response = app.request(
            "/api/pubitems/kind_in_city?" + query_str,
            method='GET'
        )
        json.loads(response.data, encoding='utf-8')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 1)
        self.assertEqual(response_data[0]['id'], self.item1.id)

    def test_PubItemSearchInCityAPI_GET_missed_data_query(self):
        query_str = "access_token={0}&city={1}&page={2}".format(
            self.user.access_token, '43', '1'
        )
        response = app.request(
            "/api/pubitems/search_in_city?" + query_str,
            method='GET'
        )
        json.loads(response.data, encoding='utf-8')
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(u"query", response_data['msg'])

    def test_PubItemSearchInCityAPI_GET_401(self):
        query_str = "access_token={0}&query={1}&city={2}&page={3}".format(
            utils.gen_uuid(), 'shirt', '43', '1'
        )
        response = app.request(
            "/api/pubitems/search_in_city?" + query_str,
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'请登录.')

    def test_PubItemSearchInCityAPI_GET_404_profile(self):
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=self.profile.id)
        )

        query_str = "access_token={0}&query={1}&city={2}&page={3}".format(
            self.user.access_token, 'shirt', '43', '1'
        )
        response = app.request(
            "/api/pubitems/search_in_city?" + query_str,
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'用户不存在.')

    def test_PubItemSearchInCityAPI_GET_400_query(self):
        query_str = "access_token={0}&query={1}&city={2}&page={3}".format(
            self.user.access_token, '', '43', '1'
        )
        response = app.request(
            "/api/pubitems/search_in_city?" + query_str,
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'请输入需要搜索的物品.')

    @unittest.skip('elasticsearch is not available when testing')
    def test_PubItemSearchInCityAPI_GET_404_city(self):
        query_str = "access_token={0}&query={1}&city={2}&page={3}".format(
            self.user.access_token, 'shirt', '704', '1'
        )
        response = app.request(
            "/api/pubitems/search_in_city?" + query_str,
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'城市不存在.')

    @unittest.skip('elasticsearch is not available when testing')
    def test_PubItemSearchInCityAPI_GET(self):
        item = Item.publish(
            Item.create(self.profile, Storage(self.item))
        )

        query_str = "access_token={0}&query={1}&kind={2}&city={3}&page={4}".format(
            self.user.access_token, ' ', '1', '1', '1'
        )
        response = app.request(
            "/api/pubitems/search_in_city?" + query_str,
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data[0]['id'], item.id)

    def test_PubItemShowAPI_GET_missed_data_access_token(self):
        response = app.request(
            '/api/pubitems/{0}'.format(self.item1.id),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"access_token is required."
        )

    def test_PubItemShowAPI_GET_401(self):
        response = app.request(
            '/api/pubitems/{0}?access_token={1}'.format(
                self.item1.id, utils.gen_uuid() 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'请登录.'
        )

    def test_PubItemShowAPI_GET_404_item_not_found(self):
        response = app.request(
            '/api/pubitems/{0}?access_token={1}'.format(
                self.item1.id + 1, self.user.access_token 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'The item is not found.'
        )

    def test_PubItemShowAPI_GET_403_item_not_available(self):
        item = Item.create(self.profile, Storage(self.item))

        response = app.request(
            '/api/pubitems/{0}?access_token={1}'.format(
                item.id, self.user.access_token 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'The item is not available.'
        )

    def test_PubItemShowAPI_GET_404_profile_not_found(self):
        db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=self.profile.id)
        )

        response = app.request(
            '/api/pubitems/{0}?access_token={1}'.format(
                self.item1.id, self.user.access_token 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'The profile is not found.'
        )

    def test_PubItemshowAPI_GET(self):
        response = app.request(
            '/api/pubitems/{0}?access_token={1}'.format(
                self.item1.id, self.user.access_token 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['id'], self.item1.id)
        self.assertEqual(response_data['profile']['id'], self.profile1.id)
        self.assertFalse(response_data['is_followed'])
        self.assertFalse(response_data['is_liked'])
        self.assertEqual(len(response_data['itemimgs']), 0)
        self.assertEqual(len(response_data['comments']), 0)

    def test_PubItemsHerAPI_GET_missed_data_access_token(self):
        response = app.request(
            '/api/pubitems/her?profile_id={0}&page={1}'.format(
                self.profile1.id, '1' 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue('access_token' in response_data.get('msg'))

    def test_PubItemsHerAPI_GET_401(self):
        response = app.request(
            '/api/pubitems/her?access_token={0}&profile_id={1}&page={2}'.format(
                utils.gen_uuid(), self.profile1.id, '1' 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_PubItemsHerAPI_GET_404_profile(self):
        response = app.request(
            '/api/pubitems/her?access_token={0}&profile_id={1}&page={2}'.format(
                self.user.access_token, self.profile1.id + 1, '1' 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'用户不存在.')

    def test_PubItemsHerAPI_GET(self):
        response = app.request(
            '/api/pubitems/her?access_token={0}&profile_id={1}&page={2}'.format(
                self.user.access_token, self.profile1.id, 1
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 1)
        self.assertEqual(response_data[0]['id'], self.item1.id)

    def test_PubItemsHerShowAPI_GET_missed_data_profile_id(self):
        response = app.request(
            '/api/pubitems/her/{0}?access_token={1}'.format(
                self.item1.id, self.user.access_token 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue('profile_id' in response_data.get('msg'))

    def test_PubItemsHerShowAPI_GET_401(self):
        response = app.request(
            '/api/pubitems/her/{0}?access_token={1}&profile_id={2}'.format(
                self.item1.id, utils.gen_uuid(), self.profile1.id 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_PubItemsHerShowAPI_GET_404_item(self):
        response = app.request(
            '/api/pubitems/her/{0}?access_token={1}&profile_id={2}'.format(
                self.item1.id + 1, self.user.access_token, self.profile1.id 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'物品不存在.')

    def test_PubItemsHerShowAPI_GET_403_item_not_available(self):
        item = Item.create(self.profile1, Storage(self.item))

        response = app.request(
            '/api/pubitems/her/{0}?access_token={1}&profile_id={2}'.format(
                item.id, self.user.access_token, self.profile1.id 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'物品未公开.')

    def test_PubItemsHerShowAPI_GET_400_item_not_hers(self):
        item = Item.publish( 
            Item.create(self.profile, Storage(self.item))
        )

        response = app.request(
            '/api/pubitems/her/{0}?access_token={1}&profile_id={2}'.format(
                item.id, self.user.access_token, self.profile1.id 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'不是她的物品.')

    def test_PubItemsHerShowAPI_GET(self):
        response = app.request(
            '/api/pubitems/her/{0}?access_token={1}&profile_id={2}'.format(
                self.item1.id, self.user.access_token, self.profile1.id 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['id'], self.item1.id)
        self.assertFalse(response_data['is_liked'])
        self.assertEqual(len(response_data['comments']), 0)
        self.assertEqual(len(response_data['itemimgs']), 0)

if __name__ == '__main__':
    unittest.main()
