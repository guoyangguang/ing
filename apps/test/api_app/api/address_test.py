# -*- coding: utf-8 -*-

import unittest
from datetime import datetime
import json
from web import Storage
import uuid

from ....api_app.app import app
from ....model.address import Address
from ....box import utils 
from ....config import conf
from ...helper import truncate_db, signup_and_signin


class AddressAPITest(unittest.TestCase):

    def setUp(self):
        self.user1, self.profile1 = signup_and_signin()
        self.data = dict(
            recipient='user',
            phone='18611440935',
            state='1',
            city='1',
            detail='MiLiangKuHuTong No.4, DiAnMenNei Street',
            postcode='100000'
        )

    def tearDown(self):
        truncate_db()

    def test_AddressMineAPI_POST_missed_data_recipient(self):
        self.data['csrf_token'] = uuid.uuid4().hex
        self.data['access_token'] = self.user1.access_token
        self.data.pop('recipient')

        response = app.request(
            '/api/addresses/mine',
            method='POST',
            data=json.dumps(self.data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'recipient is required.')

    def test_AddressMineAPI_POST_401(self):
        self.data['csrf_token'] = uuid.uuid4().hex
        self.data['access_token'] = utils.gen_uuid() 

        response = app.request(
            '/api/addresses/mine',
            method='POST',
            data=json.dumps(self.data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_AddressMineAPI_POST_400_invalid_state(self):
        self.data['csrf_token'] = uuid.uuid4().hex
        self.data['access_token'] = self.user1.access_token
        self.data['state'] = 'guangdong'

        response = app.request(
            '/api/addresses/mine',
            method='POST',
            data=json.dumps(self.data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            utils.invalid_msgs['one_of'].lower() in response_data.get('msg')[1]
        )

    def test_AddressMineAPI_POST_404_profile(self):
        conf.db.update(
            'profiles', deleted_at=datetime.now(), where='id=$_id',
            vars=dict(_id=self.profile1.id)
        )
        self.data['csrf_token'] = uuid.uuid4().hex
        self.data['access_token'] = self.user1.access_token

        response = app.request(
            '/api/addresses/mine',
            method='POST',
            data=json.dumps(self.data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data.get('msg'), u'The profile is not found.')

    def test_AddressMineAPI_POST(self):
        self.data['csrf_token'] = uuid.uuid4().hex
        self.data['access_token'] = self.user1.access_token

        response = app.request(
            '/api/addresses/mine',
            method='POST',
            data=json.dumps(self.data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertIsInstance(response_data['created_at'], unicode)

    def test_AddressMineAPI_GET_missed_data_access_token(self):
        response = app.request(
            '/api/addresses/mine',
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'access_token is required.')

    def test_AddressMineAPI_GET_401(self):
        response = app.request(
            '/api/addresses/mine?access_token={0}'.format(utils.gen_uuid()),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_AddressMineAPI_GET_404_profile(self):
        conf.db.update(
            'profiles', deleted_at=datetime.now(), where='id=$_id',
            vars=dict(_id=self.profile1.id)
        )

        response = app.request(
            '/api/addresses/mine?access_token={0}'.format(self.user1.access_token),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'The profile is not found.')

    def test_AddressMineAPI_GET(self):
        Address.create(self.profile1, Storage(self.data))

        response = app.request(
            '/api/addresses/mine?access_token={0}'.format(self.user1.access_token),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 1)
        self.assertIsInstance(response_data[0]['created_at'], unicode)


if __name__ == '__main__':
    unittest.main()
