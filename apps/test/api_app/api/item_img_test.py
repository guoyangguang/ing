# -*- coding: utf-8 -*-

import unittest
from datetime import datetime, timedelta
import json
from web import Storage
import uuid
from ...helper import truncate_db, signup_and_signin
from ....api_app.app import app
from ....config import conf 
from ....box import utils
from ....model.user import User
from ....model.profile import Profile
from ....model.item import Item
from ....model.item_img import ItemImg

class ItemImgAPITest(unittest.TestCase):

    def setUp(self):
        self.user, self.profile = signup_and_signin()
        item = Storage(
            kind='1',
            name='<Norwegian Wood>',
            link='http://book.douban.com/subject/1046265/',
            buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
            percent_new='95',
            buy_price='1800',
            price='500',
            express_price='1000',
            if_bargain='0',
            state='1',
            city='1',
            img='cover.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [425, 500], 'thumb': [235, 300]}
            ),
            description='The cover'
        )
        self.item = Item.create(self.profile, item)

        self.data = dict(
            img='note.jpg',
            img_meta=dict(
                size=1024 * 2,
                filetype='image/jpeg'
            ),
            description='The note',
            is_note='1'
        )
        user1 = Storage(
            email='user2@ing.com',
            phone='18612345679',
            password_digest='123pw4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)
        item1 = Storage(
            kind='11',
            name='muji shirt',
            link='http://book.douban.com/subject/1046266/',
            buy_at=datetime.strftime(
                datetime.now()-timedelta(days=60), '%Y/%m/%d'
            ),
            percent_new='90',
            buy_price='30000',
            price='15000',
            express_price='1000',
            if_bargain='1',
            state='1',
            city='1',
            img='cover.jpg',
            img_meta=dict(
                size=1024 * 2,
                filetype='image/jpeg',
                vdimensions={'detail': [425, 500], 'thumb': [235, 300]}
            ),
            description='please send message to me'
        )
        self.item1 = Item.create(self.profile1, item1)

    def tearDown(self):
        truncate_db()

    def test_ItemMineItemImgAPI_POST_missed_data_access_token(self):
        self.data.pop('description')
        self.data.pop('is_note')
        self.data['csrf_token'] = uuid.uuid4().hex
        response = app.request(
            '/api/items/mine/{0}/itemimgs'.format(self.item.id),
            method='POST',
            data=self.data
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u'csrf_token, access_token, img is required.'
        )

    def test_ItemMineItemImgAPI_POST_401(self):
        self.data.pop('description')
        self.data.pop('is_note')
        self.data['access_token'] = utils.gen_uuid()
        self.data['csrf_token'] = uuid.uuid4().hex
        response = app.request(
            '/api/items/mine/{0}/itemimgs'.format(self.item.id),
            method='POST',
            data=self.data
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_ItemMineItemImgAPI_POST_400_invalid_img(self):
        self.data.pop('description')
        self.data.pop('is_note')
        self.data['access_token'] = self.user.access_token 
        self.data['csrf_token'] = uuid.uuid4().hex
        response = app.request(
            '/api/items/mine/{0}/itemimgs'.format(self.item.id),
            method='POST',
            data=self.data
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg')[0], u'Please upload an image.')

    @unittest.skip('cgi.FieldStorage is used in the api.')
    def test_ItemMineItemImgAPI_POST_404_item_not_found(self):
        self.data.pop('description')
        self.data.pop('is_note')
        self.data['access_token'] = self.user.access_token 
        self.data['csrf_token'] = uuid.uuid4().hex
        response = app.request(
            '/api/items/mine/{0}/itemimgs'.format(self.item.id + 1),
            method='POST',
            data=self.data
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'The item is not found.')

    @unittest.skip('cgi.FieldStorage is used in the api.')
    def test_ItemMineItemImgAPI_POST_403_others_item(self):
        pass

    @unittest.skip('cgi.FieldStorage is used in the api.')
    def test_ItemMineItemImgAPI_POST_403_item_published(self):
        pass

    @unittest.skip('cgi.FieldStorage is used in the api.')
    def test_ItemMineItemImgAPI_POST(self):
        pass

    def test_ItemMineItemImgAPI_PUT_missed_data_description(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item, Storage(self.data))

        request_data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            is_note='1'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(self.item.id, note.id),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'description is required.')

    def test_ItemMineItemImgAPI_PUT_401(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item, Storage(self.data))

        request_data = dict(
            access_token=utils.gen_uuid(),
            csrf_token = uuid.uuid4().hex,
            is_note='1',
            description='The note'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(self.item.id, note.id),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )

        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'请登录.')

    def test_ItemMineItemImgAPI_PUT_400(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item, Storage(self.data))

        request_data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            is_note='True',
            description='The note'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(self.item.id, note.id),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )

        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            utils.invalid_msgs['one_of'].lower() in response_data['msg'][0]
        )

    def test_ItemMineItemImgAPI_PUT_404_profile(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item, Storage(self.data))
        conf.db.update(
            'profiles', deleted_at=datetime.now(),
            where='id=$_id', vars=dict(_id=self.profile.id)
        )

        request_data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            is_note='1',
            description='The note'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(self.item.id, note.id),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], 'The profile is not found.')

    def test_ItemMineItemImgAPI_PUT_404_item(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item, Storage(self.data))

        request_data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            is_note='1',
            description='The note'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(
                self.item.id + self.item1.id, note.id
            ),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], 'The item is not found.')

    def test_ItemMineItemImgAPI_PUT_403_item_belongs_to_other(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item, Storage(self.data))

        request_data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            is_note='1',
            description='The note'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(
                self.item1.id, note.id
            ),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )

        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], 'The item does not belong to you.')

    def test_ItemMineItemImgAPI_PUT_403_published(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item, Storage(self.data))
        Item.publish(self.item)

        request_data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            is_note='1',
            description='The note'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(
                self.item.id, note.id
            ),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )

        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], 'The item has been published.')

    def test_ItemMineItemImgAPI_PUT_404_itemimg(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item, Storage(self.data))

        request_data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            is_note='1',
            description='The note'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(
                self.item.id, note.id + 1
            ),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )

        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], u'The item img is not found.')

    def test_ItemMineItemImgAPI_PUT_403_img_not_belong_to_item(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item1, Storage(self.data))

        request_data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            is_note='1',
            description='The note'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(
                self.item.id, note.id
            ),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )

        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['msg'], 'The img does not belong to the item.')

    def test_ItemMineItemImgAPI_PUT(self):
        self.data.pop('description')
        self.data.pop('is_note')
        note = ItemImg.create(self.item, Storage(self.data))

        request_data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            is_note='1',
            description='The note'
        )
        response = app.request(
            '/api/items/mine/{0}/itemimgs/{1}'.format(
                self.item.id, note.id
            ),
            headers={'Content-Type': 'application/json'},
            data = json.dumps(request_data),
            method='PUT'
        )

        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data['updated_at'])
        self.assertTrue(response_data['thumb_img'])
        self.assertTrue(response_data['description'])


if __name__ == '__main__':
    unittest.main()
