# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime, timedelta
import json
import uuid

from ....api_app.app import app
from ....model.user import User
from ....model.profile import Profile
from ....model.item import Item
from ....model.comment import Comment
from ....box import utils 
from ....config import conf
from ...helper import truncate_db, signup_and_signin


class CommentAPITest(unittest.TestCase):

    def setUp(self):
        # user create an item, user1 and user comment it
        user = Storage(
            email='user@book.com',
            phone='18612345678',
            password_digest='123password4'
        )
        self.user = User.signup(user)
        self.profile = Profile.create_default_profile(self.user)
        item = Storage(
            kind='1',
            name='<Norwegian Wood>',
            link='http://book.douban.com/subject/1046265/',
            buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
            percent_new='95',
            buy_price='1800',
            price='500',
            express_price='1000',
            if_bargain='0',
            state='1',
            city='1',
            img='cover1.jpg',
            img_meta=dict(
                size=1024,
                filetype='image/jpeg',
                vdimensions={'detail': [375, 400], 'thumb': [235, 300]}
            ),
            description='The cover'
        )
        self.item = Item.publish(Item.create(self.profile, item))

        self.user1, self.profile1 = signup_and_signin()

        self.comment = Storage(body='it is nice')

    def tearDown(self):
        truncate_db()

    def test_PubItemCommentAPI_POST_missed_data(self):
        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user1.access_token
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'body is required.')

    def test_PubItemCommentAPI_POST_401_Unauthorized(self):
        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=utils.gen_uuid(),
            body=self.comment.body
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_PubItemCommentAPI_POST_404_item_not_found(self):
        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user1.access_token,
            body=self.comment.body
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id + 1),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data.get('msg'), u'The item is not found.')

    def test_PubItemCommentAPI_POST_403_item_not_published(self):
        conf.db.update(
            'items', status=1, where='id=$_id', vars=dict(_id=self.item.id)
        )
        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user1.access_token,
            body=self.comment.body
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data.get('msg'), u'The item is not published.')

    def test_PubItemCommentAPI_POST_404_profile_not_found(self):
        conf.db.update(
            'profiles', deleted_at=datetime.now(), where='id=$_id',
            vars=dict(_id=self.profile1.id)
        )
        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user1.access_token,
            body=self.comment.body
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data.get('msg'), u'The profile is not profile.')

    def test_PubItemCommentAPI_POST_400_invalid_comment(self):
        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user1.access_token,
            body=''
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            utils.invalid_msgs['required'].lower() in response_data.get('msg')[0]
        )

    def test_PubItemCommentAPI_POST_400_invalid_reply(self):
        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user1.access_token,
            body=self.comment.body,
            comment_id='-1'
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            utils.invalid_msgs['positive_int'].lower() in response_data.get('msg')[0]
        )

    def test_PubItemCommentAPI_POST_404_comment_not_found(self):
        profile_comment = Comment.create(
            self.profile, self.item, Storage(body='any one?')
        )

        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user1.access_token,
            body=self.comment.body,
            comment_id=str(profile_comment.id + 1)
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(
            response_data.get('msg'), 'The comment is not found.'
        )

    def test_PubItemCommentAPI_POST_comment(self):
        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user1.access_token,
            body=self.comment.body
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['profile']['name'], self.profile1.name)
        self.assertEqual(response_data['body'], self.comment.body)
        self.assertIsInstance(response_data['created_at'], unicode)

    def test_PubItemCommentAPI_POST_reply(self):
        profile_comment = Comment.create(
            self.profile, self.item, Storage(body='any one?')
        )

        data = dict(
            csrf_token=uuid.uuid4().hex,
            access_token=self.user1.access_token,
            body=self.comment.body,
            comment_id=str(profile_comment.id)
        )
        response = app.request(
            '/api/pubitems/{0}/comments'.format(self.item.id),
            method='POST',
            data=json.dumps(data), 
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data['profile']['name'], self.profile1.name)
        self.assertEqual(response_data['body'], self.comment.body)
        self.assertIsInstance(response_data['created_at'], unicode)
        self.assertEqual(response_data['comment']['id'], profile_comment.id)


if __name__ == '__main__':
    unittest.main()
