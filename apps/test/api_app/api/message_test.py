#-*- coding: utf-8 -*-

import unittest
import json
from web import storage
import uuid

from ...helper import truncate_db, signup_and_signin 
from ....model.user import User
from ....model.profile import Profile
from ....model.message import Message
from ....api_app.app import app
from ....box.utils import gen_uuid


class MessageAPITest(unittest.TestCase): 
    
    def setUp(self):
        self.current_user, self.current_profile = signup_and_signin()
        user2 = storage(
            email='user2@ing.com',
            phone='18612345679',
            password_digest='123pw4'
        )
        self.user2 = User.signup(user2)
        self.profile2 = Profile.create_default_profile(self.user2)

        msg1 = storage(body='current profile msg1 profile2')
        msg2 = storage(body='profile2 msg2 current profile')
        self.msg1 = Message.send_message(
            self.current_profile, msg1, self.profile2
        )
        self.msg2 = Message.send_message(
            self.profile2, msg2, self.current_profile
        )

    def tearDown(self):
        truncate_db()

    def test_MessagesContactsAPI_GET(self):
        response = app.request(
            '/api/messages/contacts/mine?access_token={0}'.format(
                self.current_user.access_token
            ),
            method='GET' 
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        contacts = json.loads(response.data)
        self.assertEqual(len(contacts), 1)
        self.assertEqual(contacts[0]['id'], self.profile2.id)

    def test_MessagesContactsAPI_GET_missed_data(self):
        response = app.request(
            '/api/messages/contacts/mine',
            method='GET' 
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        msg = json.loads(response.data).get('msg')
        self.assertEqual(msg, "access_token is required.")

    def test_MessagesContactsAPI_GET_unauthorized(self):
        response = app.request(
            '/api/messages/contacts/mine?access_token={0}'.format(gen_uuid()),
            method='GET' 
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        msg = json.loads(response.data).get('msg')
        self.assertEqual(msg, u"请登录.")

    def test_MessagesUnreadMineAPI_GET(self):
        response = app.request(
            '/api/messages/unread/mine?access_token={0}&page={1}'.format(
                self.current_user.access_token,
                1
            ),
            method='GET' 
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        msgs = json.loads(response.data)
        self.assertEqual(len(msgs), 1)
        self.assertEqual(msgs[0]['id'], self.msg2.id)
        self.assertFalse(msgs[0]['read'], False)
        self.assertEqual(msgs[0]['profile']['id'], self.profile2.id)

    def test_MessagesUnreadMineAPI_GET_missed_data(self):
        response = app.request(
            '/api/messages/unread/mine?access_token={0}'.format(
                self.current_user.access_token,
            ),
            method='GET' 
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        msg = json.loads(response.data).get('msg')
        self.assertEqual(msg, u"access_token, page is required.")

    def test_MessagesUnreadMineAPI_GET_unauthorized(self):
        response = app.request(
            '/api/messages/unread/mine?access_token={0}&page={1}'.format(
                gen_uuid(),
                1
            ),
            method='GET' 
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        msg = json.loads(response.data).get('msg')
        self.assertEqual(msg, u"请登录.")

    def test_MessagesBetweenAPI_GET(self):
        response = app.request(
            '/api/messages/between?access_token={0}&the_other={1}&page={2}'.format(
                self.current_user.access_token, self.profile2.id, 1
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        msgs = json.loads(response.data)
        self.assertIsInstance(msgs, list)
        self.assertEqual(len(msgs), 2)
    
    def test_MessagesBetweenAPI_GET_missed_data(self):
        response = app.request(
            '/api/messages/between?access_token={0}&page={1}'.format(
                self.current_user.access_token, 1 
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(
            response_data.get('msg'),
            u"access_token, the_other, page is required."
        )
    
    def test_MessagesBetweenAPI_GET_unauthorized(self):
        response = app.request(
            '/api/messages/between?access_token={0}&the_other={1}&page={2}'.format(
                gen_uuid(), self.profile2.id, 1
            ),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data.get('msg'), u'请登录.')
    
    def test_MessagesBetweenAPI_GET_bad_request(self):
        response = app.request(
            '/api/messages/between?access_token={0}&the_other={1}&page={2}'.format(
                self.current_user.access_token, self.current_profile.id, 1
            ),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data.get('msg'), u'Bad request.')

    def test_MessagesBetweenAPI_GET_not_found(self):
        response = app.request(
            '/api/messages/between?access_token={0}&the_other={1}&page={2}'.format(
                self.current_user.access_token, self.profile2.id + 1, 1
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(response_data.get('msg'), u'The user is not found')

    def test_MessagesBetweenAPI_POST(self): 
        request_data = json.dumps(dict(
            body= 'msg', 
            recipient_id=self.profile2.id, 
            csrf_token = uuid.uuid4().hex,
            access_token=self.current_user.access_token
        ))
        response = app.request(
            '/api/messages/between',
            method='POST',
            headers={'Content-Type': 'application/json'}, 
            data=request_data
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        message = json.loads(response.data)
        self.assertIsInstance(message.get('id'), int)
        self.assertEqual(
            message.get('profile_id'), self.current_profile.id
        )
        self.assertEqual(message.get('recipient_id'), self.profile2.id)
        self.assertFalse(message.get('read'))
        self.assertIsNone(message.get('read_at'))
        self.assertEqual(message['profile']['id'], self.current_profile.id)

    def test_MessagesBetween_POST_missed_data(self): 
        request_data = json.dumps(dict(
            body= 'msg', 
            csrf_token = uuid.uuid4().hex,
            access_token=self.current_user.access_token
        ))
        response = app.request(
            '/api/messages/between',
            method='POST',
            headers={'Content-Type': 'application/json'}, 
            data=request_data
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        msg = u'recipient_id is required.'
        self.assertEqual(response_data.get('msg'), msg)

    def test_MessagesBetween_POST_unauthorized(self): 
        request_data = json.dumps(dict(
            body= 'msg', 
            recipient_id=self.profile2.id, 
            csrf_token = uuid.uuid4().hex,
            access_token=gen_uuid()
        ))
        response = app.request(
            '/api/messages/between',
            method='POST',
            headers={'Content-Type': 'application/json'}, 
            data=request_data
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        msg = u'请登录.'
        self.assertEqual(response_data.get('msg'), msg)

    def test_MessagesBetween_POST_user_not_found(self): 
        request_data = json.dumps(dict(
            body= 'msg', 
            recipient_id=self.profile2.id + 1, 
            csrf_token = uuid.uuid4().hex,
            access_token=self.current_user.access_token
        ))
        response = app.request(
            '/api/messages/between',
            method='POST',
            headers={'Content-Type': 'application/json'}, 
            data=request_data
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        msg = u'The user receiving message is not found.'
        self.assertEqual(response_data.get('msg'), msg)

    def test_MessagesBetween_POST_forbidden(self): 
        request_data = json.dumps(dict(
            body= 'msg', 
            recipient_id=self.current_profile.id, 
            csrf_token = uuid.uuid4().hex,
            access_token=self.current_user.access_token
        ))
        response = app.request(
            '/api/messages/between',
            method='POST',
            headers={'Content-Type': 'application/json'}, 
            data=request_data
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        msg = u'forbidden.'
        self.assertEqual(response_data.get('msg'), msg)

    def test_MessagesBetween_POST_bad_request(self): 
        request_data = json.dumps(dict(
            body= '', 
            recipient_id=self.profile2.id, 
            csrf_token = uuid.uuid4().hex,
            access_token=self.current_user.access_token
        ))
        response = app.request(
            '/api/messages/between',
            method='POST',
            headers={'Content-Type': 'application/json'}, 
            data=request_data
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data)
        self.assertEqual(len(response_data['msg']), 1)
    

if __name__ == '__main__':
    unittest.main()
