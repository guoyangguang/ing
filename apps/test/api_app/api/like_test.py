# -*- coding: utf-8 -*-

import unittest
from web import Storage
from datetime import datetime, timedelta
import json
import uuid
from ....api_app.app import app
from ....config import conf
from ....box import utils
from ....model.user import User
from ....model.profile import Profile
from ....model.item import Item
from ....model.like import Like
from ...helper import truncate_db, signup_and_signin
 

class LikeAPITest(unittest.TestCase):
    
    def setUp(self):
        self.user, self.profile = signup_and_signin() 

        user1 = Storage(
            email='user1@book.com',
            phone='18612345679',
            password_digest='123password4'
        )
        self.user1 = User.signup(user1)
        self.profile1 = Profile.create_default_profile(self.user1)

        item1 = Storage(
            kind='1',
            name='<Norwegian Wood>',
            link='http://book.douban.com/subject/1046265/',
            buy_at=datetime.strftime(datetime.now()-timedelta(days=30), '%Y/%m/%d'),
            percent_new='95',
            buy_price='1800',
            price='500',
            express_price='1000',
            if_bargain='0',
            state='1',
            city='1',
            img='cover1.jpg',
            img_meta=dict(
                dimension=[500, 666],
                ratio=1.3,
                size=1024,
                filetype='image/jpeg',
                vdimension={'detail': [375, 400], 'thumb': [235, 300]}
            ),
            description='The cover'
        )
        self.item1 = Item.create(self.profile1, item1)

    def tearDown(self):
        truncate_db()
    
    def test_LikeMineAPI_POST_missed_data_access_token(self):
        Item.publish(self.item1)

        data = dict(
           csrf_token = uuid.uuid4().hex,
           id=self.item1.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"access_token is required."
        )

    def test_LikeMineAPI_POST_401_unauthorized(self):
        Item.publish(self.item1)
        data = dict(
            access_token=utils.gen_uuid(),
            csrf_token = uuid.uuid4().hex,
            id=self.item1.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"请登录."
        )

    def test_LikeMineAPI_POST_404_item_not_found(self):
        Item.publish(self.item1)

        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            id=self.item1.id + 1
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The item is not found."
        )

    def test_LikeMineAPI_POST_403_item_not_published(self):
        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            id=self.item1.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The item is not published."
        )

    def test_LikeMineAPI_POST_404_profile_not_found(self):
        conf.db.update(
            'profiles', deleted_at=datetime.now(),
            where='id=$_id', vars=dict(_id=self.profile.id)
        )
        Item.publish(self.item1)

        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            id=self.item1.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The profile is not found."
        )

    def test_LikeMineAPI_POST_400_item_already_liked(self):
        Item.publish(self.item1)
        Like.like(self.profile, self.item1)

        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            id=self.item1.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"You have already liked the item."
        )

    def test_LikeMine_POST(self):
        Item.publish(self.item1)

        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex,
            id=self.item1.id
        )
        response = app.request(
            '/api/likes/mine',
            method='POST',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '201 Created')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data['created_at'])
        self.assertIsNone(response_data['deleted_at'])
        self.assertEqual(response_data['item_liked_count'], 1)

    def test_LikeMineAPI_PUT_missed_data_access_token(self):
        Item.publish(self.item1)
        Like.like(self.profile, self.item1)
        data = dict(
            csrf_token = uuid.uuid4().hex
        )

        response = app.request(
            '/api/likes/mine/{0}'.format(self.item1.id),
            method='PUT',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"access_token is required."
        )

    def test_LikeMineAPI_PUT_401_Unauthorized(self):
        Item.publish(self.item1)
        Like.like(self.profile, self.item1)

        data = dict(
            access_token=utils.gen_uuid(),
            csrf_token = uuid.uuid4().hex
        )
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item1.id),
            method='PUT',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"请登录."
        )

    def test_LikeMineAPI_PUT_404_item_not_found(self):
        Item.publish(self.item1)
        Like.like(self.profile, self.item1)

        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex
        )
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item1.id + 1),
            method='PUT',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The item is not found."
        )

    def test_LikeMineAPI_PUT_403_item_not_published(self):
        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex
        )
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item1.id),
            method='PUT',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '403 Forbidden')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The item is not published."
        )

    def test_LikeMineAPI_PUT_404_profile_not_found(self):
        Item.publish(self.item1)
        Like.like(self.profile, self.item1)
        conf.db.update(
            'profiles', deleted_at=datetime.now(),
            where='id=$_id', vars=dict(_id=self.profile.id)
        )

        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex
        )
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item1.id),
            method='PUT',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"The profile is not found."
        )

    def test_LikeMineAPI_PUT_404_like_not_found(self):
        Item.publish(self.item1)

        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex
        )
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item1.id),
            method='PUT',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(
            response_data.get('msg'), u"not found."
        )

    def test_LikeMineAPI_PUT(self):
        Item.publish(self.item1)
        Like.like(self.profile, self.item1)
        self.assertEqual(Like.item_liked_count(self.item1), 1)

        data = dict(
            access_token=self.user.access_token,
            csrf_token = uuid.uuid4().hex
        )
        response = app.request(
            '/api/likes/mine/{0}'.format(self.item1.id),
            method='PUT',
            data=json.dumps(data),
            headers={'Content-Type': 'application/json'}
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue(response_data['deleted_at'])
        self.assertIsNone(response_data['updated_at'])
        self.assertEqual(response_data['item_liked_count'], 0)

    def test_PubItemLikeMineAPI_GET_missed_data_access_token(self):
        Item.publish(self.item1)
        Like.like(self.profile, self.item1)

        response = app.request(
            '/api/pubitems/likes/mine?page={0}'.format('1'),
            method='GET'
        )
        self.assertEqual(response.status, '400 Bad Request')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertTrue('access_token' in response_data.get('msg'))

    def test_PubItemLikeMineAPI_GET_401(self):
        Item.publish(self.item1)
        Like.like(self.profile, self.item1)

        response = app.request(
            '/api/pubitems/likes/mine?access_token={0}&page={1}'.format(
            utils.gen_uuid(), '1'
            ),
            method='GET'
        )
        self.assertEqual(response.status, '401 Unauthorized')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'请登录.')

    def test_PubItemLikeMineAPI_GET_404_profile(self):
        Item.publish(self.item1)
        Like.like(self.profile, self.item1)
        conf.db.update(
            'profiles',
            deleted_at=datetime.now(),
            where='id=$_id',
            vars=dict(_id=self.profile.id)
        )

        response = app.request(
            '/api/pubitems/likes/mine?access_token={0}&page={1}'.format(
            self.user.access_token, '1'
            ),
            method='GET'
        )
        self.assertEqual(response.status, '404 Not Found')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(response_data.get('msg'), u'用户不存在.')

    def test_PubItemLikeMineAPI_GET_200(self):
        item1 = Item.publish(self.item1)
        Like.like(self.profile, self.item1)

        response = app.request(
            '/api/pubitems/likes/mine?access_token={0}&page={1}'.format(
            self.user.access_token, '1'
            ),
            method='GET'
        )
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        response_data = json.loads(response.data, encoding='utf-8')
        self.assertEqual(len(response_data), 1)
        self.assertEqual(response_data[0]['id'], item1.id)


if __name__ == '__main__':
    unittest.main()
