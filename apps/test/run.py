# -*- coding: utf-8 -*-

import unittest

# config tests

# box tests
from .box.utils_test import UtilsTest
from .box.exception_test import ExceptionTest

# model tests
from .model.access_token_test import AccessTokenTest
from .model.account_test import AccountTest
from .model.account_order_test import AccountOrderTest
from .model.address_test import AddressTest
from .model.assignment_test import AssignmentTest
from .model.comment_test import CommentTest
from .model.followship_test import FollowshipTest
from .model.item_test import ItemTest
from .model.item_img_test import ItemImgTest
from .model.like_test import LikeTest
from .model.message_test import MessageTest
from .model.order_test import OrderTest
from .model.profile_test import ProfileTest
from .model.role_test import RoleTest
from .model.user_test import UserTest
from .model.verification_test import VerificationTest
# from .model.tag_test import TagTest

# api_app tests
from .api_app.api.access_token_test import AccessTokenAPITest
from .api_app.api.address_test import AddressAPITest
from .api_app.api.comment_test import CommentAPITest
from .api_app.api.followship_test import FollowshipAPITest
from .api_app.api.item_test import ItemAPITest
from .api_app.api.item_img_test import ItemImgAPITest
from .api_app.api.like_test import LikeAPITest
from .api_app.api.message_test import MessageAPITest
from .api_app.api.profile_test import ProfileAPITest
from .api_app.api.user_test import UserAPITest
from .api_app.api.verification_test import VerificationAPITest

# admin_app tests'''
# from .admin_app.controllers.authentication_test import AuthenticationControllerTest
# from .admin_app.controllers item_test import ItemControllerTest


if __name__ == '__main__':
    unittest.main()
