# -*- coding: utf-8 -*-

'''
helper functions used in the tests
'''

from web import storage
from ..model.user import User
from ..model.profile import Profile
from ..config import conf
from ..box import utils

TRUNCATED_TABLES = ', '.join(utils.tables)


def truncate_db():
    '''
    truncate database
    '''
    conf.db.query('truncate ' + TRUNCATED_TABLES)


def signup_and_signin():
    '''
    signup a test user and signin.
    '''

    user = storage(
        email='user@ing.com', phone='18612345678', password_digest='123pw4'
    )
    user = User.signup(user)
    profile = Profile.create_default_profile(user)
    user = User.signin(user)
    return (user, profile)
