# -*- coding; utf-8 -*-
'''
model user
'''

from datetime import datetime
from wheezy.validation import Validator, rules
import bcrypt
from ..config import conf 
from ..box import utils


def is_unique_email(email):
    '''
    check if user with same email is already registered
    '''
    users = conf.db.query(
        'select * from users where email=$email and deleted_at is null',
        vars=dict(email=email)
    )
    return True if len(users) == 0 else False


def is_unique_phone(phone):
    '''
    check if user with same phone is already registered
    '''
    users = conf.db.query(
        'select * from users where phone=$phone and deleted_at is null',
        vars=dict(phone=phone)
    )
    return True if len(users) == 0 else False


class User(object):
    '''
    model user
    '''
    table = 'users'

    validator = Validator({
        'email': [
            rules.email(utils.invalid_msgs['email']),
            rules.must(
                is_unique_email,
                utils.invalid_msgs['unique'].format('email')
            )
        ],
        'phone': [
            rules.must(
                utils.is_phone,
                utils.invalid_msgs['phone']
            ),
            rules.must(
                is_unique_phone,
                utils.invalid_msgs['unique'].format('phone')
            )
        ],
        'password_digest': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(6, 30, utils.invalid_msgs['length'].format(6, 30)),
            rules.compare(
                'password_confirmation',
                message_template=utils.invalid_msgs['compare'].format(
                    'password_confirmation'
                )
            )
        ]
    })
    password_validator = Validator({
        'password_digest': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(6, 30, utils.invalid_msgs['length'].format(6, 30)),
            rules.compare(
                'password_confirmation',
                message_template=utils.invalid_msgs['compare'].format(
                    'password_confirmation'
                )
            )
        ]
    })
    # used to validate user's input in the api
    phone_code_validator = Validator({
        'phone': [
            rules.must(
                utils.is_phone,
                utils.invalid_msgs['phone']
            )
        ],
        'verification_code': [
            rules.must(
                utils.is_verification_code,
                utils.invalid_msgs['verification_code']
            )
        ]
    })

    @classmethod
    def encrypt_password_digest(cls, user):
        '''
        encrypt user.password_digest
        '''
        hashedpw = bcrypt.hashpw(user.password_digest, bcrypt.gensalt())
        user.password_digest = hashedpw

    @classmethod
    def check_password(cls, user, password):
        '''
        check if readable password is equal with the encrypted password
        '''
        val = bcrypt.checkpw(password, user.password_digest)
        return val

    @classmethod
    def signup(cls, user):
        '''
        create an user
        '''
        cls.encrypt_password_digest(user)
        _id = conf.db.insert(
            'users',
            email=user.email,
            phone=user.phone,
            password_digest=user.password_digest
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter user by id
        '''
        users = conf.db.query(
            'select * from users where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(users) == 1:
            return users[0]

    @classmethod
    def filter_by_phone(cls, phone):
        '''
        filter user by phone 
        '''
        users = conf.db.query(
            'select * from users where phone=$_phone and deleted_at is null',
            vars=dict(_phone=phone)
        )
        if len(users) == 1:
            return users[0]

    @classmethod
    def filter_by_email(cls, email):
        '''
        filter user by email
        '''
        users = conf.db.query(
            'select * from users where email=$email and deleted_at is null',
            vars=dict(email=email)
        )
        if len(users) == 1:
            return users[0]

    @classmethod
    def count(cls):
        '''
        count users include deleted
        '''
        users = conf.db.query('select count(id) from users')
        return users[0].count

    @classmethod
    def signin(cls, user, ip='127.0.0.1'):
        '''
        user is given an access token to signin
        '''
        user.access_token = utils.gen_uuid()
        user.sign_in_count += 1
        user.last_sign_in_at = user.current_sign_in_at
        user.current_sign_in_at = datetime.now()
        user.last_sign_in_ip = user.current_sign_in_ip
        user.current_sign_in_ip = ip
        user.updated_at = datetime.now()
        num = conf.db.update(
            'users',
            access_token=user.access_token,
            sign_in_count=user.sign_in_count,
            current_sign_in_at=user.current_sign_in_at,
            last_sign_in_at=user.last_sign_in_at,
            current_sign_in_ip=user.current_sign_in_ip,
            last_sign_in_ip=user.last_sign_in_ip,
            updated_at=user.updated_at,
            where='id=$id',
            vars=dict(id=user.id)
        )
        if num == 1:
            return user

    @classmethod
    def filter_by_access_token(cls, token):
        '''
        filter user by access token
        '''
        users = conf.db.query(
            'select * from users where access_token=$token and deleted_at is null',
            vars=dict(token=token)
        )
        if len(users) == 1:
            return users[0]

    @classmethod
    def signout(cls, user):
        '''
        set access token to None to signout user
        '''
        user.updated_at = datetime.now()
        user.access_token = None
        num = conf.db.update(
            'users',
            access_token=user.access_token,
            updated_at=user.updated_at,
            where='id=$id',
            vars=dict(id=user.id)
        )
        if num == 1:
            return user

    @classmethod
    def reset_password(cls, user):
        '''
        user reset password
        '''
        cls.encrypt_password_digest(user)
        user.updated_at = datetime.now()
        num = conf.db.update(
            'users',
            password_digest=user.password_digest,
            updated_at=user.updated_at,
            where='id=$id',
            vars=dict(id=user.id)
        )
        if num == 1:
            return user
