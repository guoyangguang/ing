# -*- coding: utf-8 -*-

'''
mode item
'''

from wheezy.validation import Validator, rules
from datetime import datetime
import json
from ..config.conf import db
from ..box import utils


class Item(object):
    '''
    model item
    '''

    table = 'items'
    per_page = 15
    versions = {'thumb': 235, 'detail': 425}

    validator = Validator({
        'kind': [
            rules.one_of(
                [str(ele) for ele in list(range(1, 19))],
                utils.invalid_msgs['one_of']
            )
        ],
        'name': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(1, 30, utils.invalid_msgs['length'].format(1, 30))
        ],
        'link': [
            rules.must(
                utils.is_valid_http_link,
                message_template=utils.invalid_msgs['http_link']
            )
        ],
        'buy_at': [
            rules.must(
                utils.is_valid_date,
                message_template=utils.invalid_msgs['date']
            )
        ],
        'percent_new': [
            rules.one_of(
                [str(ele) for ele in list(range(1, 101))],
                utils.invalid_msgs['one_of']
            )
        ],
        'buy_price': [
            rules.regex(
                utils.positive_int_pattern,
                message_template=utils.invalid_msgs['positive_int']
            )
        ],
        'price': [
            rules.regex(
                utils.positive_int_pattern,
                message_template=utils.invalid_msgs['positive_int']
            )
        ],
        'express_price': [
            rules.must(
                utils.is_positive_int_or_zero,
                message_template=utils.invalid_msgs['positive_int_or_zero']
            )
        ],
        'if_bargain': [rules.one_of(['1', '0'], utils.invalid_msgs['one_of'])],
        'state': [
            rules.one_of(
                [str(ele) for ele in list(range(1, 35))],
                utils.invalid_msgs['one_of']
            )
        ],
        'city': [
            rules.one_of(
                [str(ele) for ele in list(range(1, 703))],
                utils.invalid_msgs['one_of']
            ),
            rules.predicate(utils.check_city, utils.invalid_msgs['city'])
        ],
        'description': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(1, 200, utils.invalid_msgs['length'].format(1, 200))
        ]
    })

    @classmethod
    def create(cls, profile, item):
        '''
        create an item for profile
        '''
        _id = db.insert(
            'items',
            profile_id=profile.id,
            kind=int(item.kind),
            name=item.name,
            link=item.link,
            buy_at=datetime.strptime(item.buy_at, '%Y/%m/%d'),
            percent_new=int(item.percent_new),
            buy_price=int(item.buy_price) * 100,
            price=int(item.price) * 100,
            express_price=int(item.express_price) * 100,
            if_bargain=bool(int(item.if_bargain)),
            state=int(item.state),
            city=int(item.city),
            img=item.img,
            img_meta=json.dumps(item.img_meta),
            description=item.description
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter item by _id
        '''
        items = db.query(
            'select * from items where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(items) == 1:
            return items[0]

    @classmethod
    def publish(cls, item):
        '''
        publish the item to change status to 2
        '''
        item.status = 2
        item.updated_at = datetime.now()
        num = db.update(
            'items',
            status=item.status,
            updated_at=item.updated_at,
            where='id=$_id',
            vars=dict(_id=item.id)
        )
        if num == 1:
            return item

    @classmethod
    def filter_pubitems_by_kind_and_city(cls, kind, city, page):
        '''
        filter pubitems by kind and city
        '''
        if city:
            items = db.query(
                'select * from items where status=$status' +
                ' and kind=$kind and city=$city and deleted_at is null' +
                ' order by created_at desc limit $limit offset $offset',
                vars=dict(
                    status=2,
                    kind=kind,
                    city=city,
                    limit=cls.per_page, offset=(page-1)*cls.per_page
                )
            )
        else:
            items = db.query(
                'select * from items where status=$status' +
                ' and kind=$kind and deleted_at is null' +
                ' order by created_at desc limit $limit offset $offset',
                vars=dict(
                    status=2,
                    kind=kind,
                    limit=cls.per_page, offset=(page-1)*cls.per_page
                )
            )
        return list(items)

    @classmethod
    def filter_by_published(cls, page):
        '''
        filter items by published
        '''
        items = db.query(
            'select * from items where status=$status' +
            ' and deleted_at is null' +
            ' order by created_at desc limit $limit offset $offset',
            vars=dict(
                status=2,
                limit=cls.per_page, offset=(page-1)*cls.per_page
            )
        )
        return list(items)

    @classmethod
    def filter_by_profile(cls, profile, page):
        '''
        filter items created by profile
        '''
        items = db.query(
            'select * from items where profile_id=$profileid and deleted_at is null' +
            ' order by created_at limit $limit offset $offset',
            vars=dict(
                profileid=profile.id, limit=cls.per_page, offset=(page-1)*cls.per_page
            )
        )
        return list(items)

    @classmethod
    def filter_pubitems_by_profile(cls, profile, page):
        '''
        filter pubitems by profile
        '''
        items = db.query(
            'select * from items where profile_id=$profileid' +
            ' and status=$status and deleted_at is null' +
            ' order by created_at desc limit $limit offset $offset',
            vars=dict(
                profileid=profile.id, status=2,
                limit=cls.per_page, offset=(page-1)*cls.per_page
            )
        )
        return list(items)

    @classmethod
    def filter_by_ids(cls, ids):
        '''
        filter items by ids
        '''
        if not ids:
            return []
        items = db.query(
            'select * from items where id in $ids and deleted_at is null',
            vars=dict(ids=ids)
        )
        return list(items)

    @classmethod
    def filter_pubitems_by_ids(cls, ids):
        '''
        filter pubitems by ids
        '''
        if not ids:
            return []
        items = db.query(
            'select * from items where id in $ids and status=$status' +
            ' and deleted_at is null',
            vars=dict(ids=ids, status=2)
        )
        return list(items)

    @classmethod
    def update_vdimensions(cls, item, vdimensions):
        '''
        update vdimension for the item version imgs
        '''
        item.img_meta['vdimensions'] = vdimensions
        item.updated_at = datetime.now()
        num = db.update(
            'items',
            img_meta=json.dumps(item.img_meta),
            updated_at=item.updated_at,
            where='id=$_id',
            vars=dict(_id=item.id)
        )
        if num == 1:
            return item
