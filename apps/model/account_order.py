# -*- coding: utf-8 -*-

'''
model account order
'''

from wheezy.validation import Validator, rules
from ..config import conf
from ..box import utils


class AccountOrder(object):

    table = 'account_orders'

    per_page = 15

    validator = Validator({
        'amount': [
            rules.regex(
                utils.positive_int_pattern,
                message_template=utils.invalid_msgs['positive_int']
            )
        ]
    })

    @classmethod
    def create(cls, account, account_order):
        '''
        create one pending account order for the account
        '''
        _id = conf.db.insert(
            'account_orders',
            account_id=account.id,
            amount=int(account_order.amount)
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter account orders by _id
        '''
        account_orders = conf.db.query(
            'select * from account_orders where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(account_orders) == 1:
            return account_orders[0]

    @classmethod
    def paid(cls):
        pass

    @classmethod
    def refund(cls):
        pass
