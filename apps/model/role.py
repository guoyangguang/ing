# -*- coding: utf-8 -*-
'''
model role
'''

from ..config.conf import db
from ..box import utils
from wheezy.validation import Validator, rules
from datetime import datetime


def is_unique_role(name):
    roles = db.query(
        'select * from roles where name=$name and deleted_at is null',
        vars = dict(name=name)
    )
    return True if len(roles) == 0 else False

class Role(object):
    '''
    model role
    '''   
    table = 'roles'    

    validator = Validator({
        'name': [
            rules.slug(utils.invalid_msgs['slug']),
            rules.must(
               is_unique_role,
               utils.invalid_msgs['unique'].format('role')
            )
        ]
    })
  
    @classmethod
    def get(cls , _id): 
        '''
        filter role by _id
        '''
        roles = db.query(
            'select * from roles where id=$_id and deleted_at is null',
            vars = dict(_id=_id)
        )
        if len(roles) == 1:
            return roles[0]

    @classmethod
    def all(cls):
        '''
        filter all roles
        '''
        roles = db.query('select * from roles where deleted_at is null')
        return [role for role in roles]
    
    @classmethod
    def filter_by_ids(cls, ids):
        '''
        filter roles by a list of ids
        '''
        if len(ids) == 0:
            return list()
        roles = db.query(
            'select * from roles where id in $ids and deleted_at is null',
            vars = dict(ids=ids)
        )
        return [role for role in roles]

    @classmethod
    def filter_by_name(cls, name):
        '''
        filter roles by rolename
        '''
        roles = db.query(
            'select * from roles where name=$name and deleted_at is null',
            vars = dict(name=name)
        )
        if len(roles) == 1:
            return roles[0]
    
    @classmethod
    def create(cls, role): 
        '''
        create one role
        '''
        # check if role already exists before calling the method
        _id = db.insert(
            'roles',
            name=role.name
        )
        if _id:
            return cls.get(_id) 

    @classmethod 
    def delete(cls, role):
        # TODO how to delete assignment 
        role.deleted_at = datetime.now() 
        num = db.update(
            'roles',
            deleted_at = role.deleted_at,
            where='id=$_id',
            vars=dict(_id=role.id) 
        )
        if num == 1:
            return role 
