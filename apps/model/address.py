# -*- coding: utf-8 -*-

'''
model address
'''

from wheezy.validation import Validator, rules
from ..config import conf
from ..box import utils


class Address(object):

    table = 'addresses'

    per_page = 15

    validator = Validator({
        'recipient': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(
                max=25, message_template=utils.invalid_msgs['max_length'].format(25)
            )
        ],
        'phone': [rules.must(utils.is_phone, utils.invalid_msgs['phone'])],
        'state': [
            rules.one_of(
                [str(ele) for ele in list(range(1, 35))], utils.invalid_msgs['one_of']
            )
        ],
        'city': [
            rules.one_of(
                [str(ele) for ele in list(range(1, 703))], utils.invalid_msgs['one_of']
            ),
            rules.predicate(utils.check_city, utils.invalid_msgs['city'])
        ],
        'detail': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(
                max=100, message_template=utils.invalid_msgs['max_length'].format(100)
            )
        ],
        'postcode': [rules.must(utils.is_postcode, utils.invalid_msgs['postcode'])]
    })

    @classmethod
    def create(cls, profile, address):
        '''
        create one address for the profile
        '''
        _id = conf.db.insert(
            'addresses',
            profile_id=profile.id,
            recipient=address.recipient,
            phone=address.phone,
            state=int(address.state),
            city=int(address.city),
            detail=address.detail,
            postcode=address.postcode
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter addresses by _id
        '''
        addresses = conf.db.query(
            'select * from addresses where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(addresses) == 1:
            return addresses[0]

    @classmethod
    def filter_by_profile(cls, profile):
        '''
        filter addresses by profile
        '''
        addresses = conf.db.query(
            'select * from addresses where profile_id=$profileid' +
            ' and deleted_at is null order by created_at desc',
            vars=dict(profileid=profile.id)
        )
        return list(addresses)
