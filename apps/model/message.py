# -*- coding: utf-8 -*-
'''
messages sent among profiles
'''

from datetime import datetime
from wheezy.validation import Validator, rules

from .profile import Profile
from ..config.conf import db
from ..box import utils


class Message(object):
    '''
    messages sent among profiles
    '''

    table = 'messages'

    validator = Validator({
        'body': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(1, 255, utils.invalid_msgs['length'].format(1, 255))
        ]
    })

    per_page = 15

    @classmethod
    def _select_message_and_profile(cls):
        '''
        select message and profile
        '''
        cols = ('id', 'profile_id', 'body', 'recipient_id', 'read', 'read_at',
            'created_at', 'updated_at', 'deleted_at')
        profile_cols = ('id', 'user_id', 'name', 'gender', 'location',
            'job', 'img', 'about', 'uploaded_at', 'created_at',
            'updated_at', 'deleted_at')
        select_msg = ', '.join(
            ['messages.{0}'.format(col) for col in cols]
        )
        select_profile = ', '.join(
            ['profiles.{0} as profiles_{1}'.format(col, col) for col in profile_cols]
        )
        return 'select ' + select_msg + ', ' + select_profile

    @classmethod
    def get(cls, _id):
        '''
        get a message with an id
        '''
        val = db.query(
            'select * from messages' +
            ' where id=$id and deleted_at is null',
            vars=dict(id=_id)
        )
        if len(val) == 1:
            return val[0]

    @classmethod
    def contacts(cls, profile):
        '''
        the contacts that profile has
        '''
        sql = cls._select_message_and_profile() +\
           ' from messages inner join profiles on messages.recipient_id=profiles.id' +\
           ' where messages.profile_id=$pid and messages.deleted_at is null'
        sent = db.query(sql, vars=dict(pid=profile.id))
        sql = cls._select_message_and_profile() +\
           ' from messages inner join profiles on messages.profile_id=profiles.id' +\
           ' where messages.recipient_id=$pid and messages.deleted_at is null'
        received = db.query(sql, vars=dict(pid=profile.id))
        msgs = list(sent) + list(received)
        contacts = [msg.profiles_id for msg in msgs] 
        contacts = list(set(contacts))
        contacts = filter(lambda contact: contact != profile.id, contacts)
        profiles = Profile.filter_by_ids(contacts)
        return profiles
        # for contact in contacts:
        #     between = filter(
        #         lambda msg: (msg.profile_id == profile.id and
        #             msg.recipient_id == contact) or
        #             (msg.recipient_id == profile.id and
        #             msg.profile_id == contact),
        #         msgs
        #     )
        #     sorted_between = sorted(
        #         between, key=lambda msg: msg.created_at, reverse=True
        #     )
        #     _all.append(sorted_between[0])
        # return _all 

    @classmethod
    def unread(cls, profile, page):
        '''
        messages that profile not read

        :param profile: an instance of profile 
        :param page: page number
        '''
        sql = cls._select_message_and_profile() +\
            ' from messages inner join profiles on messages.profile_id=profiles.id' +\
            ' where messages.recipient_id=$profile and messages.read=$read' +\
            ' and messages.deleted_at is null' +\
            ' order by messages.created_at limit $limit offset $offset'
        msgs = db.query(
            sql,
            vars=dict(
                profile=profile.id, read=False,
                limit=cls.per_page, offset=(page-1)*cls.per_page
            )
        )
        return list(msgs)

    @classmethod
    def unread_count(cls, profile):
        '''
        messages count that profile unread

        :param profile: an instance of profile 
        '''
        sql = 'select count(id) from messages where' +\
              ' recipient_id=$profile_id and read=$read and deleted_at is null'
        msgs = db.query(sql, vars=dict(profile_id=profile.id, read=False))
        return msgs[0].count

    @classmethod
    def between(cls, profile1, profile2, page):
        '''
        messages between profile1 and profile2

        :param profile1: an instance of profile   
        :param profile2: an instance of profile
        :param page: page number
        '''
        sql = 'select * from messages where' +\
            ' ((profile_id=$p1 and recipient_id=$r1)' +\
            ' or (profile_id=$p2 and recipient_id=$r2))' +\
            ' and deleted_at is null' +\
            ' order by created_at limit $limit offset $offset'
        msgs = db.query(
            sql,
            vars=dict(p1=profile1.id, r1=profile2.id, p2=profile2.id, r2=profile1.id,
                      limit=cls.per_page, offset=(page-1)*cls.per_page)
        )
        return list(msgs)

    @classmethod
    def read_message(cls, msg):
        '''
        profile read the message

        :param msg: an instance of message
        '''
        # check if not(cls.read) before calling the method
        msg.read = True
        msg.read_at = datetime.now()
        msg.updated_at = datetime.now()
        num = db.update(
            'messages',
            read=msg.read,
            read_at=msg.read_at,
            updated_at=msg.updated_at,
            where='id=$id',
            vars=dict(id=msg.id)
        )
        if num == 1:
            return msg

    @classmethod
    def send_message(cls, profile, msg, recipient):
        '''
        profile sends message to recipient

        :param profile: an instance of profile 
        :param msg: an instance of message
        :param recipient: an instance of profile 
        '''
        # check if profile == recipient before calling the method'''
        msg.profile_id = profile.id
        msg.recipient_id = recipient.id
        _id = db.insert(
            'messages',
            profile_id=msg.profile_id,
            body=msg.body,
            recipient_id=msg.recipient_id
        )
        if _id:
            return cls.get(_id)
