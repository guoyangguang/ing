# -*- coding: utf-8 -*-
'''
model item imgs
'''

from wheezy.validation import Validator, rules
from datetime import datetime
import json
from ..config import conf
from ..box import utils


class ItemImg(object):

    table = 'item_imgs'

    validator = Validator({
        'description': [
            rules.required(utils.invalid_msgs['required']),
            rules.length(1, 50, utils.invalid_msgs['length'].format(1, 50))
        ],
        'is_note': [rules.one_of(['1', '0'], utils.invalid_msgs['one_of'])]
    })

    versions = {'thumb': 235, 'detail': 425}

    @classmethod
    def create(cls, item, itemimg):
        '''
        create an item_img for item
        '''
        _id = conf.db.insert(
            'item_imgs',
            item_id=item.id,
            img=itemimg.img,
            img_meta=json.dumps(itemimg.img_meta)
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def update_general(cls, itemimg, new_itemimg):
        '''
        update the general attrs of an item_img with new one
        '''
        itemimg.description = new_itemimg.description
        itemimg.is_note = bool(int(new_itemimg.is_note))
        itemimg.updated_at = datetime.now()
        num = conf.db.update(
            'item_imgs',
            description=itemimg.description,
            is_note=itemimg.is_note,
            updated_at=itemimg.updated_at,
            where='id=$_id',
            vars=dict(_id=itemimg.id)
        )
        if num == 1:
            return itemimg

    @classmethod
    def get(cls, _id):
        '''
        filter item_img by id
        '''
        itemimgs = conf.db.query(
            'select * from item_imgs where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(itemimgs) == 1:
            return itemimgs[0]

    @classmethod
    def filter_by_item(cls, item):
        '''
        filter item_imgs by item
        '''
        sql = 'select * from item_imgs where item_id=$item_id' +\
            ' and deleted_at is null order by created_at'
        itemimgs = conf.db.query(
            sql,
            vars=dict(item_id=item.id)
        )
        return list(itemimgs)
