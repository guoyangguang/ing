# -*- coding: utf-8 -*-
'''
model likes
'''

from datetime import datetime
from .item import Item
from ..config import conf


class Like(object):

    table = 'likes'
    per_page = 15

    @classmethod
    def like(cls, profile, item):
        '''
        like
        '''
        # NOTE don't call the method if profile already liked the item
        # to ensure primary key that is not deleted
        conf.db.insert(
            'likes',
            profile_id=profile.id,
            item_id=item.id,
        )
        return cls.get(profile, item)

    @classmethod
    def get(cls, profile, item):
        '''
        filter like by profile, item
        '''
        likes = conf.db.query(
            'select * from likes' +
            ' where profile_id=$profileid and item_id=$itemid and deleted_at is null',
            vars=dict(profileid=profile.id, itemid=item.id)
        )
        if len(likes) == 1:
            return likes[0]

    @classmethod
    def cancel_like(cls, like):
        '''
        cancel like
        '''
        like.deleted_at = datetime.now()
        num = conf.db.update(
            'likes',
            deleted_at=like.deleted_at,
            where='profile_id=$profileid and item_id=$itemid and deleted_at is null',
            vars=dict(profileid=like.profile_id, itemid=like.item_id)
        )
        if num == 1:
            return like

    @classmethod
    def item_liked_count(cls, item):
        '''
        count likes for an item
        '''
        likes = conf.db.query(
            'select count(*) from likes' +
            ' where item_id=$itemid and deleted_at is null',
            vars=dict(itemid=item.id)
        )
        return likes[0].count

    @classmethod
    def pubitems_liked_by(cls, profile, page):
        '''
        pubitems that profile likes
        '''
        # FIXME why the order is not created_at desc?
        likes = conf.db.query(
            'select item_id from likes' +
            ' where profile_id=$profileid and deleted_at is null' +
            ' order by created_at desc limit $limit offset $offset',
            vars=dict(
                profileid=profile.id,
                limit=cls.per_page,
                offset=(page-1)*cls.per_page
            )
        )
        return Item.filter_pubitems_by_ids([like.item_id for like in list(likes)])
