# -*- coding: utf-8 -*-

'''
model order
'''

from wheezy.validation import Validator, rules
from ..config import conf
from ..box import utils


class Order(object):

    table = 'orders'
    per_page = 15

    validator = Validator({
        'amount': [
            rules.regex(
                utils.positive_int_pattern,
                message_template=utils.invalid_msgs['positive_int']
            )
        ]
    })

    @classmethod
    def create(cls, profile, item, order):
        '''
        profile create one pending order for the item
        '''
        _id = conf.db.insert(
            'orders',
            profile_id=profile.id,
            item_id=item.id,
            amount=int(order.amount)
        )
        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        '''
        filter orders by _id
        '''
        orders = conf.db.query(
            'select * from orders where id=$_id and deleted_at is null',
            vars=dict(_id=_id)
        )
        if len(orders) == 1:
            return orders[0]

    @classmethod
    def paid(cls):
        pass

    @classmethod
    def refund(cls):
        pass
