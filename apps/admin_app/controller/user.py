# -*- coding: utf-8 -*-

from ...model.user import User
from ...config import conf


class AuthSignupController(object):
    '''
    as a not registered user, i want to signup, so that i
    can do something for myself.
    '''
    def GET(self):
        conf.web.ctx.name = 'renhongyan'
        return 'ok' 
