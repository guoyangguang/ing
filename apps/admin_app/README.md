##Install git from source
###download stable version 
<http://git-scm.com/download>
###extract
`tar -zxf path` 
`cd extracted_path`
###configure and compile
`make prefix=/usr/local all`
###install as super user
`sudo make prefix=/usr/local install`
###check 
`git --version`
###if the dependent libs not installed, visit the doc page
<http://git-scm.com/book/en/Getting-Started-Installing-Git>
###config git, config name and email
`vi .gitconfig`
###config to communicate with github.com
visit <https://help.github.com/articles/generating-ssh-keys>
###clone the repository from github.com
`git clone address` 

##install nginx from source
###download stable version source
visit <http://nginx.org/en/download.html>
###extract
`tar -xf source` 
`cd extracted dir`
###configure
`./configure`
###compile
`make`
###install with super user
`sudo make install`
###check
`nginx -v`
###config nginx
`vi /usr/local/nginx/conf/nginx.conf`
###start nginx server as super user
`sudo nginx`
###quit nginx server as super user
`sudo nginx -s quit`
###reload config as super user
`sudo nginx -s reload`
###doc page 
<http://nginx.org/en/docs/configure.html>

##install nvm 
`curl https://raw.github.com/creationix/nvm/master/install.sh | sh`
##install node
`nvm install version`
### use some version
nvm use version

##Install virtualenv 
###install virtual
`pip install virtualenv`
###build and use virtualenv
`cd /project`
`virtualenv virtualenv_name`
`source virtualenv_name/bin/activate`
`pip install -r requirements.txt`
###to quit from virtualenv
`deactivate`

##Install sql database 
###download source
<http://www.postgresql.org/ftp/source/>
###configure
`./configure`
###make
`make`
###signin as superuser
`su`
###install
`make install`
###for secure, add an user to manage postgresql 
`adduser somename`
###make dir to store data
`mkdir /usr/local/pgsql/data`
###change owner for data dir 
`chown postgres /usr/local/pgsql/data`
###signin with somename
`su somename`
###init db
`/usr/local/pgsql/bin/initdb -D /usr/local/pgsql/data`
###run postgresql server
`/usr/local/pgsql/bin/postgres -D /usr/local/pgsql/data >logfile 2>&1 &`
###create a test database to test 
`/usr/local/pgsql/bin/createdb test`
###use postgresql client to test
`/usr/local/pgsql/bin/psql test`
###symlink server and client
`ln -s /usr/local/pgsql/bin/postgres /usr/bin`
`ln -s /usr/local/pgsql/bin/psql /usr/bin`
doc page <http://www.postgresql.org/docs/9.3/static/install-short.html>

##Manually test from terminal
###get request
`curl --request GET 'http://127.0.0.1:300/path' --include`
###post request
`curl --request POST 'http://127.0.0.1:300/path' --data 'name1=val1&name2=val2' --include`
###put request
`curl --request PUT 'http://127.0.0.1:300/path' --data 'name1=val1&name2=val2' --include`
###delete request
`curl --request DELETE 'http://127.0.0.1:300/path' --include`
