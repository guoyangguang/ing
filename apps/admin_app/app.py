# -*- coding: utf-8 -*-

import os
from ..config import conf
from .controller.user import AuthSignupController

urls = (
    '/admin/signup', 'AuthSignupController'
)


app = conf.web.application(mapping=urls, fvars=locals(), autoreload=None)

wsgi_app = app.wsgifunc()
