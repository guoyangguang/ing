# -*- coding: utf-8 -*-

'''
add manual test data
'''

from datetime import datetime, timedelta
from web import Storage
from faker import Faker

from ..model.user import User
from ..model.profile import Profile
from ..model.message import Message
from ..model.item import Item
from ..box.esearch import Esearch
from ..test.helper import truncate_db

fake = Faker()


if __name__ == '__main__':

    # truncate db before seeding data
    truncate_db()

    esearch = Esearch(index='items')
    # esearch.drop_index()
    # esearch.create_index()

    # users
    # toby = Storage(email='toby@ing.com', password_digest='123password4')
    # toby = User.confirmable(toby)
    # toby = User.filter_by_confirmation_token(toby.confirmation_token)
    # toby = User.confirm(toby)
    # toby_profile = Profile.create_default_profile(toby)

    # may = Storage(email='may@ing.com', password_digest='123password4')
    # may = User.confirmable(may)
    # may = User.filter_by_confirmation_token(may.confirmation_token)
    # may = User.confirm(may)
    # may_profile = Profile.create_default_profile(may)

    # emily = Storage(email='emily@ing.com', password_digest='123password4')
    # emily = User.confirmable(emily)
    # emily = User.filter_by_confirmation_token(emily.confirmation_token)
    # emily = User.confirm(emily)
    # emily_profile = Profile.create_default_profile(emily)

    # messages
    # toby_msg1 = Storage(body='Would you like to have a coffee this weekend?')
    # Message.send_message(toby_profile, toby_msg1, may_profile)

    # toby_msg2 = Storage(body='hi, i am toby, it is long time since last meet.')
    # Message.send_message(toby_profile, toby_msg2, emily_profile)

    # may_msg1 = Storage(body='toby, ok, thanks.')
    # Message.send_message(may_profile, may_msg1, toby_profile)

    # may_msg2 = Storage(body='but where and when?')
    # Message.send_message(may_profile, may_msg2, toby_profile)

    # emily_msg = Storage(body='do some shopping this weekend?')
    # Message.send_message(emily_profile, emily_msg, may_profile)

    # items(no image insert)
    # toby_item1 = Storage(
    #     kind='17',
    #     name='G banboo',
    #     link='http://book.douban.com/subject/1046265/',
    #     buy_at=datetime.strftime(datetime.now()-timedelta(days=60), '%Y/%m/%d'),
    #     percent_new='95',
    #     buy_price='280',
    #     price='180',
    #     express_price='10',
    #     if_bargain='0',
    #     state='1',
    #     city='1',
    #     img='dongxiao.jpg',  # img pretend to be uploaded image.
    #     img_meta=dict(
    #         size=1024,
    #         filetype='image/jpeg',
    #         vdimensions={'detail': [425, 566], 'thumb': [235, 313]}
    #     ),
    #     description='no price bargain'
    # )
    # toby_item1 = Item.publish(
    #     Item.create(toby_profile, toby_item1)
    # )
    # esearch.insert(dict(toby_item1))

    # toby_item2 = Storage(
    #     kind='3',
    #     name='leica',
    #     link='http://book.douban.com/subject/1046266/',
    #     buy_at=datetime.strftime(datetime.now()-timedelta(days=60), '%Y/%m/%d'),
    #     percent_new='75',
    #     buy_price='10000',
    #     price='4000',
    #     express_price='10',
    #     if_bargain='0',
    #     state='1',
    #     city='1',
    #     img='leica.jpg',  # img pretend to be uploaded image.
    #     img_meta=dict(
    #         size=1024,
    #         filetype='image/jpeg',
    #         vdimensions={'detail': [425, 566], 'thumb': [235, 313]}
    #     ),
    #     description='it is a leica camera'
    # )
    # toby_item2 = Item.publish(
    #     Item.create(toby_profile, toby_item2)
    # )
    # esearch.insert(dict(toby_item2))

    # may_item = Storage(
    #     kind='11',
    #     name='zara coat',
    #     link='http://book.douban.com/subject/1046267/',
    #     buy_at=datetime.strftime(
    #         datetime.now()-timedelta(days=30), '%Y/%m/%d'
    #     ),
    #     percent_new='90',
    #     buy_price='400',
    #     price='250',
    #     express_price='10',
    #     if_bargain='1',
    #     state='1',
    #     city='1',
    #     img='zara1.jpg',
    #     img_meta=dict(
    #         size=1024,
    #         filetype='image/jpeg',
    #         vdimensions={'detail': [425, 425], 'thumb': [235, 235]}
    #     ),
    #     description='please message to me'
    # )
    # may_item = Item.publish(
    #     Item.create(may_profile, may_item)
    # )
    # esearch.insert(dict(may_item))

    # emily_item = Storage(
    #     kind='4',
    #     name='mirror',
    #     link='http://book.douban.com/subject/1046268/',
    #     buy_at=datetime.strftime(
    #         datetime.now()-timedelta(days=10), '%Y/%m/%d'
    #     ),
    #     percent_new='99',
    #     buy_price='400',
    #     price='290',
    #     express_price='30',
    #     if_bargain='1',
    #     state='2',
    #     city='42',
    #     img='mirror.jpg',
    #     img_meta=dict(
    #         size=1024,
    #         filetype='image/jpeg',
    #         vdimensions={'detail': [425, 636], 'thumb': [235, 352]}
    #     ),
    #     description='a big mirror'
    # )
    # emily_item = Item.publish(
    #     Item.create(emily_profile, emily_item)
    # )
    # esearch.insert(dict(emily_item))

    print('Manual test data has been added')
