# -*- coding: utf-8 -*-

from .base import Base
from .profile import ProfilePresenter
from ...box.uploader import Uploader


class MessagePresenter(Base):

    @classmethod
    def send_message(cls, profile, msg):
        data = cls._message(msg)
        data['profile'] = ProfilePresenter._profile(profile)
        cls.data = data 
        return cls

    @classmethod
    def contacts(cls, profiles):
        cls.data = [ProfilePresenter._profile(profile) for profile in profiles] 
        return cls

    @classmethod
    def unread(cls, msgs):
        msg_list = cls._joineds_with_profile(msgs)
        cls.data = msg_list
        return cls

    @classmethod
    def between(cls, profile, the_other, msgs):
        msg_list = [cls._message(msg) for msg in msgs]
        profile_presenter = ProfilePresenter._profile(profile)
        the_other_presenter = ProfilePresenter._profile(the_other)
        data = list()
        for msg in msg_list:
            if msg['profile_id'] == profile.id:
                msg['profile'] = profile_presenter
            else:
                msg['profile'] = the_other_presenter
            data.append(msg)
        cls.data = data 
        return cls

    @classmethod
    def _message(cls, msg):
        msg_storage = cls.strf_timestamp(
            msg, 'read_at', 'created_at', 'updated_at', 'deleted_at'
        )
        return dict(msg_storage)

    @classmethod
    def _joineds_with_profile(cls, msgs):
        messages = Base._joineds_with_profile(msgs)
        msg_list = list()
        for msg in messages:
            profile = ProfilePresenter._profile(msg.pop('profile'))
            msg_dic = cls._message(msg) 
            msg_dic['profile'] = profile
            msg_list.append(msg_dic)
        return msg_list
