# -*- coding: utf-8 -*-
'''
like presenter
'''

from .base import Base
from .item import ItemPresenter


class LikePresenter(Base):

    @classmethod
    def created(cls, like, item_liked_count):
        like_storage = cls.strf_timestamp(
            like, 'created_at', 'updated_at', 'deleted_at'
        )
        like_storage.item_liked_count = item_liked_count
        cls.data = dict(like_storage)
        return cls

    @classmethod
    def cancel_like(cls, like, item_liked_count):
        like_storage = cls.strf_timestamp(
            like, 'created_at', 'updated_at', 'deleted_at'
        )
        like_storage.item_liked_count = item_liked_count
        cls.data = dict(like_storage)
        return cls

    @classmethod
    def pubitems_liked_by(cls, items):
        item_list = [ItemPresenter._item(item) for item in items]
        cls.data = item_list
        return cls
