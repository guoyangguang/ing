# -*- coding: utf-8 -*-
'''
verification presenter
'''

from .base import Base


class VerificationPresenter(Base):

    @classmethod
    def verification(cls, verification):
        verification = cls.strf_timestamp(
            verification, 'created_at', 'updated_at', 'deleted_at'
        )
        cls.data = {
            'id': verification.id,
            'phone': verification.phone,
            'created_at': verification.created_at,
            'updated_at': verification.updated_at,
            'deleted_at': verification.deleted_at
        }
        return cls
