# -*- coding: utf-8 -*-
'''
access token presenter
'''

from .base import Base


class AccessTokenPresenter(Base):

    @classmethod
    def access_token(cls, access_token):
        data = cls._access_token(access_token)
        cls.data = data
        return cls

    @classmethod
    def filter_by_profile_and_site(cls, access_token):
        data = cls._access_token(access_token)
        cls.data = data
        return cls

    # @classmethod
    # def filter_by_profile(cls, access_tokens):
    #     data = [cls._access_token(access_token) for access_token in access_tokens]
    #     cls.data = data
    #     return cls

    @classmethod
    def _access_token(cls, access_token):
        access_token_storage = cls.strf_timestamp(
            access_token, 'created_at', 'updated_at', 'deleted_at'
        )
        return dict(access_token_storage)
