# -*- coding: utf-8 -*-

from .base import Base


class UserPresenter(Base):
  
    @classmethod 
    def user(cls, user):
        user_storage = cls.strf_timestamp(
            user,
            'created_at',
            'updated_at',
            'deleted_at'
        ) 
        cls.data = dict(
            id=user_storage.id,
            email=user_storage.email,
            phone=user_storage.phone,
            access_token=user_storage.access_token,
            created_at=user_storage.created_at,
            updated_at=user_storage.updated_at,
            deleted_at=user_storage.deleted_at
        )
        return cls
