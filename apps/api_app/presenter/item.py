#  -*- coding: utf-8 -*-

'''
item presenter
'''

from .base import Base
from .profile import ProfilePresenter
from .comment import CommentPresenter
from .item_img import ItemImgPresenter
from ...box.uploader import Uploader


class ItemPresenter(Base):

    @classmethod
    def created(cls, item):
        item_dict = cls._item(item)
        cls.data = item_dict
        return cls

    @classmethod
    def filter_by_published(cls, items):
        item_list = [cls._item(item) for item in items]
        cls.data = item_list
        return cls

    @classmethod
    def filter_pubitems_by_kind_in_city(cls, items):
        item_list = [cls._item(item) for item in items]
        cls.data = item_list
        return cls

    @classmethod
    def filter_pubitems_by_query_in_city(cls, items):
        item_list = [cls._item(item) for item in items]
        cls.data = item_list
        return cls

    @classmethod
    def filter_by_profile(cls, items):
        item_list = [cls._item(item) for item in items]
        cls.data = item_list
        return cls

    @classmethod
    def item_mine_show(cls, item, is_liked, like_count, comments, itemimgs):
        item_dic = cls._item(item)
        item_dic['is_liked'] = is_liked
        item_dic['like_count'] = like_count
        item_dic['comments'] = CommentPresenter._joined_with_profile(comments)
        item_dic['itemimgs'] = [
            ItemImgPresenter._itemimg(itemimg) for itemimg in itemimgs
        ]
        cls.data = item_dic
        return cls

    @classmethod
    def pubitem_show(cls, item, profile, is_followed, is_liked, like_count, comments, itemimgs):
        item_dic = cls._item(item)
        item_dic['profile'] = ProfilePresenter._profile(profile)
        item_dic['is_followed'] = is_followed
        item_dic['is_liked'] = is_liked
        item_dic['like_count'] = like_count
        item_dic['comments'] = CommentPresenter._joined_with_profile(comments)
        item_dic['itemimgs'] = [
            ItemImgPresenter._itemimg(itemimg) for itemimg in itemimgs
        ]
        cls.data = item_dic
        return cls

    @classmethod
    def filter_pubitems_by_profile(cls, items):
        item_list = [cls._item(item) for item in items]
        cls.data = item_list
        return cls

    @classmethod
    def pubitems_her_show(cls, item, is_liked, like_count, comments, itemimgs):
        item_dic = cls._item(item)
        item_dic['is_liked'] = is_liked
        item_dic['like_count'] = like_count
        item_dic['comments'] = CommentPresenter._joined_with_profile(comments)
        item_dic['itemimgs'] = [
            ItemImgPresenter._itemimg(itemimg) for itemimg in itemimgs
        ]
        cls.data = item_dic
        return cls

    @classmethod
    def _item(cls, item):
        item_storage = cls.strf_timestamp(
            item, 'buy_at', 'created_at', 'updated_at', 'deleted_at'
        )
        item_storage.thumb_img = Uploader.filepath_of(
            'items', item_storage, 'thumb'
        )
        item_storage.detail_img = Uploader.filepath_of(
            'items', item_storage, 'detail'
        )
        item_storage.buy_price = float(item_storage.buy_price) / float(100)
        item_storage.price = float(item_storage.price) / float(100)
        item_storage.express_price = float(item_storage.express_price) / float(100)
        return dict(item_storage)
