# -*- coding: utf-8 -*-
'''
address presenter
'''

from .base import Base


class AddressPresenter(Base):

    @classmethod
    def created(cls, address):
        data = cls._address(address)
        cls.data = data
        return cls

    @classmethod
    def filter_by_profile(cls, addresses):
        address_list = [cls._address(address) for address in addresses]
        cls.data = address_list
        return cls

    @classmethod
    def _address(cls, address):
        address_storage = cls.strf_timestamp(
            address, 'created_at', 'updated_at', 'deleted_at'
        )
        return dict(address_storage)
