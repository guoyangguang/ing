# -*- coding: utf-8 -*-

'''
api app
'''
import os
from ..config import conf
from .api.access_token import AccessTokenMineAPI, AccessTokenMinePubStatusAPI
from .api.address import AddressMineAPI
from .api.captcha import CaptchaAPI
from .api.comment import PubItemCommentAPI
from .api.csrf_token import CSRFTokenAPI
from .api.followship import FollowedMineAPI, FollowingMineAPI
from .api.item import (
    ItemMineAPI, ItemMineShowAPI, PubItemsAPI, PubItemKindInCityAPI,
    PubItemSearchInCityAPI, PubItemShowAPI, PubItemsHerAPI,
    PubItemsHerShowAPI
)
from .api.item_img import ItemMineItemImgAPI
from .api.like import LikeMineAPI, PubItemLikeMineAPI
from .api.message import (
    MessagesContactsAPI, MessagesUnreadMineAPI, MessagesBetweenAPI
)
from .api.profile import ProfileAPI, ProfileFileAPI
from .api.user import (
    AuthSignupAPI, AuthSigninAPI, AuthSignoutAPI,
    AuthResetPasswordAPI
)
from .api.verification import VerificationAPI

urls = (
    '/api/addresses/mine', 'AddressMineAPI',
    '/api/captcha', 'CaptchaAPI',
    '/api/pubitems/(\d+)/comments', 'PubItemCommentAPI',
    '/api/csrf_token', 'CSRFTokenAPI',
    '/api/followeds/mine', 'FollowedMineAPI',
    '/api/followeds/mine/(\d+)', 'FollowedMineAPI',
    '/api/followings/mine', 'FollowingMineAPI',
    '/api/items/mine', 'ItemMineAPI',
    '/api/items/mine/(\d+)', 'ItemMineShowAPI',
    '/api/pubitems', 'PubItemsAPI',
    '/api/pubitems/kind_in_city', 'PubItemKindInCityAPI',
    '/api/pubitems/search_in_city', 'PubItemSearchInCityAPI',
    '/api/pubitems/likes/mine', 'PubItemLikeMineAPI',
    '/api/pubitems/(\d+)', 'PubItemShowAPI',
    '/api/pubitems/her', 'PubItemsHerAPI',
    '/api/pubitems/her/(\d+)', 'PubItemsHerShowAPI',
    '/api/items/mine/(\d+)/itemimgs', 'ItemMineItemImgAPI',
    '/api/items/mine/(\d+)/itemimgs/(\d+)', 'ItemMineItemImgAPI',
    '/api/likes/mine', 'LikeMineAPI',
    '/api/likes/mine/(\d+)', 'LikeMineAPI',
    '/api/messages/contacts/mine', 'MessagesContactsAPI',
    '/api/messages/unread/mine', 'MessagesUnreadMineAPI',
    '/api/messages/between', 'MessagesBetweenAPI',
    '/api/access_tokens/mine', 'AccessTokenMineAPI',
    '/api/access_tokens/mine/pubstatus', 'AccessTokenMinePubStatusAPI',
    '/api/profile', 'ProfileAPI',
    '/api/profile/file', 'ProfileFileAPI',
    '/api/signup', 'AuthSignupAPI',
    '/api/signin', 'AuthSigninAPI',
    '/api/signout', 'AuthSignoutAPI',
    '/api/reset_password', 'AuthResetPasswordAPI',
    '/api/verifications', 'VerificationAPI'
)

app = conf.web.application(mapping=urls, fvars=locals(), autoreload=None)

wsgi_app = app.wsgifunc()

if not conf.web.config.debug:
    session = conf.web.session.Session(
        app,
        conf.web.session.DiskStore(os.path.join(os.path.dirname(__file__), '..', 'sessions'))
    )
    # render = conf.web.template.render('templates', globals={'csrf_token': csrf_token})
    def session_hook():
        conf.web.ctx.session = session
        conf.web.template.Template.globals['session'] = session
    # TODO how does the admin app to use session?
    app.add_processor(conf.web.loadhook(session_hook))

def after_request():
    '''
    add content type header for api request
    '''
    # if conf.web.ctx['path'] == '/api/captcha':
    #     conf.web.header('Content-Type', 'image/jpeg')
    # else:
    conf.web.header('Content-Type', 'application/json')
app.add_processor(conf.web.unloadhook(after_request))

# keys in web.ctx,
# ['status', 'realhome', 'homedomain', 'protocol', 'app_stack', 'ip', 'fullpath',
# 'headers', 'host', 'environ', 'env', 'home', 'homepath', 'output', 'path',
# 'query', 'method']
# for example,
# web.ctx.env.get('HTTP_USER_AGENT')
