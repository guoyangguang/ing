#  -*- coding: utf-8 -*-

'''
item apis
'''

from web import Storage
from .base import Base
from ...model.profile import Profile
from ...model.item import Item
from ...model.item_img import ItemImg
from ...model.comment import Comment
from ...model.followship import Followship
from ...model.like import Like
from ..presenter.item import ItemPresenter
from ...box.exception import Forbidden, NotFound, BadRequest, format_errors
from ...box import utils
from ...box.uploader import Uploader
from ...box.esearch import Esearch
from ...config import conf

items_esearch = Esearch(index='items')


class ItemMineAPI(Base):

    def POST(self):
        '''
        As a general user, i want to create an item, so that i can sell my item.
        '''
        data = self.input(
            'csrf_token', 'access_token', 'kind', 'name', 'link', 'buy_at',
            'percent_new', 'buy_price', 'price', 'express_price', 'if_bargain',
            'state', 'city', 'img', 'description', img={}
        )
        # from pdb import set_trace;set_trace()
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        parsed_upload = Uploader.parse_uploaded(data.img)
        if parsed_upload:
            img_errors = Uploader.validate_img(
                parsed_upload['filesize'], parsed_upload['filetype']
            )
            new_item = Storage(
                kind=data.kind,
                name=data.name,
                link=data.link,
                buy_at=data.buy_at,
                percent_new=data.percent_new,
                buy_price=data.buy_price,
                price=data.price,
                express_price=data.express_price,
                if_bargain=data.if_bargain,
                state=data.state,
                city=data.city,
                description=data.description
            )
            other_errors = dict()
            Item.validator.validate(new_item, results=other_errors)
            errors = img_errors + format_errors(other_errors)
        else:
            errors = ['请上传图片.']
        if errors:
            raise BadRequest(errors)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('用户不存在.')
        new_item.img = parsed_upload['filename']
        new_item.img_meta = dict(
            size=parsed_upload['filesize'],
            filetype=parsed_upload['filetype']
        )
        item = Item.create(profile, new_item)
        Uploader.remove('items', item)
        Uploader.insert('items', item, parsed_upload.get('_file'))
        vdimensions = Uploader.insert_versions('items', item, Item.versions)
        item = Item.update_vdimensions(item, vdimensions)
        if not conf.web.config.debug:
            # esearch_insert.delay(items_esearch, item)
            items_esearch.insert(dict(item))
        conf.web.ctx['status'] = '201 Created'
        return ItemPresenter.created(item).as_json()

    def GET(self):
        '''
        As a general user, i want to filter my created items,
        so that i can check my items.
        '''
        data = self.input('access_token', 'page')
        current_user = self.signin_required(data.access_token)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('用户不存在.')
        items = Item.filter_by_profile(profile, int(data.page))
        return ItemPresenter.filter_by_profile(items).as_json()


class ItemMineShowAPI(Base):

    def GET(self, _id):
        '''
        As a general user, i want to look one of my created items,
        so that i can manage the item.
        '''
        data = self.input('access_token')
        current_user = self.signin_required(data.access_token)
        item = Item.get(_id)
        if not item:
            raise NotFound('物品不存在.')
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('用户不存在.')
        if item.profile_id != current_profile.id:
            raise BadRequest('不是您的物品.')
        is_liked = bool(Like.get(current_profile, item))
        like_count = Like.item_liked_count(item)
        comments = Comment.filter_by_item(item)
        itemimgs = ItemImg.filter_by_item(item)
        return ItemPresenter.item_mine_show(
            item, is_liked, like_count, comments, itemimgs
        ).as_json()


class PubItemsAPI(Base):
    def GET(self):
        '''
        As a general user, i want to filter published items,
        so that i can look everything.
        '''
        data = self.input('access_token', 'page')
        current_user = self.signin_required(data.access_token)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('用户不存在.')
        items = Item.filter_by_published(int(data.page))
        return ItemPresenter.filter_by_published(items).as_json()


class PubItemKindInCityAPI(Base):

    def GET(self):
        '''
        As a general user, i want to filter published items by kind at some
        location, so that i can look items.
        '''
        data = self.input('access_token', 'kind', 'page', city=None)
        current_user = self.signin_required(data.access_token)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('用户不存在.')
        if not utils.item_kind.get(int(data.kind)):
            raise NotFound('分类不存在.')
        if data.city and not int(data.city) in list(range(1, 703)):
            raise NotFound('城市不存在.')
        city = int(data.city) if data.city else data.city
        items = Item.filter_pubitems_by_kind_and_city(
            int(data.kind), city, int(data.page)
        )
        return ItemPresenter.filter_pubitems_by_kind_in_city(items).as_json()


class PubItemSearchInCityAPI(Base):

    def GET(self):
        '''
        As a general user, i want to filter published items by query at some
        location, so that i can look items.
        '''
        data = self.input('access_token', 'query', 'page', city=None)
        current_user = self.signin_required(data.access_token)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('用户不存在.')
        query = data.query.strip()
        if not query:
            raise BadRequest('请输入需要搜索的物品.')
        if data.city and not conf.web.config.debug:
            if not int(data.city) in list(range(1, 703)):
                raise NotFound('城市不存在.')
            val = items_esearch.query(
                utils.query_pubitems_in_city(
                    query,
                    int(data.city),
                    (int(data.page)-1)*Item.per_page,
                    Item.per_page
                )
            )
        elif not data.city and not conf.web.config.debug:
            val = items_esearch.query(
                utils.query_pubitems(
                    query,
                    (int(data.page)-1)*Item.per_page,
                    Item.per_page
                )
            )
        items = Item.filter_by_ids(val['ids'])
        return ItemPresenter.filter_pubitems_by_query_in_city(items).as_json()


class PubItemShowAPI(Base):

    def GET(self, _id):
        '''
        As a general user, i want to look one of published items,
        so that i can know more about the item.
        '''
        data = self.input('access_token')
        current_user = self.signin_required(data.access_token)
        item = Item.get(_id)
        if not item:
            raise NotFound('The item is not found.')
        if item.status != 2:
            raise Forbidden('The item is not available.')
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The profile is not found.')
        profile = Profile.get(item.profile_id)
        is_followed = bool(Followship.get(current_profile.id, profile.id))
        is_liked = bool(Like.get(current_profile, item))
        like_count = Like.item_liked_count(item)
        comments = Comment.filter_by_item(item)
        itemimgs = ItemImg.filter_by_item(item)
        return ItemPresenter.pubitem_show(
            item, profile, is_followed, is_liked, like_count, comments, itemimgs
        ).as_json()


class PubItemsHerAPI(Base):

    def GET(self):
        '''
        As a general user, i want to filter her published items,
        so that i can look her items.
        '''
        # from pdb import set_trace;set_trace()
        data = self.input('access_token', 'profile_id', 'page')
        current_user = self.signin_required(data.access_token)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('用户不存在.')
        her_profile = Profile.get(int(data.profile_id))
        if not her_profile:
            raise NotFound('用户不存在.')
        items = Item.filter_pubitems_by_profile(
            her_profile, int(data.page)
        )
        return ItemPresenter.filter_pubitems_by_profile(items).as_json()


class PubItemsHerShowAPI(Base):

    def GET(self, _id):
        '''
        As a general user, i want to look one of her published items,
        so that i can know more about the item.
        '''
        data = self.input('access_token', 'profile_id')
        current_user = self.signin_required(data.access_token)
        item = Item.get(_id)
        if not item:
            raise NotFound('物品不存在.')
        if item.status != 2:
            raise Forbidden('物品未公开.')
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('用户不存在.')
        if item.profile_id != int(data.profile_id):
            raise BadRequest('不是她的物品.')
        is_liked = bool(Like.get(current_profile, item))
        like_count = Like.item_liked_count(item)
        comments = Comment.filter_by_item(item)
        itemimgs = ItemImg.filter_by_item(item)
        return ItemPresenter.pubitems_her_show(
            item, is_liked, like_count, comments, itemimgs
        ).as_json()
