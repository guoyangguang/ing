#  -*- coding: utf-8 -*-
'''
csrf token apis
'''
import json
from .base import Base
from ...box import utils
from ...config import conf


class CSRFTokenAPI(Base): 
    '''
    as a general user, i want to get the csrf token from session,
    so that i can use token to launch post and put request. 
    '''
    def GET(self):
        if not conf.web.config.debug:
            token = utils.csrf_token(conf.web.ctx.session)
            return json.dumps({'csrf_token': token})
