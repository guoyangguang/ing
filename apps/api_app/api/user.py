# -*- coding: utf-8 -*-

from web import Storage
from .base import Base
from ...model.user import User
from ...model.verification import Verification
from ...model.profile import Profile
from ...model.account import Account
from ..presenter.user import UserPresenter
from ...box.exception import Unauthorized, NotFound, Forbidden, BadRequest
from ...config import conf
from ...box import utils, pycaptcha


class AuthSignupAPI(Base):
    '''
    as a not registered user, i want to signup, so that i
    can do something for myself.
    '''
    def POST(self):
        data = self.data(
            'csrf_token', 'captcha_data', 'email', 'phone', 'verification_code',
            'password', 'password_confirmation', 'accept', accept='0'
        )
        if int(data.accept) == 0:
            raise BadRequest(u'请阅读并接受协议条款.')
        errors = dict()
        phone_code = Storage(
            phone=data.phone, verification_code=data.verification_code
        )
        if not User.phone_code_validator.validate(phone_code, results=errors):
            raise BadRequest(errors)
        if not conf.web.config.debug:
            pycaptcha.check_captcha(data.captcha_data, conf.web.ctx.session)
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        verifications = Verification.filter_by_phone(data.phone)
        if not verifications:
            raise NotFound('请点击获取验证码.')
        if not verifications[0].verification_code == int(data.verification_code):
            raise BadRequest('无法确认手机号码, 请点击获取验证码.')
        user = Storage(
            email=data.email,
            phone=data.phone,
            password_digest=data.password,
            password_confirmation=data.password_confirmation
        )
        errors = dict()
        if not User.validator.validate(user, results=errors):
            raise BadRequest(message=errors)
        trans = conf.db.transaction()
        try:
            user = User.signup(user)
            profile = Profile.create_default_profile(user)
            account = Storage(balance=0)
            Account.create(profile, account)
        # TODO too general exception
        except Exception:
            trans.rollback()
        # TODO return 500?
            raise
        else:
            trans.commit()
            conf.web.ctx['status'] = '201 Created'
            return UserPresenter.user(user).as_json()


class AuthSigninAPI(Base):
    '''
    as a confirmed user,
    i want to signin with my account, so that i can generate my data.
    '''
    def POST(self):
        data = self.data('csrf_token', 'captcha_data', 'phone', 'password')
        if not conf.web.config.debug:
            pycaptcha.check_captcha(data.captcha_data, conf.web.ctx.session)
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        user = User.filter_by_phone(data.phone)
        if user and User.check_password(user, data.password):
            user = User.signin(user, conf.web.ctx.get('ip'))
            return UserPresenter.user(user).as_json()
        else:
            raise NotFound('用户不存在.')


class AuthSignoutAPI(Base):
    '''
    as a signin user,
    i want to signout, so that i must signin next time to use my account
    '''
    def POST(self):
        data = self.data('csrf_token', 'access_token')
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        user = User.signout(current_user)
        return UserPresenter.user(user).as_json()


class AuthResetPasswordAPI(Base):
    '''
    as a signup user,
    i want to input my new password, so that i can reset my password
    '''
    def POST(self):
        # TODO if user1 has signup our app, and currently user2
        # uses user1's phone number, user2 must not reset user1's password. 
        data = self.data(
            'csrf_token', 'captcha_data', 'verification_code',
            'phone', 'password', 'password_confirmation'
        )
        errors = dict()
        phone_code = Storage(
            phone=data.phone, verification_code=data.verification_code
        )
        if not User.phone_code_validator.validate(phone_code, results=errors):
            raise BadRequest(errors)
        if not conf.web.config.debug:
            pycaptcha.check_captcha(data.captcha_data, conf.web.ctx.session)
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        user = User.filter_by_phone(data.phone)
        if not user:
            raise NotFound('用户不存在.')
        verifications = Verification.filter_by_phone(data.phone)
        if not verifications:
            raise NotFound('请点击获取验证码.')
        if not verifications[0].verification_code == int(data.verification_code):
            raise BadRequest('无法确认手机号码, 请点击获取验证码.')
        user.password_digest = data.password
        user.password_confirmation = data.password_confirmation
        errors = dict()
        if not User.password_validator.validate(user, results=errors):
            raise BadRequest(errors)
        user = User.reset_password(user)
        return UserPresenter.user(user).as_json()
