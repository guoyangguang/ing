#  -*- coding: utf-8 -*-

from .base import Base
from ...model.followship import Followship
from ...model.profile import Profile 
from ..presenter.profile import ProfilePresenter
from ..presenter.followship import FollowshipPresenter
from ...box.exception import NotFound, Forbidden, BadRequest
from web import Storage
from ...box import utils
from ...config import conf


class FollowedMineAPI(Base): 

    def GET(self):
        data = self.input('access_token')
        current_user = self.signin_required(data.access_token) 
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The profile is not found.')
        profiles = Followship.followeds(current_profile)
        return ProfilePresenter.profiles(profiles).as_json()

    def POST(self):
        data = self.data('csrf_token', 'access_token', 'id')
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token) 
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The current profile is not found.')
        if current_profile.id == data.id:
            raise BadRequest('Could not follow yourself.')
        profile = Profile.get(data.id)
        if not profile:
            raise NotFound('user is not found.')
        if Followship.get(current_profile.id, profile.id):
            raise BadRequest('user is already followed.')
        followship = Followship.follow(current_profile, profile)
        conf.web.ctx.status = '201 Created'
        return FollowshipPresenter.followship(followship).as_json()

    def PUT(self, _id):
        data = self.data('csrf_token', 'access_token')
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token) 
        current_profile = Profile.filter_by_user(current_user)
        profile = Profile.get(_id)
        if not profile:
            raise NotFound('user is not found.')
        if not current_profile:
            raise NotFound('The current profile is not found.')
        followship = Followship.get(current_profile.id, profile.id)
        if not followship:
            raise NotFound('user is not followed.')
        followship = Followship.unfollow(followship)
        return FollowshipPresenter.followship(followship).as_json()


class FollowingMineAPI(Base):
    
    def GET(self):
        data = self.input('access_token')
        current_user = self.signin_required(data.access_token) 
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('The profile is not found.')
        profiles = Followship.followings(current_profile)
        return ProfilePresenter.profiles(profiles).as_json()
