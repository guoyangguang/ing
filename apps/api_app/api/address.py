# -*- coding: utf-8 -*-
'''
address API
'''

from web import Storage
from .base import Base
from ...model.profile import Profile
from ...model.address import Address
from ..presenter.address import AddressPresenter
from ...box.exception import NotFound, Forbidden, BadRequest
from ...box import utils
from ...config import conf


class AddressMineAPI(Base):

    def POST(self):
        '''
        As a general user, i want to create a my address,
        so that items are sent there.
        '''
        data = self.data(
            'csrf_token', 'access_token', 'recipient', 'phone',
            'state', 'city', 'detail', 'postcode'
        )
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        new = Storage(
            recipient=data.recipient,
            phone=data.phone,
            state=data.state,
            city=data.city,
            detail=data.detail,
            postcode=data.postcode
        )
        errors = dict()
        if not Address.validator.validate(new, results=errors):
            raise BadRequest(errors)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        created = Address.create(profile, new)
        conf.web.ctx.status = '201 Created'
        return AddressPresenter.created(created).as_json()

    def GET(self):
        '''
        As a general user, i want to get my addresses,
        so that i can choose address to be used for sending items.
        '''
        data = self.input('access_token')
        current_user = self.signin_required(data.access_token)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        addresses = Address.filter_by_profile(profile)
        return AddressPresenter.filter_by_profile(addresses).as_json()
