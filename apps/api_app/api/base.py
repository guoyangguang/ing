# -*- coding: utf-8 -*-

import web
import json
from ...box.exception import Unauthorized, MissedData
from ...model.user import User


class Base(object):

    def signin_required(self, access_token):
        user = User.filter_by_access_token(access_token)
        if user: 
            return user
        else:
            raise Unauthorized('请登录.')

    def input(self, *requireds, **defaults):
        try:
            request_data = web.input(*requireds, **defaults)
            return request_data 
        except web.webapi.BadRequest:
            raise MissedData(', '.join(requireds) + ' is required.')

    def data(self, *requireds, **defaults):
        try:
            data = web.storage(json.loads(web.data()))
        except ValueError:
            raise MissedData(', '.join(requireds) + ' is required.')
        for required in requireds:
            if required not in data:
                try:
                    data[required] = defaults[required]
                except KeyError:
                    raise MissedData(required + ' is required.')
        return data
