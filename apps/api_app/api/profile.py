#  -*- coding: utf-8 -*-

from web import storage
from .base import Base
from ...model.profile import Profile
from ..presenter.profile import ProfilePresenter
from ...box.exception import Forbidden, NotFound, BadRequest
from ...box.uploader import Uploader
from ...box import utils
from ...config import conf


class ProfileAPI(Base):

    def GET(self):
        '''
        as a general user, i want to get my profile, so that i can see it
        '''
        data = self.input('access_token')
        current_user = self.signin_required(data.access_token)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        return ProfilePresenter.profile(profile).as_json()

    def PUT(self):
        '''
        as a general user, i want update my profile, so that it is updated
        '''
        data = self.data(
            'csrf_token', 'access_token', 'name', 'gender', 'location',
            'job', 'about'
        )
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        new_profile = storage(
            name=data.name,
            gender=data.gender,
            location=data.location,
            job=data.job,
            about=data.about
        )
        errors = dict()
        if not Profile.validator.validate(new_profile, results=errors):
            raise BadRequest(errors)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        profile = Profile.update(profile, new_profile)
        return ProfilePresenter.profile(profile).as_json()


class ProfileFileAPI(Base):

    def PUT(self):
        '''
        as a general user, i want to update my profile's img,
        so that it is updated
        '''
        # request with non json data
        data = self.input(
            'csrf_token', 'access_token', 'profile_upload', profile_upload={}
        )
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        parsed_upload = Uploader.parse_uploaded(data.profile_upload)
        if parsed_upload:
            errors = Uploader.validate_img(
                parsed_upload['filesize'], parsed_upload['filetype']
            )
        else:
            errors = [u'Please upload an image.']
        if errors:
            raise BadRequest(message=errors)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        img_meta = dict(
            size=parsed_upload['filesize'],
            filetype=parsed_upload['filetype']
        )
        profile = Profile.update_img(
            profile, parsed_upload.get('filename'), img_meta
        )
        Uploader.remove('profiles', profile)
        Uploader.insert('profiles', profile, parsed_upload.get('_file'))
        Uploader.insert_versions('profiles', profile, Profile.versions)
        return ProfilePresenter.profile(profile).as_json()
