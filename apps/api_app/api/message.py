#  -*- coding: utf-8 -*-
'''
message apis
'''
from web import storage
from .base import Base
from ...box.exception import Forbidden, NotFound, BadRequest 
from ...box import utils
from ...config import conf
from ...model.message import Message
from ...model.user import User
from ...model.profile import Profile 
from ..presenter.message import MessagePresenter


class MessagesContactsAPI(Base): 
    '''
    as a general user, i want to get all my contacts,
    so that i can message with them. 
    '''
    def GET(self):
        data = self.input('access_token')
        current_user = self.signin_required(data.access_token) 
        current_profile = Profile.filter_by_user(current_user)
        if not current_profile:
            raise NotFound('用户不存在.')
        profiles = Message.contacts(current_profile)
        return MessagePresenter.contacts(profiles).as_json()


class MessagesUnreadMineAPI(Base):
    '''
    as a general user, i want to get my unread messages,
    so that i can read them
    '''
    def GET(self):
        data = self.input('access_token', 'page')
        current_user = self.signin_required(data.access_token) 
        current_profile = Profile.filter_by_user(current_user)
        if not(current_profile):
            raise NotFound('The user is not found')
        msgs = Message.unread(current_profile, int(data.page))
        return MessagePresenter.unread(msgs).as_json()


class MessagesBetweenAPI(Base):

    def GET(self):
        '''
        as a general user, i want to get
        messages between me and the other, so that i can read them 
        '''
        data = self.input('access_token', 'the_other', 'page')
        current_user = self.signin_required(data.access_token) 
        current_profile = Profile.filter_by_user(current_user)
        if not(current_profile):
            raise NotFound('The user is not found')
        if int(data.the_other) == current_profile.id:
            raise BadRequest('Bad request.')
        the_other = Profile.get(data.the_other)
        if not(the_other):
            raise NotFound('The user is not found')
        msgs = Message.between(current_profile, the_other, int(data.page))
        for msg in msgs:
            if not msg.read and msg.recipient_id == current_profile.id:
                Message.read_message(msg)
        return MessagePresenter.between(current_profile, the_other, msgs).as_json()

    def POST(self):
        '''
        as a general user, i want to send a msg to the other,
        so that i can keep in touch with the other
        '''
        data = self.data('csrf_token', 'access_token', 'body', 'recipient_id')
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token) 
        current_profile = Profile.filter_by_user(current_user)
        if not(current_profile):
            raise NotFound('The user is not found')
        recipient = Profile.get(data.recipient_id)
        if not recipient:
            raise NotFound('The user receiving message is not found.')
        if recipient.id == current_profile.id:
            raise Forbidden()
        msg = storage(body=data.body) 
        errors = dict()
        if not Message.validator.validate(msg, results=errors):
            raise BadRequest(errors)
        msg = Message.send_message(current_profile, msg, recipient)
        conf.web.ctx.status = '201 Created'
        return MessagePresenter.send_message(current_profile, msg).as_json() 
