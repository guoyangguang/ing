# -*- coding: utf-8 -*-
'''
comment API
'''

from web import Storage
from .base import Base
from ...model.profile import Profile
from ...model.item import Item
from ...model.comment import Comment
from ..presenter.comment import CommentPresenter
from ...box.exception import NotFound, Forbidden, BadRequest
from ...box import utils
from ...config import conf


class PubItemCommentAPI(Base):

    def POST(self, _id):
        '''
        As a general user, i want to create a comment for an published item,
        so that i can understand the item more.
        '''
        # from pdb import set_trace;set_trace()
        data = self.data(
            'csrf_token', 'access_token', 'body', 'comment_id', comment_id=None
        )
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        item = Item.get(_id)
        if not item:
            raise NotFound('The item is not found.')
        if item.status != 2:
            raise Forbidden('The item is not available.')
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        if data.comment_id:
            new_reply = Storage(body=data.body, comment_id=data.comment_id)
            errors = dict()
            if not Comment.reply_validator.validate(new_reply, results=errors):
                raise BadRequest(errors)
            comment = Comment.get(int(data.comment_id))
            if not comment:
                raise NotFound('The comment is not found.')
            comment_profile = Profile.get(comment.profile_id)
            created = Comment.create(profile, item, new_reply, comment)
            conf.web.ctx.status = '201 Created'
            return CommentPresenter.reply(
                profile, created, comment_profile, comment
            ).as_json()
        else:
            new_comment = Storage(body=data.body)
            errors = dict()
            if not Comment.comment_validator.validate(new_comment, results=errors):
                raise BadRequest(errors)
            created = Comment.create(profile, item, new_comment)
            conf.web.ctx.status = '201 Created'
            return CommentPresenter.comment(profile, created).as_json()
