# -*- coding: utf-8 -*-

'''
like api
'''

from .base import Base
from ...box.exception import NotFound, BadRequest, Forbidden
from ...model.profile import Profile
from ...model.item import Item
from ...model.like import Like
from ..presenter.like import LikePresenter
from ...box import utils
from ...config import conf


class LikeMineAPI(Base):

    def POST(self):
        '''
        As an general user, i want to like an item,
        so that i can express my like
        '''
        data = self.data('csrf_token', 'access_token', 'id')
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        item = Item.get(data.id)
        if not item:
            raise NotFound('The item is not found.')
        if item.status == 1:
            raise Forbidden('The item is not published.')
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        if Like.get(profile, item):
            raise BadRequest('You have already liked the item.')
        like = Like.like(profile, item)
        item_liked_count = Like.item_liked_count(item)
        conf.web.ctx.status = '201 Created'
        return LikePresenter.created(like, item_liked_count).as_json()

    def PUT(self, _id):
        '''
        As an general user, i want to cancel like an item,
        so that i can express my like
        '''
        data = self.data('csrf_token', 'access_token')
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        item = Item.get(_id)
        if not item:
            raise NotFound('The item is not found.')
        if item.status == 1:
            raise Forbidden('The item is not published.')
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        like = Like.get(profile, item)
        if not like:
            raise NotFound()
        like = Like.cancel_like(like)
        item_liked_count = Like.item_liked_count(item)
        return LikePresenter.cancel_like(like, item_liked_count).as_json()


class PubItemLikeMineAPI(Base):

    def GET(self):
        '''
        As an general user, i want to get the pubitems that i liked,
        so that i can look at them.
        '''
        data = self.input('access_token', 'page')
        current_user = self.signin_required(data.access_token)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('用户不存在.')
        items = Like.pubitems_liked_by(profile, int(data.page))
        return LikePresenter.pubitems_liked_by(items).as_json()
