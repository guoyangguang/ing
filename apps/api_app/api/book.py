# -*- coding: utf-8 -*-

from .base import Base
#from ...model.book import Book


class BookSample(Base): 

    def GET(self, id):
        import json
        '''
        f = open('/Users/gyg/Documents/freebooks/white_night_walk/sample.txt', 'r')
        val = f.read()
        f.close()
        return json.dumps(
            dict(sample='现在还不知道。田川把手指从0移到九，拨动转盘'),
            encoding='utf-8'
        )
        '''
        f = open('/Users/gyg/Documents/book/freebooks/white_night_walk/sample.txt', 'r')
        s = f.read()
        f.close()
        
        u = s.decode('utf-8')
        o = u.encode('utf-8')
        return json.dumps(dict(sample=o))
