# -*- coding: utf-8 -*-

'''
ItemImg APIs
'''

from web import Storage
from .base import Base
from ...box.exception import Forbidden, NotFound, BadRequest
from ...model.profile import Profile
from ...model.item import Item
from ...model.item_img import ItemImg
from ..presenter.item_img import ItemImgPresenter
from ...box.uploader import Uploader
from ...box import utils
from ...config import conf


class ItemMineItemImgAPI(Base):

    def POST(self, _id):
        '''
        As a general user, i want to create an item img for one of my item,
        so that there will be one more item img.
        '''
        # from pdb import set_trace;set_trace()
        # img={} to get cgi.FieldStorage
        data = self.input('csrf_token', 'access_token', 'img', img={})
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        parsed_upload = Uploader.parse_uploaded(data.img)
        if parsed_upload:
            errors = Uploader.validate_img(
                parsed_upload['filesize'], parsed_upload['filetype']
            )
        else:
            errors = ['Please upload an image.']
        if errors:
            raise BadRequest(errors)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        item = Item.get(_id)
        if not item:
            raise NotFound('The item is not found.')
        if item.profile_id != profile.id:
            raise Forbidden('The item does not belong to you.')
        # admin check
        if item.status != 1:
            raise Forbidden('The item has been published.')
        img_meta = dict(
            size=parsed_upload['filesize'],
            filetype=parsed_upload['filetype']
        )
        new_itemimg = Storage(img=parsed_upload['filename'], img_meta=img_meta)
        itemimg = ItemImg.create(item, new_itemimg)
        Uploader.remove('item_imgs', itemimg)
        Uploader.insert('item_imgs', itemimg, parsed_upload.get('_file'))
        Uploader.insert_versions('item_imgs', itemimg, ItemImg.versions)
        conf.web.ctx['status'] = '201 Created'
        return ItemImgPresenter.created(itemimg).as_json()

    def PUT(self, _id, itemimg_id):
        '''
        As a general user, i want to update the itemimg general attrs for
        one of my item, so that itemimg is updated .
        '''
        data = self.data('csrf_token', 'access_token', 'is_note', 'description')
        if not conf.web.config.debug:
            utils.prevent_csrf(data.csrf_token, conf.web.ctx.session)
        current_user = self.signin_required(data.access_token)
        new_itemimg = Storage(
            is_note=data['is_note'],
            description=data['description']
        )
        errors = dict()
        if not ItemImg.validator.validate(new_itemimg, results=errors):
            raise BadRequest(errors)
        profile = Profile.filter_by_user(current_user)
        if not profile:
            raise NotFound('The profile is not found.')
        item = Item.get(_id)
        if not item:
            raise NotFound('The item is not found.')
        if profile.id != item.profile_id:
            raise Forbidden('The item does not belong to you.')
        if item.status != 1:
            raise Forbidden('The item has been published.')
        itemimg = ItemImg.get(itemimg_id)
        if not itemimg:
            raise NotFound('The item img is not found.')
        if item.id != itemimg.item_id:
            raise Forbidden('The img does not belong to the item.')
        itemimg = ItemImg.update_general(itemimg, new_itemimg)
        return ItemImgPresenter.update_general(itemimg).as_json()
