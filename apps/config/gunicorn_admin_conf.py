#-*- coding: utf-8 -*-

import os


bind = '%s:%s' % ('127.0.0.1', 9292)
debug = False
daemon = False
loglevel = 'error'
accesslog = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'gunicorn/admin_access.log')
)
errorlog = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'gunicorn/admin_error.log')
)
workers = 1
