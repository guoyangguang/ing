# -*- coding: utf-8 -*-
'''
configures for deploy 
'''

import web
from elasticsearch import Elasticsearch
import cgi
import os

# debug mode enables module reloading
web.config.debug = False 
# TODO the secret key is never changed?
# default, fLjUfxqXtfNoIldA0A0J
# os.urandom(24),\xa8=\x1e\r\xf9y\xf7\xf8\x99Q\xe9\x9f\xdb\xce\xbd\xde\
# xdf\xd5\xe1\xdd\tN\xb8\xd2
# web.config.secret_key = os.urandom(24)

# config session
web.config.session_parameters.cookie_name = 'ing'

db = web.database(
    host='/tmp/',
    dbn='postgres',
    db='ing_pro',
    user='data',
    pw='82854269data'
)

# elastic_search client
es_cli = Elasticsearch([
    {'host': 'localhost', 'port': 9200}  # 'http://pgsqldata:3338179pgsqldata@localhost:9200'
])

# domain
DOMAIN = 'https://www.ing.com'

# files for logger
API_LOG_FILE = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'ing/app.log')
) 
BOX_LOG_FILE = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'ing/box.log')
) 
TENPAY_LOG_FILE = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'ing/tenpay.log')
)
ALIPAY_LOG_FILE = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'ing/alipay.log')
) 

# celery configure
BROKER_URI = 'amqp://ubuntu@localhost:5672//'
# BACKEND_URI = 'db+mysql://root:zzc@localhost:3306/celery'

# Maximum input we will accept when REQUEST_METHOD is POST
# 0 ==> unlimited input
cgi.maxlen = 6 * 1024 * 1024  # 2MB

UPLOADED_IMG_DIR_PREFIX = os.path.abspath(
    os.path.join(__file__, '../../../../img')
) 
UPLOADED_IMG_DIR = '/server'
DEFAULT_UPLOAD = 'default_upload'

MAIL_SERVER_HOST = 'smtp.126.com'
MAIL_SERVER_EMAIL = 'ing'
MAIL_SERVER_PASSWORD = 'ing@2014'

# oauth2
CLIENT_ID_DOUBAN = '0d14e1b965be3c2f273ea52c313ab48d'
SECRET_DOUBAN = '074de25b906b3f16'
REDIRECT_URI_DOUBAN = 'http://127.0.0.1/douban/oauth/callback'
AUTHORIZATION_URL_DOUBAN = 'https://www.douban.com/service/auth2/auth'
TOKEN_URL_DOUBAN = 'https://www.douban.com/service/auth2/token'


CLIENT_ID_SINA = '1886547968'
SECRET_SINA = '0d769f215f8cf5420cc4d2cf14b8e51e'
REDIRECT_URI_SINA = 'http://127.0.0.1/sina/oauth/callback'
CANCEL_AUTH_CALLBACK_SINA = 'http://127.0.0.1/sina/oauth/cancel'#  what does the url do?
AUTHORIZATION_URL_SINA = 'https://api.weibo.com/oauth2/authorize'
TOKEN_URL_SINA = 'https://api.weibo.com/oauth2/access_token'

DOUBAN_AUTHOR_DATA = dict(
    client_id=CLIENT_ID_DOUBAN,
    redirect_uri=REDIRECT_URI_DOUBAN
)
SINA_AUTHOR_DATA = dict(
    client_id=CLIENT_ID_SINA,
    redirect_uri=REDIRECT_URI_SINA
)
DOUBAN_TOKEN_DATA = dict(
    client_id=CLIENT_ID_DOUBAN,
    secret=SECRET_DOUBAN,
    redirect_uri=REDIRECT_URI_DOUBAN
)
SINA_TOKEN_DATA = dict(
    client_id=CLIENT_ID_SINA,
    secret=SECRET_SINA,
    redirect_uri=REDIRECT_URI_SINA
)

# alipay
ALIPAY_PARTNER_ID = '2088411660677514'
# ALIPAY_ORIGINAL_PARTNER_ID = '2088111053970139'
ALIPAY_MD5KEY = '84kfbp0suz31kn2do1iv3ceipaggmqat'
# ALIPAY_ORIGINAL_MD5KEY = 'xmz9crk1iwj07sm7p8vlxfrt89xrvfip'
# ALIPAYAPI_ORIGINAL_MD5KEY = 'jwdrwsaalraj0odiud3c067ftmz23aedm'
ALIPAY_SELLER_EMAIL = 'zhifubao@tiantianld.com'
ALIPAY_WAP_PAY_CALLBACK = 'https://www.ing.com/alipay/wap_pay/callback/'
ALIPAY_REFUND_CALLBACK = 'https://www.ing.com/alipay/refund/callback/'

# tenpay
TENPAY_PARTNER_ID = '1219176801'
TENPAY_PARTNER_KEY = 'c3a7561d49c76a2aec1ead05b7927cf3'
TENPAY_OP_USER_PASSWORD = 'tt2014ttyd'
TENPAY_REFUND_PEM_PATH = os.path.join(
    os.path.dirname(__file__),
    'tenpay_refund.pem'
)
TENPAY_WAP_PAY_CALLBACK = 'https://www.ing.com/tenpay/wap_pay/callback/'
TENPAY_REFUND_CALLBACK = 'https://www.ing.com/tenpay/refund/callback/'

# selenium
SELENIUM_WAIT_TIMEOUT = 6
# TODO what's it?
USER_AGENT = 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0'

from .dev_conf import *
