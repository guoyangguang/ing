# -*- coding: utf-8 -*-
'''
configures for local dev 
'''
import os
import web
from elasticsearch import Elasticsearch

web.config.debug = False 

# TODO pool: 5,
# encoding: unicode
db = web.database(
    dbn='postgres',
    db='ing_dev',
    user='pgsqldata',
    pw='3338179pgsqldata'
)

# elastic_search client
es_cli = Elasticsearch([
    {'host': 'localhost', 'port': 9200}  # 'http://pgsqldata:3338179pgsqldata@localhost:9200'
])

# domain
DOMAIN = 'http://127.0.0.1'

# files for logger
API_LOG_FILE = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'ing/api.log')
)
BOX_LOG_FILE = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'ing/box.log')
)
TENPAY_LOG_FILE = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'ing/tenpay.log')
) 
ALIPAY_LOG_FILE = os.path.abspath(
    os.path.join(__file__, '../../../../log', 'ing/alipay.log')
)

# celery configure 
BROKER_URI = 'amqp://gyg@localhost:5672//'
# BACKEND_URI = 'db+mysql://root:zzc@localhost:3306/celery'

UPLOADED_IMG_DIR_PREFIX = os.path.abspath(
    os.path.join(__file__, '../../../../img')
)
UPLOADED_IMG_DIR = '/server'

MAIL_SERVER_HOST = 'smtp.126.com'
MAIL_SERVER_EMAIL = 'gyg22@126.com'
MAIL_SERVER_PASSWORD = '3338179126'
