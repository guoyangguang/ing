# -*- coding: utf-8 -*-

from celery import Celery
from box import mailer
from box import log
from config.conf import (
    API_LOG_FILE, BOX_LOG_FILE, TENPAY_LOG_FILE, ALIPAY_LOG_FILE
)


celery_ins = Celery(
    'tasks',
    broker='amqp://guest@localhost:5672//',
    # backend=BACKEND_URI,
)

# logger
api_logger = log.get_logger('api', API_LOG_FILE)
box_logger = log.get_logger('box', BOX_LOG_FILE)
tenpay_logger = log.get_logger('tenpay', TENPAY_LOG_FILE)
alipay_logger = log.get_logger('alipay', ALIPAY_LOG_FILE)


@celery_ins.task
def api_logger_log(level, msg):
    api_logger.log(level, msg)


@celery_ins.task
def box_logger_log(level, msg):
    box_logger.log(level, msg)


@celery_ins.task
def tenpay_logger_log(level, msg):
    tenpay_logger.log(level, msg)


@celery_ins.task
def alipay_logger_log(level, msg):
    alipay_logger.log(level, msg)


@celery_ins.task
def send_email(subject, body, tos):
    try:
        mailer.send_mail(subject, body, tos)
    except Exception as e:
        box_logger.log(40, '@mailer ' + str(e))


@celery_ins.task
def send_sms(phone, msg):
    pass


# @celery_ins.task
# def esearch_insert(esearch, doc):
#     esearch.insert(dict(doc))
