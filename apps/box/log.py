# -*- coding: utf-8 -*-

import logging
from logging import StreamHandler
from logging.handlers import RotatingFileHandler, SMTPHandler


def get_logger(logger_name, log_file):
    logger = logging.getLogger(logger_name)
    formatter = logging.Formatter(
        "%(asctime)s %(filename)s %(funcName)s %(levelname)s %(lineno)d %(message)s"
    )

    console_handler = StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(formatter)
    file_handler = RotatingFileHandler(
        log_file, maxBytes=1024 * 1024, backupCount=5
    )
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    # mail_handler = SMTPHandler(
    #     mailhost=EHOST,
    #     fromaddr=ENAME + '@126.com',
    #     toaddrs=['test@163.com'],
    #     subject='log from ttwq',
    #     credentials=(ENAME, EPASS),
    #     secure=None
    # )
    # mail_handler.setLevel(logging.DEBUG)
    # mail_handler.setFormatter(formatter)

    logger.addHandler(console_handler)
    logger.addHandler(file_handler)
    # logger.addHandler(mail_handler)

    logger.setLevel(logging.DEBUG)

    return logger
