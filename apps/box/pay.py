# -*- coding: utf-8 -*-
'''
pay, refund... with tenpay and alipay
'''

import hashlib
import urllib
from datetime import datetime
import re
from xml.etree import ElementTree

import requests

from ..tasks import tenpay_logger_log, alipay_logger_log
from ..config.conf import (
    ALIPAY_PARTNER_ID,
    ALIPAY_MD5KEY,
    ALIPAY_SELLER_EMAIL,
    ALIPAY_WAP_PAY_CALLBACK,
    ALIPAY_REFUND_CALLBACK,
    TENPAY_PARTNER_ID,
    TENPAY_PARTNER_KEY,
    TENPAY_OP_USER_PASSWORD,
    TENPAY_REFUND_PEM_PATH,
    TENPAY_WAP_PAY_CALLBACK,
    TENPAY_REFUND_CALLBACK
)


class Tenpay(object):
    '''
    pay, refund... with tenpay
    '''

    pay_data_template = '<xml>' +\
        '<appid>{0}</appid>' +\
        '<mch_id>{1}</mch_id>' +\
        '<nonce_str>{2}</nonce_str>' +\
        '<sign>{3}</sign>' +\
        '<body>{4}</body>' +\
        '<attach>{5}</attach>' +\
        '<out_trade_no>{6}</out_trade_no>' +\
        '<fee_type>CNY</fee_type>' +\
        '<total_fee>{7}</total_fee>' +\
        '<spbill_create_ip>{8}</spbill_create_ip>' +\
        '<notify_url>{9}</notify_url>' +\
        '<trade_type>NATIVE</trade_type>' +\
        '<product_id>{10}</product_id>' +\
        '</xml>'

    def __init__(self, order):
        '''
        pay, refund ... for the order
        '''
        self.order = order

    def pay(self):
        '''
        pay with tenpay
        '''
        # TODO test pay with credit card
        sign_dic = dict(
            appid=TENPAY_PARTNER_ID,
            mch_id=TENPAY_PARTNER_ID,
            nonce_str='5K8264ILTKCH16CQ2502SI8ZNMTM67VS',
            body='test tenpay',
            attach='test tenpay',
            out_trade_no=self.order.id,
            fee_type='CNY',
            total_fee=8,
            spbill_create_ip='192.168.1.100',
            notify_url='http://www.baidu.com',
            trade_type='NATIVE',
            product_id='12235413214070356458058'
        )
        sign = self._build_sign(sign_dic)
        data = Tenpay.pay_data_template.format(
            TENPAY_PARTNER_ID,
            TENPAY_PARTNER_ID,
            '5K8264ILTKCH16CQ2502SI8ZNMTM67VS',
            sign,
            'test tenpay',
            'test tenpay',
            self.order.out_trade_no,
            'CNY',
            8,
            '192.168.1.100',
            'http://www.baidu.com',
            'NATIVE',
            '12235413214070356458058'
        )
        url = 'https://api.mch.weixin.qq.com/pay/unifiedorder'
        try:
            response = requests.request(
                method='POST',
                url=url,
                headers={'Content-Type': 'text/xml'},
                data=data
            )
        except requests.exceptions.RequestException as exception:
            tenpay_logger_log.delay(
                40,
                (u'tenpay ' + unicode(str(exception))).encode('utf-8')
            )
        else:
            response_text = response.text
            parser = ElementTree.XMLParser(encoding="utf-8")
            e_tree = ElementTree.fromstring(response_text, parser=parser)
            return e_tree

    def request_wap_pay_token(self):
        '''
        request wap pay token
        '''
        data = dict(
            bank_type='0',
            ver='2.0',
            desc='ing',
            bargainor_id=TENPAY_PARTNER_ID,
            sp_billno=self.order.id,
            total_fee=int(self.order.amount * 10 * 10),
            # notify_url=TENPAY_WAP_PAY_NOTIFY,
            callback_url=TENPAY_WAP_PAY_CALLBACK,
            attach=''
        )
        try:
            token_url = 'https://wap.tenpay.com/cgi-bin/wappayv2.0/wappay_init.cgi?'
            sign = self._build_sign(data)
            data['sign'] = sign
            query_str = '&'.join([
                '{0}={1}'.format(key, urllib.quote(data[key].encode('utf-8')))
                for key in data.keys()
            ])
            token_url = token_url + query_str
            response = requests.request(method='GET', url=token_url)
        except requests.exceptions.RequestException as exception:
            tenpay_logger_log.delay(
                40, (u'tenpay  ' + unicode(str(exception))).encode('utf-8')
            )
        else:
            e_tree = ElementTree.fromstring(response.text)
            token_id = e_tree.find('token_id')
            if token_id:
                return token_id.text
            else:
                error = e_tree.find('err_info').text
                tenpay_logger_log.delay(
                    40, (u'tenpay  ' + unicode(error)).encode('utf-8')
                )

    def build_wap_request(self, token_id):
        '''
        build wap request
        '''
        url = 'https://wap.tenpay.com/cgi-bin/wappayv2.0/wappay_gate.cgi?token_id={0}'
        return url.format(token_id)

    def refund(self):
        '''
        refund
        '''
        data = dict(
            op_user_id=TENPAY_PARTNER_ID,
            op_user_passwd=TENPAY_OP_USER_PASSWORD,
            out_refund_no=self.order.out_refund_no,
            partner=TENPAY_PARTNER_ID,
            refund_fee=int(self.order.refund_fee * 10 * 10),
            total_fee=int(self.order.total_fee * 10 * 10),
            transaction_id=self.order.transaction_id
        )
        data['key'] = TENPAY_PARTNER_KEY
        sign = self._build_sign(data)
        data['sign'] = sign
        data.pop('key')
        refund_url = 'https://mch.tenpay.com/refundapi/gateway/refund.xml'
        try:
            response = requests.request(
                method='POST',
                url=refund_url,
                data=data,
                cert=TENPAY_REFUND_PEM_PATH,
                verify=False
            )
        except requests.exceptions.RequestException as exception:
            tenpay_logger_log.delay(
                40,
                (u'tenpay ' + unicode(str(exception))).encode('utf-8')
            )
        else:
            # retcode, int, 返回状态码,0 表示成功,其他未定义
            # retmsg, string, 返回信息,如非空,为错误原因
            # refund_status, int, 退款状态:4,10:退款成功。
            # 3,5,6:退款失败。 8,9,11:退款处理中。
            # 1,2:未确定,需要商户原退款单号重新发起。
            # 7:转入代发,退款到银行发现用户的卡作废或者冻结了,
            # 导致原路退款银行卡失败,资金回流到商户的现金帐号,
            # 需要商户人工干预,通过线下或者
            # 财付通转账的方式进行退款
            # refund_id, string, 财付通退款单号
            # refund_channel, int, 退款渠道,0:退到财付通、1:退到银行
            # recv_user_id, string, 转账退款接收退款的财付通帐号
            # reccv_user_name, string, 转账退款接收退款的姓名
            # (需与接收退款的财 付通帐号绑定的姓名一致)
            response_text = response.text
            parser = ElementTree.XMLParser(encoding="utf-8")
            e_tree = ElementTree.fromstring(response_text, parser=parser)
            retcode = int(e_tree.find('retcode').text)
            if retcode == 0:
                refund_id = e_tree.find('refund_id').text
                refund_status = int(e_tree.find('refund_status').text)
                if refund_status in (4, 10):
                    msg = u'退款成功.'
                    is_refunded = True
                elif refund_status in (8, 9, 11):
                    msg = u'退款处理中,退款需要1至3个工作日,请您耐心等待.'
                    is_refunded = True
                elif refund_status in (3, 5, 6):
                    msg = u'退款失败,请您联系客服人工退单.'
                    is_refunded = False
                elif refund_status in (1, 2):
                    msg = u'请您重新退单.'
                    is_refunded = False
                elif refund_status == 7:
                    msg = u'请您联系客服人工退单.'
                    is_refunded = False
                else:
                    msg = u'未知状态,请您联系客服人工退单.'
                    is_refunded = False
                val = dict(is_refunded=is_refunded, msg=msg, refund_id=refund_id)
            else:
                val = dict(is_refunded=False, msg=e_tree.find('retmsg').text, refund_id=None)
            val_copy = val.copy()
            val_copy['transaction_id'] = self.order.transaction_id
            val_copy['out_refund_no'] = self.order.out_refund_no
            val_copy['total_fee'] = self.order.total_fee
            val_copy['refund_fee'] = self.order.refund_fee
            tenpay_logger_log.delay(
                20,
                (u'tenpay ' + unicode(str(val_copy))).encode('utf-8')
            )
            return val

    def fetch_refunding_order(self):
        '''
        fetch refunding order
        '''
        data = dict(
            partner=TENPAY_PARTNER_ID,
            transaction_id=self.order.transaction_id
        )
        data['key'] = TENPAY_PARTNER_KEY
        sign = self._build_sign(data)
        data['sign'] = sign
        data.pop('key')
        order_detail_url = 'https://gw.tenpay.com/gateway/normalrefundquery.xml'
        try:
            response = requests.request(
                method='GET',
                url=order_detail_url,
                params=data,
                cert=TENPAY_REFUND_PEM_PATH,
                verify=False
            )
        except requests.exceptions.RequestException as exception:
            tenpay_logger_log.delay(
                40,
                (u'tenpay  ' + unicode(str(exception))).encode('utf-8')
            )
        else:
            parser = ElementTree.XMLParser(encoding="utf-8")
            e_tree = ElementTree.fromstring(response.text, parser=parser)
            retcode = int(e_tree.find('retcode').text)
            if retcode == 0:
                refund_status = int(e_tree.find('refund_state_0').text)
                if refund_status in (4, 10):
                    msg = u'退款成功.'
                elif refund_status in (8, 9, 11):
                    msg = u'退款处理中,退款需要1至3个工作日,请用户耐心等待.'
                elif refund_status in (3, 5, 6):
                    msg = u'退款失败,请帮助用户人工退单.'
                elif refund_status in (1, 2):
                    msg = u'请告知用户自主重新退单.'
                elif refund_status == 7:
                    msg = u'请帮助用户人工退单.'
                else:
                    msg = u'未知状态,请帮助用户人工退单.'
                return dict(msg=msg, refund_fee=e_tree.find('refund_fee_0').text)
            else:
                return dict(msg=e_tree.find('retmsg').text, refund_fee=None)

    def check_callback_sign(self, callback_sign, data):
        '''
        check callback sign
        '''
        sign = self._build_sign(data)
        return callback_sign == sign

    def _build_sign(self, data):
        '''
        build a md5 string from dict data

        :param data: a dict of data
        :type data: dict
        '''
        keys = data.keys()
        keys.sort()
        str_for_md5 = '&'.join(['{0}={1}'.format(key, data[key]) for key in keys])
        str_for_md5 = str_for_md5 + '&key={0}'.format(TENPAY_PARTNER_KEY)
        md5_str = hashlib.md5(str_for_md5.encode('utf-8')).hexdigest().upper()
        return md5_str


class Alipay(object):
    '''
    pay, refund... with alipay
    '''

    def __init__(self, order):
        '''
        pay, refund... for the order
        '''
        self.order = order

    def request_wap_pay_token(self):
        '''
        request wap pay token
        '''

        req_data_template = '<direct_trade_create_req><subject>ing</subject>' +\
            '<out_trade_no>{0}</out_trade_no><total_fee>{1}</total_fee>' +\
            '<seller_account_name>{2}</seller_account_name>' +\
            '<notify_url>{3}</notify_url><call_back_url>{4}</call_back_url>' +\
            '<out_user>{5}</out_user><merchant_url>{}</merchant_url>' +\
            '</direct_trade_create_req>'
        req_data = req_data_template.format(
            self.order.id,
            str(self.order.amount),
            ALIPAY_SELLER_EMAIL,
            ALIPAY_WAP_PAY_CALLBACK,
            # ALIPAY_WAP_PAY_SUC_REDIRECT,
            # ALIPAY_WAP_PAY_CANCEL_REDIRECT
        )
        data = dict(
            service='alipay.wap.trade.create.direct',
            v='2.0',
            partner=ALIPAY_PARTNER_ID,
            sec_id='MD5',
            req_id=str(self.order.id),
            format='xml',
            req_data=req_data
        )
        sign = self._build_sign(data)
        data['sign'] = sign
        query_str = '&'.join([
            '{0}={1}'.format(key, urllib.quote(data[key].encode('utf-8')))
            for key in data.keys()
        ])
        try:
            url = 'https://wappaygw.alipay.com/service/rest.htm?' + query_str
            response = requests.request(method='GET', url=url)
        except requests.exceptions.RequestException as exception:
            alipay_logger_log.delay(
                40, (u'alipay  ' + unicode(str(exception))).encode('utf-8')
            )
        else:
            body = urllib.unquote(response.text)
            request_token_re = re.compile('<request_token>(.+)</request_token>')
            matched = request_token_re.search(body)
            if matched:
                return matched.group(1)
            else:
                alipay_logger_log.delay(40, (u'alipay  ' + body).encode('utf-8'))

    def build_wap_pay_request(self, request_token):
        '''
        build wap pay request
        '''
        req_data_template = '<auth_and_execute_req><request_token>{0}' +\
            '</request_token></auth_and_execute_req>'
        req_data = req_data_template.format(request_token)
        data = dict(
            service='alipay.wap.auth.authAndExecute',
            v='2.0',
            partner=ALIPAY_PARTNER_ID,
            sec_id='MD5',
            format='xml',
            req_data=req_data
        )
        sign = self._build_sign(data)
        data['sign'] = sign
        query_str = '&'.join([
            '{0}={1}'.format(key, urllib.quote(data[key].encode('utf-8')))
            for key in data.keys()
        ])
        return 'https://wappaygw.alipay.com/service/rest.htm?' + query_str

    def refund(self, batch_orders, batch_no_suffix):
        '''
        refund from alipay

        :param batch_orders: like [[trade_no1, refund_fee1]]
        :type batch_orders: list
        :param batch_no_suffix: like order.id
        :type batch_no_suffix: integer
        '''
        today = datetime.now()
        stringified_batch_orders = list()
        for order in batch_orders:
            order[1] = str(order[1])
            order.append('negotiate')
            stringified_batch_orders.append('^'.join(order))
        detail_data = '#'.join(stringified_batch_orders)
        data = dict(
            _input_charset='utf-8',
            service='refund_fastpay_by_platform_pwd',
            partner=ALIPAY_PARTNER_ID,
            seller_email=ALIPAY_SELLER_EMAIL,
            seller_user_id=ALIPAY_PARTNER_ID,
            batch_no=today.strftime('%Y%m%d') + str(batch_no_suffix),
            batch_num=str(len(batch_orders)),
            detail_data=detail_data,
            refund_date=today.strftime('%Y-%m-%d %H:%M:%S'),
            notify_url=ALIPAY_REFUND_CALLBACK
        )
        sign = self._build_sign(data)
        data['sign'] = sign
        data['sign_type'] = 'MD5'
        query_str = '&'.join([
            '{0}={1}'.format(key, urllib.quote(data[key].encode('utf-8')))
            for key in data.keys()
        ])
        return 'https://mapi.alipay.com/gateway.do?' + query_str

    def is_callback_from_alipay(self, notify_id):
        '''
        is callback from alipay
        '''
        data = dict(
            service='notify_verify'.encode('utf-8'),
            partner=ALIPAY_PARTNER_ID.encode('utf-8'),
            notify_id=notify_id.encode('utf-8')
        )
        verify_url = 'https://mapi.alipay.com/gateway.do?' + urllib.urlencode(data)
        try:
            response = requests.request(method='GET', url=verify_url)
        except requests.exceptions.RequestException as exception:
            alipay_logger_log.delay(
                40,
                (u'alipay  ' + unicode(str(exception))).encode('utf-8')
            )
        else:
            return response.text == 'true'

    def parse_wap_pay_callback_data(self, notify_data):
        '''
        parse wap pay callback data
        '''
        pass
        # e_tree = ElementTree.fromstring(notify_data)
        # parsed_data = dict(
        #     payment_type=int(e_tree.find('payment_type').text),
        #     subject=e_tree.find('subject').text,
        #     trade_no=e_tree.find('trade_no').text,
        #     buyer_email=e_tree.find('buyer_email').text,
        #     gmt_create=e_tree.find('gmt_create').text,
        #     notify_type=e_tree.find('notify_type').text,
        #     quantity=int(e_tree.find('quantity').text),
        #     out_trade_no=int(e_tree.find('out_trade_no').text),
        #     notify_time=e_tree.find('notify_time').text,
        #     seller_id=e_tree.find('seller_id').text,
        #     trade_status=e_tree.find('trade_status').text,
        #     is_total_fee_adjust=e_tree.find('is_total_fee_adjust').text,
        #     total_fee=e_tree.find('total_fee').text,
        #     gmt_payment=e_tree.find('gmt_payment').text,
        #     seller_email=e_tree.find('seller_email').text,
        #     gmt_close=e_tree.find('gmt_close').text if e_tree.find('gmt_close') else 'unk# nown',
        #     price=e_tree.find('price').text,
        #     buyer_id=e_tree.find('buyer_id').text,
        #     notify_id=e_tree.find('notify_id').text,
        #     use_coupon=e_tree.find('use_coupon').text
        # )
        # return parsed_data

    def check_callback_sign(self, callback_sign, data):
        '''
        check callback sign
        '''
        sign = self._build_sign(data)
        return callback_sign == sign

    def _build_sign(self, data):
        '''
        build a md5 string from dict data

        :param data: a dict of data
        :type data: dict
        '''
        keys = data.keys()
        keys.sort()
        str_for_md5 = '&'.join(['{0}={1}'.format(key, data[key]) for key in keys])
        md5_str = hashlib.md5(
            (str_for_md5 + ALIPAY_MD5KEY).encode('utf-8')
        ).hexdigest()
        return md5_str
