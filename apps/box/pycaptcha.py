# -*-coding: utf-8-*-

'''
captcha image and data
'''

import os
import random
import string
import StringIO
from wheezy.captcha.image import captcha
from wheezy.captcha.image import background
from wheezy.captcha.image import curve
from wheezy.captcha.image import noise
from wheezy.captcha.image import smooth
from wheezy.captcha.image import text
from wheezy.captcha.image import offset
from wheezy.captcha.image import rotate
from wheezy.captcha.image import warp

from .exception import BadRequest


def set_captcha(session):
    '''
    set new captcha in the session
    '''
    session.pop('captcha_data', None)
    stringio, data = gen_captcha()
    print '@@@@@@@@@@@@@@@', data
    session.captcha_data = data
    return stringio


def check_captcha(captcha_data, session):
    '''
    check captcha
    '''
    if not captcha_data == session.captcha_data:
        raise BadRequest(message='请输入正确的图片文字.')


def gen_captcha():
    '''
    generate captcha image and data
    '''
    font_path = os.path.join(
        os.path.dirname(__file__), 'fonts/DroidSansMono.ttf'
    )
    captcha_image = captcha(drawings=[
        background(),
        text(
            # ['fonts/CourierNew-Bold.ttf', 'fonts/LiberationMono-Bold.ttf']
            fonts=[font_path],
            drawings=[
                warp(),
                rotate(),
                offset()
            ]
        ),
        curve(),
        noise(),
        smooth()
    ])
    seq = string.uppercase + string.digits
    data = [str(random.SystemRandom().choice(seq)) for _ in range(4)]
    # TODO handle exceptions
    image = captcha_image(data)
    stringio = StringIO.StringIO()
    image.save(stringio, 'JPEG', quality=75)
    return (stringio, ''.join(data))


if __name__ == '__main__':
    def generate_captcha():
        '''
        another version of gen_captcha
        '''
        font_path = os.path.join(
            os.path.dirname(__file__), 'fonts/DroidSansMono.ttf'
        )
        captcha_image = captcha(drawings=[
            background(),
            text(
                # ['fonts/CourierNew-Bold.ttf', 'fonts/LiberationMono-Bold.ttf']
                fonts=[font_path],
                drawings=[
                    warp(),
                    rotate(),
                    offset()
                ]
            ),
            curve(),
            noise(),
            smooth()
        ])
        seq = string.uppercase + string.digits
        data = [str(random.SystemRandom().choice(seq)) for _ in range(4)]
        # TODO handle exceptions
        image = captcha_image(data)
        stringified_data = ''.join(data)
        filename = "{0}.jpg".format(stringified_data)
        img_path = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                '../../../',
                'img/server/captcha',
                filename
            )
        )
        image.save(img_path, 'JPEG', quality=75)
        return (filename, stringified_data)
    val = gen_captcha()
    print('@@@@@@@@@@@@@', val)
