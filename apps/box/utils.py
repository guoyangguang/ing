# -*- coding: utf-8 -*-

"""
the module implements various functions
"""

import uuid
import random
import re
import datetime
from .exception import BadRequest
from ..config import conf

positive_int_pattern = r"^[1-9][0-9]*$"


def check_city(model):
    try:
        cities = state_city[int(model.state)]
        cities.index(int(model.city))
    except (KeyError, ValueError):
        return False
    else:
        return True


def is_positive_int_or_zero(val):
    '''
    validate the val of user input as valid positive int or zero
    '''
    zero_pattern = re.compile(r"^0$")
    pint_pattern = re.compile(positive_int_pattern)
    if pint_pattern.search(val):
        return True
    if zero_pattern.search(val):
        return True
    return False


def is_valid_http_link(link):
    '''
    validate the http link value of user input
    '''
    if not len(link):
        return True
    else:
        pattern = re.compile(r"^(https|http)\:\/\/.+\.(com|cn|org)\/.*$")
        return pattern.search(link)


def slug(val):
    '''
    validate the string value of user input
    '''
    # TODO
    return r'^[-\w]+$'


def is_valid_date(val):
    '''
    validate the date value of user input
    '''
    try:
        datetime.datetime.strptime(val, '%Y/%m/%d')
        return True
    # except ValueError:
    #     pass
    # try:
    #     datetime.datetime.strptime(val, '%Y-%m-%d')
    #     return True
    # except ValueError:
    #     pass
    # try:
    #     datetime.datetime.strptime(val, '%Y %m %d')
    #     return True
    # except ValueError:
    #     pass
    # try:
    #     datetime.datetime.strptime(val, '%Y,%m,%d')
    #     return True
    except ValueError:
        return False


def is_phone(phone):
    '''
    validate the phone value of user input
    '''
    pattern = re.compile(r"^1[0-9]{10}$")
    return pattern.search(phone)


def is_verification_code(code): 
    '''
    validate the verification code of user input
    '''
    pattern = re.compile(r"^[0-9]{6}$")
    return pattern.search(code)


def is_postcode(code):
    '''
    validate the postcode of user input
    '''
    pattern = re.compile(r"^[0-9]{6}$")
    return pattern.search(code)


def gen_uuid():
    '''
    uuid is Universally Unique IDentifier (UUID) URN Namespace
    uuid4 generate a random uniq unguessable UUID.

    '''
    uid = uuid.uuid4()
    uid_str = str(uid)
    return uid_str


def gen_verification_code(seq, stop):
    '''
    random choose element from seq for stop times, join the choices 
    example, utils.gen_verification_code(xrange(100000, 999999), 1)
    TODO how to generate a uniq number?
    '''
    if not conf.web.config.debug:
        return ''.join(
            [str(random.SystemRandom().choice(seq)) for _ in range(stop)]
        )
    else:
        return 100000


def csrf_token(session):
    '''
    session_id is browser, csrf is cross site to post or put request.
    '''
    if not session.has_key('csrf_token'):
        session.csrf_token = uuid.uuid4().hex
    return session.csrf_token


def prevent_csrf(csrf_token, session):
    '''
    prevent csrf 
    '''
    if not csrf_token == session.pop('csrf_token', None):
        raise BadRequest(message='CSRF attempt.')


def get_key_by_value(byval, dic):
    '''
    get key by val from a dic
    '''
    _key = None
    for key, val in dic.iteritems():
        if val == byval:
            _key = key 
    return _key


item_status = {
    1: 'checking',
    2: 'published'  # the item will be kept for 90 days?
}


item_kind = {
    1: 'book',
    2: 'software',
    3: 'digital',
    4: 'home',
    5: 'office',
    6: 'make_up',
    7: 'food_drink',
    8: 'toy',
    9: 'mother_baby',
    10: 'sports_outdoor',
    11: 'cloth_shoe_bag',
    12: 'timepiece',
    13: 'jewels',
    14: 'vehicle',
    15: 'ticket_card',
    16: 'pet',
    17: 'musical_instrument',
    18: 'else'
}


order_status = {
    1: 'pending',
    2: 'paid',
    3: 'refund'
}


order_pay_method = {
    1: 'tenpay',
    2: 'alipay'
}


state_city = {
    1: list(range(1, 42)),
    2: [42],
    3: list(range(43, 80)), 
    4: [80],
    5: list(range(81, 114)),
    6: list(range(114, 152)),
    7: list(range(152, 185)),
    8: list(range(185, 233)),
    9: list(range(233, 270)),
    10: list(range(270, 291)),
    11: list(range(291, 304)),
    12: list(range(304, 333)),
    13: [333],
    14: list(range(334, 363)),
    15: list(range(363, 394)),
    16: [394],
    17: list(range(395, 417)),
    18: list(range(417, 447)),
    19: list(range(447, 476)),
    20: list(range(476, 496)),
    21: list(range(496, 516)),
    22: list(range(516, 539)),
    23: list(range(539, 560)),
    24: list(range(560, 576)),
    25: list(range(576, 598)),
    26: list(range(598, 622)),
    27: list(range(622, 637)),
    28: list(range(637, 661)),
    29: list(range(661, 670)),
    30: list(range(670, 677)),
    31: list(range(677, 684)),
    32: [684],
    33: [685],
    34: list(range(686, 703))
}

# TODO locale
invalid_msgs = dict(
    required=u'不能是空值.',
    slug=u'请填入由字母,数字和下划线组成的值.',
    length=u'请输入介于 {0} - {1} 长度的值.',
    max_length=u'输入的值超过了最大长度{0}.',
    one_of=u'请输入其中的一个值.',
    email=u'请输入正确的邮箱.',
    compare=u'请输入与 "{0}"相等的值.',
    positive_int=u'请输入正的整型值.',
    positive_int_or_zero=u'请输入正的整型值或0.',
    http_link=u'请输入正确的http链接.',
    date=u'请输入正确的日期.',
    unique=u'{0} 已存在.',
    phone=u'请输入正确的手机号码.',
    city=u'该省没有这个城市.',
    verification_code=u'请输入6位验证码.',
    postcode=u'请输入正确的邮政编码.'
)


tables = [
    'verifications', 'assignments', 'followship', 'likes', 'roles', 'messages',
    'comments', 'item_imgs', 'items', 'orders', 'tags', 'addresses',
    'hobbies', 'account_orders', 'accounts', 'access_tokens', 'profiles',
    'users'
]


# elasticsearch query
def query_pubitems_in_city(query, city, offset, limit):
    '''
    query pubitems in the city
    '''
    return {
        "query": {
            "filtered": {
                "query": {
                    "multi_match": {
                        "query": query,
                        "type": "cross_fields", 
                        "fields": ["name", "description"]
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {'term': {'status': 2}},
                            {'term': {'city': city}},
                            {'missing': {'field': 'deleted_at'}}
                        ]
                    }
                } 
            }
        },
        "from": offset,
        "size": limit
    }

def query_pubitems(query, offset, limit):
    '''
    query pubitems
    '''
    return {
        "query": {
            "filtered": {
                "query": {
                    "multi_match": {
                        "query": query,
                        "type": "cross_fields", 
                        "fields": ["name", "description"]
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {'term': {'status': 2}},
                            {'missing': {'field': 'deleted_at'}}
                        ]
                    }
                } 
            }
        },
        "from": offset,
        "size": limit
    }

# oauth site
oauth_sites = {
    1: '豆瓣',
    2: '新浪微博'
}

def oauth_user_id(site, data):
    if site == 1:
        user_id = data['douban_user_id']
    elif site == 2:
        user_id = data['uid']
    return user_id
