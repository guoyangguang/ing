# -*-_ coding: utf-8 -*-

'''
TODO oauth needs to be passed by site
'''
import requests
import json
from ..config import conf
from ..tasks import box_logger_log


def authorization_url(url, data):
    '''
    app with client_id want you to authorize your douban.
    TODO 1 avoid authorization again and again;
         2 state param
    :return: https://www.douban.com/service/auth2/auth?
    redirect_uri=https://127.0.0.1/douban/oauth/callback
    &response_type=code
    &client_id=0d14e1b965be3c2f273ea52c313ab48d'
    '''
    query_str = build_query_str(
        client_id=data.get('client_id'),
        redirect_uri=data.get('redirect_uri'),
        response_type='code'
    )
    return url + query_str


def request_token(url, data):
    '''
    If agree, request user's access token.
    There are several cases(expires_in, cancel by user...)
    in which the token will be invalid,
    so using the api response to get the new token.
    :return: {
        u'access_token': u'109a80b8bbf66ba531cf822a33e342ef',
        u'expires_in': 604800,
        u'douban_user_id': u'4567923'
    }
    '''
    if not conf.web.config.debug:
        data = dict(
            client_id=data.get('client_id'),
            client_secret=data.get('secret'),
            redirect_uri=data.get('redirect_uri'),
            grant_type='authorization_code',
            code=data.get('authorization_code')
        )
        try:
            response = requests.request(method='POST', url=url, data=data)
        except Exception as e:
            box_logger_log.delay(40, 'oauth request token, {0}'.format(str(e)))
        else:
            if response.status_code == 200:
                return {'success': True, 'data': json.loads(response.text)}
            else:
                return {'success': False, 'data': json.loads(response.text)}
    else:
        return {
            'success': True,
            'data': {
                'access_token': 'ei33rr3jiji23ed0',
                'expires_in': 64000,
                'douban_user_id': '129309'
            }
        }


def pub_status(site, access_token, data):
    '''
    publish a status to site via api.
    '''
    if not conf.web.config.debug:
        if site == 1:
            # NOTE the last / in the url
            url = 'https://api.douban.com/shuo/v2/statuses/'
            headers = {'Authorization': 'Bearer ' + access_token}
            # Maybe data will be jsoned in the future.
            data = {'text': data.encode('utf-8')}
        elif site == 2:
            url = 'https://api.weibo.com/2/statuses/update.json'
            headers = {'Authorization': 'OAuth2 ' + access_token}
            data = {'status': data.encode('utf-8')}
        try:
            response = requests.request(method='POST', url=url, headers=headers, data=data)
        except Exception as e:
            box_logger_log.delay(40, 'oauth publish status, {0}'.format(str(e)))
        else:
            if response.status_code == 200:
                return {'success': True, 'data': json.loads(response.text)}
            else:
                return {'success': False, 'data': json.loads(response.text)}
    else:
        return {'success': True, 'data': 'success'}


def build_query_str(**data):
    '''
    TODO quote? encoding?
    '''
    query_str = '?' + '&'.join(
        ["{key}={value}".format(key=key, value=value) for key, value in data.iteritems()]
    )
    return query_str


if __name__ == '__main__':
    douban_api_error = {
        400: '表明api的请求出错，具体原因参考上面列出的错误码',
        100: 'invalid_request_scheme 错误的请求协议',
        101: 'invalid_request_method 错误的请求方法',
        102: 'access_token_is_missing 未找到access_token',
        103: 'invalid_access_token access_token不存在或已被用户删除，或者用户修改了密码',
        104: 'invalid_apikey apikey不存在或已删除',
        105: 'apikey_is_blocked apikey已被禁用',
        106: 'access_token_has_expired access_token已过期',
        107: 'invalid_request_uri 请求地址未注册',
        108: 'invalid_credencial1 用户未授权访问此数据',
        109: 'invalid_credencial2 apikey未申请此权限',
        110: 'not_trial_user 未注册的测试用户',
        111: 'rate_limit_exceeded1 用户访问速度限制',
        112: 'rate_limit_exceeded2 IP访问速度限制',
        113: 'required_parameter_is_missing 缺少参数',
        114: 'unsupported_grant_type 错误的grant_type',
        115: 'unsupported_response_type 错误的response_type',
        116: 'client_secret_mismatch client_secret不匹配',
        117: 'redirect_uri_mismatch redirect_uri不匹配',
        118: 'invalid_authorization_code authorization_code不存在或已过期',
        119: 'invalid_refresh_token refresh_token不存在或已过期',
        120: 'username_password_mismatch 用户名密码不匹配',
        121: 'invalid_user 用户不存在或已删除',
        122: 'user_has_blocked 用户已被屏蔽',
        123: 'access_token_has_expired_since_password_changed 因用户修改密码而导致access_token过期',
        124: 'access_token_has_not_expired access_token未过期',
        125: 'invalid_request_scope 访问的scope不合法，开发者不用太关注，一般不会出现该错误',
        126: 'invalid_request_source 访问来源不合法',
        127: 'thirdparty_login_auth_faied 第三方授权错误',
        128: 'user_locked 用户被锁定',
        999: 'unknown 未知错误'
    }
    from pdb import set_trace;set_trace()
    # Authorization: Bearer 3778ae30e3451caba66a0a92a9cf5601
    # https://api.douban.com/shuo/v2/statuses/
    # Authorization:OAuth2 2.00lGPJqBC_lfDC71669dacdd04NDqN
    # https://api.weibo.com/2/statuses/update.json
    val = pub_status(1, '3778ae30e3451caba66a0a92a9cf5601', u'今天没有雾霾')
