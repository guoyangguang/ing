# -*- coding; utf-8 -*-

'''
 the module defines customed exceptions that will be used in the api
'''

from web import webapi
import json


def format_errors(errors):
    '''
    format validation errors from dict to list
    '''
    ls = list()
    for arr in errors:
        readable_arr = arr.replace('_', ' ').capitalize()
        for error in errors[arr]:
            readable_error = readable_arr + ', ' + error.lower()
            ls.append(readable_error)
    return ls


class Unauthorized(webapi.HTTPError):

    def __init__(self, message='unauthorized.', headers=dict()):
	status = '401 Unauthorized'
	# headers.update({'Content-Type': 'application/json'})
	webapi.HTTPError.__init__(self, status, headers, json.dumps({'msg': message}))


class NotFound(webapi.HTTPError):

    def __init__(self, message='not found.', headers=dict()):
	status = '404 Not Found'
	# headers.update({'Content-Type': 'application/json'})
	webapi.HTTPError.__init__(self, status, headers, json.dumps({'msg': message}))


class Forbidden(webapi.HTTPError):

    def __init__(self, message='forbidden.', headers=dict()):
	status = '403 Forbidden'
	# headers.update({'Content-Type': 'application/json'})
	webapi.HTTPError.__init__(self, status, headers, json.dumps({'msg': message}))


class BadRequest(webapi.HTTPError):

    def __init__(self, message, headers=dict()):
	status = '400 Bad Request'
	# headers.update({'Content-Type': 'application/json'})
        if isinstance(message, dict):
	    webapi.HTTPError.__init__(
                self,
                status,
                headers,
                json.dumps({'msg': format_errors(message)})
            )
        else:
	    webapi.HTTPError.__init__(self, status, headers, json.dumps({'msg': message}))


class MissedData(webapi.HTTPError):

    def __init__(self, message='missed data.', headers=dict()):
	status = '400 Bad Request'
	# headers.update({'Content-Type': 'application/json'})
	webapi.HTTPError.__init__(self, status, headers, json.dumps({'msg': message}))
