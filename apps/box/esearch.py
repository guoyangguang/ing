# -*-coding: utf-8-*-

from elasticsearch import ElasticsearchException
from ..config import conf
from ..box import utils
from ..tasks import box_logger_log 


class Esearch(object):
    '''
    elasticsearch client
    NOTE: keeps elasticsearch data same as db data
    '''

    TABLES = utils.tables

    def __init__(self, index):
        self.index = index

    def create_index(self):
        '''
        use elasticsearch.client.IndicesClient to create index for the index
        '''
        try:
            res = conf.es_cli.indices.create(index=self.index)
        except ElasticsearchException as e:
            box_logger_log.delay(40, u'@Elastic client: ' + str(e))
        else:
            return res

    def drop_index(self):
        '''
        use elasticsearch.client.IndicesClient to delete index for the index
        '''
        try:
            res = conf.es_cli.indices.delete(index=self.index)
        except ElasticsearchException as e:
            box_logger_log.delay(40, u'@Elastic client: ' + str(e))
        else: 
            return res

    def index_exists(self):
        '''
        use elasticsearch.client.IndicesClient to check index exist for the index
        '''
        try:
            res = conf.es_cli.indices.exists(index=self.index)
        except ElasticsearchException as e:
            box_logger_log.delay(40, u'@Elastic client: ' + str(e))
        else:
            return res

    def cat_index(self):
        '''
        use elasticsearch.client.CatClient to cat indices for the index
        '''
        try:
            res = conf.es_cli.cat.indices(index=self.index, v=True)
        except ElasticsearchException as e:
            box_logger_log.delay(40, u'@Elastic client: ' + str(e))
        else:
            return res

    def get(self, _id):
        '''
        use elasticsearch.Elasticsearch to get doc from the index
        '''
        try:
            res = conf.es_cli.get(
                index=self.index, doc_type=self.index, id=_id
            )['_source']
        except ElasticsearchException as e:
            box_logger_log.delay(40, u'@Elastic client: ' + str(e))
        else:
            return res

    def insert(self, doc):
        '''
        use elasticsearch.Elasticsearch to add doc for the index
        '''
        try:
            res = conf.es_cli.create(
                index=self.index, doc_type=self.index, id=doc['id'], body=doc
            )
        except ElasticsearchException as e:
            box_logger_log.delay(40, u'@Elastic client: ' + str(e))
        else:
            _id = res.get('_id')
            if _id:
                return self.get(int(_id))

    def update(self, _id, doc):
        '''
        use elasticsearch.Elasticsearch to update doc for the index;
        delete the doc and insert a new one.
        :param doc: not containing id
        '''
        try:
            res = conf.es_cli.index(
                index=self.index, doc_type=self.index, id=_id, body=doc
            )
        except ElasticsearchException as e:
            box_logger_log.delay(40, u'@Elastic client: ' + str(e))
        else:
            _id = res.get('_id')
            if _id:
                return self.get(int(_id))

    def delete(self, _id):
        '''
        use elasticsearch.Elasticsearch to delete doc from the index
        '''
        # TODO refresh
        try:
            res = conf.es_cli.delete(
                index=self.index, doc_type=self.index, id=_id
            )
        except ElasticsearchException as e:
            box_logger_log.delay(40, u'@Elastic client: ' + str(e))
        else:
            return res.get('found')

    def query(self, query):
        '''
        use elasticsearch.Elasticsearch to search docs for the index with query
        TODO chinese search; pagination
        [doc['id'] for doc in result['docs']]
        '''
        try:
            res = conf.es_cli.search(
                index=self.index, doc_type=self.index, body=query
            )
        except ElasticsearchException as e:
            box_logger_log.delay(40, u'@Elastic client: ' + str(e))
        else:
            result = {'count': res['hits']['total'], 'ids': [], 'docs': []}
            for hit in res['hits']['hits']:
                result['docs'].append(hit['_source'])
                result['ids'].append(hit['_id'])
            return result


if __name__ == '__main__':
    # from pdb import set_trace;set_trace()
    esearch = Esearch(index='items')
    # query context is how well, it is related with score, there are
    # best_fields, most_fields, cross_fields in one query string;
    # filter context is yes or no, it is used to filter the query context.
    result = esearch.query({
        "query": {
            "filtered": {
                "query": {
                    "multi_match": {
                        "query": u"大衣",
                        "type": "cross_fields", 
                        "fields": ["name", "description"]
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {'term': {'status': 2}},
                            {'term': {'city': 42}},
                            {'missing': {'field': 'deleted_at'}}
                        ]
                    }
                } 
            }
        },
        "from": 0,
        "size": 15
    })
    print result
    # "query":{
    #     "filtered": {
    #         "query": {
    #             "query_string": {
    #                 "query":"*tom*",
    #                 "default_operator": "OR",
    #                 "fields": ["username"]
    #             }
    #         },
    #         "filter": {
    #             "bool" : {
    #                 "must" : [
    #                     {"term" : { "isActive" : "1" } },
    #                     {"term" : { "isPrivate" : "0" } },
    #                     {"term" : { "isOwner" : "1" } }
    #                 ]
    #              }
    #          }
    #      }
    # }   
